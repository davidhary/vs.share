# Share Files

Includes common code snippets for visual studio and WiX Toolset projects.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

## Source Code[](#){name=Source-Code}
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Share](https://www.bitbucket.org/davidhary/vs.share) - Share files

```
git clone git@bitbucket.org:davidhary/vs.share.git
```

Install the project into the following folder:

#### Project relative path
```
.\Libraries\VS\Share
```

## Facilitated By[](#){name=FacilitatedBy}
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows

## Authors[](#){name=Authors}
* [ATE Coder](https://www.IntegratedScientificResources.com)

## Acknowledgments[](#){name=Acknowledgments}
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:[\
Core Libraries](https://bitbucket.org/davidhary/vs.core)[\
VI Libraries](https://bitbucket.org/davidhary/vs.io.vi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:[\
Core Libraries](https://bitbucket.org/davidhary/vs.core)[\
WiX Toolset](http://www.wixtoolset.org)[\
IVI VISA](http://www.ivifoundation.org)[\
VI Libraries](https://bitbucket.org/davidhary/vs.io.vi)

