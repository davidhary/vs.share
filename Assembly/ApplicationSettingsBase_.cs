using System.Runtime.CompilerServices;
namespace My
{
    /// <summary> An application settings base. </summary>
    /// <license>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    /// <history date="2/12/2018" by="David" revision=""> Created. </history>
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ApplicationSettingsBase : global::System.Configuration.ApplicationSettingsBase
    {
	    #region .CONVERSIONS

	    /// <summary> Converts a value to a nullable double. </summary>
	    /// <param name="value"> The value. </param>
	    /// <returns> Value as a Double? </returns>
	    public static double? ToNullableDouble(string value)
	    {
		    if (string.IsNullOrWhiteSpace(value)) {
			    return new double?();
		    } else {
			    return System.Convert.ToDouble(value);
		    }
	    }

	    public static double? ToNullableDouble(object value)
	    {
		    return value == null ? new double?() : System.Convert.ToDouble(value);
	    }

        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "Nada" )]
        public string Setting
        {
            get
            {
                return ((string)(this["Setting"]));
            }
            set
            {
                this["Setting"] = value;
            }
        }

        #endregion

#region .GENERIC

        /// <summary> Gets or sets the application setting <see cref="Boolean"/> value. </summary>
        /// <value> The application setting <see cref="Boolean"/> value. </value>
        protected bool ReadAppSettingBoolean ( [CallerMemberName()] string name = null )
            {
                return ((bool)(this[name]));
            }

        protected void WriteAppSetting( bool value, [CallerMemberName()] string name = null )
            {
                this[name] = value;
            }

        /// <summary> Gets or sets the application setting <see cref="Byte"/> value. </summary>
        /// <value> The application setting <see cref="Byte"/> value. </value>
        protected byte ReadAppSettingByte( [CallerMemberName()] string name = null )
        {
            return ((byte)(this[name]));
        }

        protected void WriteAppSetting( byte value, [CallerMemberName()] string name = null )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <value> The application setting <see cref="Double"/> value. </value>
        protected decimal ReadAppSettingDecimal( [CallerMemberName()] string name = null )
        {
            return ((decimal)(this[name]));
        }

        protected void WriteAppSetting( decimal value, [CallerMemberName()] string name = null )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <value> The application setting <see cref="Double"/> value. </value>
        protected double ReadAppSettingDouble( [CallerMemberName()] string name = null )
        {
            return ((double)(this[name]));
        }

        protected void WriteAppSetting( double value, [CallerMemberName()] string name = null )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Int32"/> value. </summary>
        /// <value> The application setting <see cref="Int32"/> value. </value>
        protected int ReadAppSettingInt( [CallerMemberName()] string name = null )
        {
            return ((int)(this[name]));
        }

        protected void WriteAppSetting( int value, [CallerMemberName()] string name = null )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Int64"/> value. </summary>
        /// <value> The application setting <see cref="Int64"/> value</value>
        protected long ReadAppSettingLong( [CallerMemberName()] string name = null )
        {
            return ((long)(this[name]));
        }

        protected void WriteAppSetting( long value, [CallerMemberName()] string name = null )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Float"/> value. </summary>
        /// <value> The application setting <see cref="Double"/> value. </value>
        protected float ReadAppSettingFloat( [CallerMemberName()] string name = null )
        {
            return ((float)(this[name]));
        }

        protected void WriteAppSetting( float value, [CallerMemberName()] string name = null )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <value> The application setting <see cref="Double"/> value. </value>
        protected System.TimeSpan ReadAppSettingTimespan( [CallerMemberName()] string name = null )
        {
            return (System.TimeSpan.Parse(this[name].ToString()));
        }

        protected void WriteAppSetting( System.TimeSpan value, [CallerMemberName()] string name = null )
        {
            this[name] = value.ToString();
        }

        protected double? ReadAppSettingNullableDouble( [CallerMemberName()] string name = null )
        {
            return ApplicationSettingsBase.ToNullableDouble( this[name].ToString() );
        }

        protected void WriteAppSetting( double? value, [CallerMemberName()] string name = null )
        {
            if( value.HasValue )
            {
                this[ name ] = value.Value;
            }
            else
            {
                this[ name ] = string.Empty();
            }
        }

        /// <summary> Gets or sets the application setting value. </summary>
        /// <value> The application setting value. </value>
        protected string ReadAppSetting( [CallerMemberName()] string name = null )
        {
            return this[name].ToString();
        }

        protected void WriteAppSetting( string value, [CallerMemberName()] string name = null )
        {
            this[name] = value;
        }

        /// <summary> Application setting enum. </summary>
        /// <param name="Name"> (Optional) caller member. </param>
        /// <returns> An ENUM value. </returns>
        protected T ReadAppSettingEnum<T>( [CallerMemberName()] string name = null )
        {
            return (T)System.Enum.Parse( typeof( T ), this[ name ].ToString() ) ;
        }

        protected void WriteAppSetting<T>( T value, [CallerMemberName()] string name = null )
        {
            this[name] = value.ToString();
        }

	    #endregion

    }

}


