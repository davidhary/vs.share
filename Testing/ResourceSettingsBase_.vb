''' <summary> Resource test settings base class. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
Public MustInherit Class ResourceSettingsBase
    Inherits ApplicationSettingsBase

#Region " CONSTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New
    End Sub

#End Region

#Region " TEST CONFIGURATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property Exists As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property Verbose As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Returns true to enable this device. </summary>
    ''' <value> The device enable option. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property Enabled As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Gets or sets all. </summary>
    ''' <value> all. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property All As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

#End Region

#Region " DEVICE RESOURCE INFORMATION "

    ''' <summary> Gets the Model of the resource. </summary>
    ''' <value> The Model of the resource. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("2002")>
    Public Overridable Property ResourceModel As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    Private _ResourcePinged As Boolean?
    ''' <summary> Gets the resource pinged. </summary>
    ''' <value> The resource pinged. </value>
    Public Overridable ReadOnly Property ResourcePinged As Boolean
        Get
            If Not Me._ResourcePinged.HasValue Then
                Me._ResourcePinged = Not String.IsNullOrWhiteSpace(Me.ResourceName)
            End If
            Return Me._ResourcePinged.Value
        End Get
    End Property

    ''' <summary> Gets the resource names delimiter. </summary>
    ''' <value> The resource names delimiter. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("|")>
    Public Overridable Property ResourceNamesDelimiter As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets the zero-based index of the host information. </summary>
    ''' <value> The host information index. </value>
    Public ReadOnly Property HostInfoIndex As Integer

    ''' <summary> Name of the resource. </summary>
    Private _ResourceName As String

    ''' <summary> Gets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public ReadOnly Property ResourceName As String
        Get
            If String.IsNullOrWhiteSpace(Me._ResourceName) Then
                Me._ResourceName = String.Empty
                Me._HostInfoIndex = -1
                For Each value As String In Me.ResourceNames.Split(Me.ResourceNamesDelimiter.ToCharArray)
                    Me._HostInfoIndex += 1
                    If VI.Pith.ResourceNamesManager.PingTcpipResource(value) Then
                        Me._ResourceName = value
                        Exit For
                    End If
                Next
            End If
            Return Me._ResourceName
        End Get
    End Property

    ''' <summary> Gets the names of the candidate resources. </summary>
    ''' <value> The names of the candidate resources. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("TCPIP0::192.168.0.254::gpib0,7::INSTR|TCPIP0::192.168.0.254::gpib0,7::INSTR|
TCPIP0::10.1.1.25::gpib0,7::INSTR|TCPIP0::10.1.1.24::gpib0,7::INSTR")>
    Public Overridable Property ResourceNames As String
        Get
            Return Me.AppSettingValue.Replace(" ", String.Empty).Replace(Environment.NewLine, String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets the Title of the resource. </summary>
    ''' <value> The Title of the resource. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("DMM2002")>
    Public Overridable Property ResourceTitle As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets the language. </summary>
    ''' <value> The language. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("SCPI")>
    Public Overridable Property Language As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets the firmware revision. </summary>
    ''' <value> The firmware revision. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1.6.4c")>
    Public Overridable Property FirmwareRevision As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

#End Region

End Class

