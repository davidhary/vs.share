﻿''' <summary> Test settings base class. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
Public MustInherit Class TestSettingsBase
    Inherits ApplicationSettingsBase

#Region " CONSTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New
    End Sub

#End Region

#Region " TEST CONFIGURATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property Exists As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property Verbose As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Returns true to enable this device. </summary>
    ''' <value> The device enable option. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property Enabled As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Gets or sets all. </summary>
    ''' <value> all. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property All As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

#End Region

End Class

