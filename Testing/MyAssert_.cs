﻿namespace isr.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    internal class MyAssert
    {
        /// <summary>   Throws the given function. </summary>
        ///
        /// <exception cref="AssertFailedException">    Thrown when an Assert Failed error condition
        ///                                             occurs. </exception>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="func"> The function. </param>
        ///
        /// <returns>   A T. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static T Throws<T>(Action func) where T : Exception
        {
            var exceptionThrown = false;
            T result = null;
            try
            {
                func.Invoke();
            }
            catch (T ex)
            {
                exceptionThrown = true;
                result = ex;
            }

            if (!exceptionThrown)
            {
                throw new AssertFailedException(
                    String.Format("An exception of type {0} was expected, but not thrown", typeof(T))
                    );
            }
            return result;
        }
    }
}
