''' <summary> The Subsystems Test Settings base class. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
Public MustInherit Class SubsystemsSettingsBase
    Inherits ApplicationSettingsBase

#Region " CONSTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New
    End Sub

#End Region

#Region " TEST CONFIGURATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property Exists As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property Verbose As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Returns true to enable this device. </summary>
    ''' <value> The device enable option. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property Enabled As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

    ''' <summary> Gets or sets all. </summary>
    ''' <value> all. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property All As Boolean
        Get
            Return Me.AppSettingBoolean()
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean() = value
        End Set
    End Property

#End Region

#Region " DEVICE SESSION INFORMATION "

    ''' <summary> Gets the keep alive query command. </summary>
    ''' <value> The keep alive query command. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("*OPC?")>
    Public Overridable Property KeepAliveQueryCommand As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets the keep-alive command. </summary>
    ''' <value> The keep-alive command. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("*OPC")>
    Public Overridable Property KeepAliveCommand As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets the initial read termination enabled. </summary>
    ''' <value> The initial read termination enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property InitialReadTerminationEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the initial read termination character. </summary>
    ''' <value> The initial read termination character. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Overridable Property InitialReadTerminationCharacter As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

    ''' <summary> Gets the read termination enabled. </summary>
    ''' <value> The read termination enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property ReadTerminationEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the read termination character. </summary>
    ''' <value> The read termination character. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Overridable Property ReadTerminationCharacter As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets the erroneous command. </summary>
    ''' <value> The erroneous command. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("*CLL")>
    Public Overridable Property ErroneousCommand As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets the error available milliseconds delay. </summary>
    ''' <value> The error available milliseconds delay. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Overridable Property ErrorAvailableMillisecondsDelay As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

    <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("-285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20")>
    Public Overridable Property ExpectedCompoundErrorMessage As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets a message describing the expected error. </summary>
    ''' <value> A message describing the expected error. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("-285")>
    Public Overridable Property ExpectedErrorMessage As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the expected error number. </summary>
    ''' <value> The expected error number. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("TSP Syntax error at line 1: unexpected symbol near `*'")>
    Public Overridable Property ExpectedErrorNumber As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

    ''' <summary> Gets or sets the expected error level. </summary>
    ''' <value> The expected error level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("20")>
    Public Overridable Property ExpectedErrorLevel As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

    <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("-113,""Undefined header;1;2018/05/26 14: 00:14.871""")>
    Public Overridable Property ParseCompoundErrorMessage As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets a message describing the Parse error. </summary>
    ''' <value> A message describing the Parse error. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Undefined header")>
    Public Overridable Property ParseErrorMessage As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the parse error number. </summary>
    ''' <value> The parse error number. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("-113")>
    Public Overridable Property ParseErrorNumber As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

    ''' <summary> Gets or sets the parse error level. </summary>
    ''' <value> The parse error level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Overridable Property ParseErrorLevel As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: ROUTE SUBSYSTEM "

    ''' <summary> Gets or sets the initial closed channels. </summary>
    ''' <value> The initial closed channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@)")>
    Public Overridable Property InitialClosedChannels As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the Initial scan list settings. </summary>
    ''' <value> The initial scan list settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@)")>
    Public Overridable Property InitialScanList As String
        Get
            Return Me.AppSettingValue
        End Get
        Set(value As String)
            Me.AppSettingValue = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: SCANNER "

    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property ScanCardInstalled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of scan cards. </summary>
    ''' <value> The number of scan cards. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("")>
    Public Overridable Property ScanCardCount As Integer?
        Get
            Return Me.AppSettingNullableInt32
        End Get
        Set(value As Integer?)
            Me.AppSettingNullableInt32 = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: SENSE SUBSYSTEM "

    ''' <summary>
    ''' Gets or sets the front terminals control enabled. With manual front terminals switch, this
    ''' would normally be set to <c><see langword="False"/></c>.
    ''' </summary>
    ''' <value> The front terminals control enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property FrontTerminalsControlEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the initial front terminals selected. </summary>
    ''' <value> The initial front terminals selected. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property InitialFrontTerminalsSelected As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the Initial power line cycles settings. </summary>
    ''' <value> The power line cycles settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Overridable Property InitialPowerLineCycles As Double
        Get
            Return Me.AppSettingDouble
        End Get
        Set(value As Double)
            Me.AppSettingDouble = value
        End Set
    End Property

    ''' <summary> Gets the Initial auto Delay Enabled settings. </summary>
    ''' <value> The auto Delay settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property InitialAutoDelayEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the Initial auto Range enabled settings. </summary>
    ''' <value> The auto Range settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property InitialAutoRangeEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the Initial auto zero Enabled settings. </summary>
    ''' <value> The auto zero settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property InitialAutoZeroEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the initial sense function. </summary>
    ''' <value> The initial sense function. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("VoltageDC")>
    Public Overridable Property InitialSenseFunction As VI.SenseFunctionModes
        Get
            Return Me.AppSettingEnum(Of VI.SenseFunctionModes)
        End Get
        Set(value As VI.SenseFunctionModes)
            Me.AppSetting = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial multimeter function. </summary>
    ''' <value> The initial multimeter function. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("VoltageDC")>
    Public Overridable Property InitialMultimeterFunction As VI.MultimeterFunctionModes
        Get
            Return Me.AppSettingEnum(Of VI.MultimeterFunctionModes)
        End Get
        Set(value As VI.MultimeterFunctionModes)
            Me.AppSetting = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial filter enabled. </summary>
    ''' <value> The initial filter enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property InitialFilterEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets the initial moving average filter enabled. </summary>
    ''' <value> The initial moving average filter enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property InitialMovingAverageFilterEnabled As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of initial filters. </summary>
    ''' <value> The number of initial filters. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Overridable Property InitialFilterCount As Integer
        Get
            Return Me.AppSettingInt32
        End Get
        Set(value As Integer)
            Me.AppSettingInt32 = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial filter window. </summary>
    ''' <value> The initial filter window. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.001")>
    Public Overridable Property InitialFilterWindow As Double
        Get
            Return Me.AppSettingDouble
        End Get
        Set(value As Double)
            Me.AppSettingDouble = value
        End Set
    End Property

    ''' <summary> Gets the initial remote sense selected. </summary>
    ''' <value> The initial remote sense selected. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property InitialRemoteSenseSelected As Boolean
        Get
            Return Me.AppSettingBoolean
        End Get
        Set(value As Boolean)
            Me.AppSettingBoolean = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: SOURCE MEASURE UNIT "

    ''' <summary> Gets the initial source function mode. </summary>
    ''' <value> The initial source function mode. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("VoltageDC")>
    Public Overridable Property InitialSourceFunction As VI.SourceFunctionModes
        Get
            Return Me.AppSettingEnum(Of VI.SourceFunctionModes)
        End Get
        Set(value As VI.SourceFunctionModes)
            Me.AppSetting = value
        End Set
    End Property

    ''' <summary> Gets the initial source level. </summary>
    ''' <value> The initial source level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0")>
    Public Overridable Property InitialSourceLevel As Double
        Get
            Return Me.AppSettingDouble
        End Get
        Set(value As Double)
            Me.AppSettingDouble = value
        End Set
    End Property

    ''' <summary> Gets the initial source limit. </summary>
    ''' <value> The initial source limit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.000105")>
    Public Overridable Property InitialSourceLimit As Double
        Get
            Return Me.AppSettingDouble
        End Get
        Set(value As Double)
            Me.AppSettingDouble = value
        End Set
    End Property


    ''' <summary> Gets the maximum output power of the instrument. </summary>
    ''' <value> The maximum output power . </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0")>
    Public Overridable Property MaximumOutputPower As Double
        Get
            Return Me.AppSettingDouble
        End Get
        Set(value As Double)
            Me.AppSettingDouble = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: STATUS SUBSYSTEM "

    ''' <summary> Gets the line frequency. </summary>
    ''' <value> The line frequency. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("60")>
    Public Overridable Property LineFrequency As Double
        Get
            Return Me.AppSettingDouble
        End Get
        Set(value As Double)
            Me.AppSettingDouble = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: TRIGGER SUBSYSTEM "

    ''' <summary> Gets the initial trigger source. </summary>
    ''' <value> The initial trigger source. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Immediate")>
    Public Overridable Property InitialTriggerSource As VI.TriggerSources
        Get
            Return Me.AppSettingEnum(Of VI.TriggerSources)
        End Get
        Set(value As VI.TriggerSources)
            Me.AppSetting = value
        End Set
    End Property

#End Region

End Class

