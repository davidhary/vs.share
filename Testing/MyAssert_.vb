﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
''' <summary> my assert. </summary>
''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/12/2016, "> 
''' https://stackoverflow.com/questions/933613/how-do-i-use-assert-to-verify-that-an-exception-has-been-thrown
''' </para></remarks>
Public NotInheritable Class MyAssert
    Private Sub New()
    End Sub

#Region " TIME SPAN "

    ''' <summary> Are equal. </summary>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="message">  The message. </param>
    Public Shared Sub AreEqual(ByVal expected As TimeSpan, ByVal actual As TimeSpan, ByVal delta As TimeSpan, ByVal message As String)
        If actual < expected.Subtract(delta) OrElse actual > expected.Add(delta) Then
            Throw New AssertFailedException($"Expected {expected} <> actual {actual} by more than {delta}; {message}")
        End If
    End Sub

    ''' <summary> Are equal. </summary>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <param name="args">     A variable-length parameters list containing arguments. </param>
    Public Shared Sub AreEqual(ByVal expected As TimeSpan, ByVal actual As TimeSpan, ByVal delta As TimeSpan, ByVal format As String, ParamArray args() As Object)
        MyAssert.AreEqual(expected, actual, delta, String.Format(format, args))
    End Sub

#End Region

#Region " IS SINGLE "

    ''' <summary> Is single. </summary>
    ''' <param name="items"> The items. </param>
    Public Shared Sub IsSingle(ByVal items As IEnumerable(Of Object))
        Assert.IsTrue(items IsNot Nothing)
        Assert.IsTrue(items.Any)
        Assert.AreEqual(1, items.Count)
    End Sub

    ''' <summary> Is single. </summary>
    ''' <param name="items">  The items. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Shared Sub IsSingle(ByVal items As IEnumerable(Of Object), ByVal format As String, ParamArray args() As Object)
        Assert.IsTrue(items IsNot Nothing, format, args)
        Assert.IsTrue(items.Any, format, args)
        Assert.AreEqual(1, items.Count, format, args)
    End Sub

#End Region

#Region " IS EMPY "

    ''' <summary> Is single. </summary>
    ''' <param name="items"> The items. </param>
    Public Shared Sub IsEmpty(ByVal items As IEnumerable(Of Object))
        If items IsNot Nothing Then Assert.IsFalse(items.Any)
    End Sub

    ''' <summary> Is single. </summary>
    ''' <param name="items">  The items. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Shared Sub IsEmpty(ByVal items As IEnumerable(Of Object), ByVal format As String, ParamArray args() As Object)
        If items IsNot Nothing Then Assert.IsFalse(items.Any, format, args)
    End Sub

#End Region

#Region " THROWS "

    ''' <summary> Throws an exception to verify that an exception has been throw.. </summary>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="func"> The function. </param>
    ''' <remakrs> 
    ''' <code>
    '''     Private Shared ReadOnly collection As NameValueCollection = New NameValueCollection From {{"ValidEntry", "ValidEntry"}}
    '''
    '''    &lt;TestMethod()&gt;
    '''    Public Sub DoesNotThrowExceptionWhenKeyDoesNotExist()
    '''        Assert.Equals(Collection.GetValueAs("InvalidEntry", "DefaultValue"), "DefaultValue")
    '''    End Sub
    '''
    '''    &lt;TestMethod()&gt;
    '''    Public Sub DoesNotThrowExceptionWhenKeyExists()
    '''        Assert.Equals(Collection.GetValueAs(Of String)("ValidEntry"), "ValidEntry")
    '''    End Sub
    '''
    '''    &lt;TestMethod()&gt;
    '''    Public Sub ThrowsExceptionWhenKeyIsNullOrWhiteSpace()
    '''        Try
    '''            Collection.GetValueAs(Of String)(Nothing)
    '''        Catch ex As ArgumentException
    '''        Catch
    '''            Assert.Fail("expected exception was not thrown")
    '''        End Try
    '''        Try
    '''            Collection.GetValueAs(Of String)(Nothing)
    '''        Catch ex As ArgumentException
    '''        Catch
    '''            Assert.Fail("expected exception was not thrown")
    '''        End Try
    '''        MyAssert.Throws(Of ArgumentException)(Function() Collection.GetValueAs(Of String)(" "))
    '''        MyAssert.Throws(Of ArgumentException)(Function() Collection.GetValueAs(Of String)(vbTab))
    '''        MyAssert.Throws(Of ArgumentException)(Function() Collection.GetValueAs(Nothing, String.Empty))
    '''        MyAssert.Throws(Of ArgumentException)(Function() Collection.GetValueAs(" ", String.Empty))
    '''        MyAssert.Throws(Of ArgumentException)(Function() Collection.GetValueAs(vbTab, String.Empty))
    '''    End Sub
    ''' </code>
    ''' </remakrs>
    Public Shared Function Throws(Of T As Exception)(ByVal func As Action) As T
        Dim exceptionThrown As Boolean = False
        Dim result As T = Nothing
        Try
            func?.Invoke()
        Catch ex As T
            exceptionThrown = True
            result = ex
        End Try
        If Not exceptionThrown Then
            Throw New AssertFailedException($"An exception of type {GetType(T)} was expected, but not thrown")
        End If
        Return result
    End Function

    ''' <summary> Throws an exception to verify that an exception has been throw. </summary>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="func">    The function. </param>
    ''' <param name="message"> The message. </param>
    ''' <returns> A T. </returns>
    Public Shared Function Throws(Of T As Exception)(ByVal func As Action, ByVal message As String) As T
        Dim exceptionThrown As Boolean = False
        Dim result As T = Nothing
        Try
            func?.Invoke()
        Catch ex As T
            exceptionThrown = True
            result = ex
        End Try
        If Not exceptionThrown Then
            Throw New AssertFailedException($"An exception of type {GetType(T)} was expected, but not thrown; {message}")
        End If
        Return result
    End Function

    ''' <summary> Throws an exception to verify that an exception has been throw. </summary>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="func">   The function. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A T. </returns>
    Public Shared Function Throws(Of T As Exception)(ByVal func As Action, ByVal format As String, ByVal ParamArray args() As Object) As T
        Return Throws(Of T)(func, String.Format(format, args))
    End Function
#End Region

End Class

