Imports System.Configuration
Imports System.Diagnostics
Imports System.Runtime.CompilerServices
Namespace My

#Region " SINGLETON INSTANCE "

    Friend Module Properties
        ''' <summary>
        ''' Returns a singleton instance of the <see cref="AppSettingsReader">App Settings Reader</see>.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public ReadOnly Property AppSettingsReader() As My.MyAppSettingsReader
            Get
                Return My.MyAppSettingsReader.[Get]
            End Get
        End Property
    End Module

#End Region

    ''' <summary> Defines a singleton class to provide application settings reading for this project. </summary>
    ''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 10/1252017, 1.0.6538.x. </para></remarks>
    Friend NotInheritable Class MyAppSettingsReader

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
            ConfigurationManager.GetSection("appSettings")
        End Sub

#End Region

#Region " SINGLETON "

        ''' <summary> Creates a new default instance of this class.</summary>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Overloads Shared Sub NewDefault()
            SyncLock MyAppSettingsReader.syncLocker
                MyAppSettingsReader.instance = New MyAppSettingsReader
            End SyncLock
        End Sub

        ''' <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
        Private Shared ReadOnly syncLocker As Object = New Object

        ''' <summary> The singleton instance </summary>
        Private Shared instance As MyAppSettingsReader

        ''' <summary> Instantiates the class. </summary>
        ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
        ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        ''' <returns> A new or existing instance of the class. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Shared Function [Get]() As MyAppSettingsReader
            If Not MyAppSettingsReader.Instantiated Then
                MyAppSettingsReader.NewDefault()
            End If
            Return MyAppSettingsReader.instance
        End Function

        ''' <summary> Gets or sets True if the singleton instance was instantiated. </summary>
        ''' <value> The instantiated. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Shared ReadOnly Property Instantiated() As Boolean
            Get
                SyncLock MyAppSettingsReader.syncLocker
                    Return MyAppSettingsReader.instance IsNot Nothing
                End SyncLock
            End Get
        End Property

#End Region

#Region " READER "

        ''' <summary> Reads application setting. </summary>
        ''' <param name="Name"> (Optional) caller member. </param>
        ''' <returns> The application setting. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
                Public Function AppSettingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
            Return ConfigurationManager.AppSettings(name)
        End Function

        ''' <summary> Reads a boolean. </summary>
        ''' <param name="Name"> (Optional) caller member. </param>
        ''' <returns> A boolean value. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
                Public Function AppSettingBoolean(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Boolean
            Return Convert.ToBoolean(ConfigurationManager.AppSettings(name))
        End Function

        ''' <summary> Reads an application setting double value. </summary>
        ''' <param name="Name"> (Optional) caller member. </param>
        ''' <returns> A Double value. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
                Public Function AppSettingDouble(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double
            Return Convert.ToDouble(ConfigurationManager.AppSettings(name))
        End Function

        ''' <summary> Application setting nullable double. </summary>
        ''' <param name="Name"> (Optional) caller member. </param>
        ''' <returns> A Double? </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
                Public Function AppSettingNullableDouble(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
            Return MyAppSettingsReader.ToNullableDouble(ConfigurationManager.AppSettings(name))
        End Function

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="int8")>
                Public Function AppSettingInt8(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Byte
            Return Convert.ToByte(ConfigurationManager.AppSettings(name))
        End Function

        ''' <summary> Reads an application setting Integer value. </summary>
        ''' <param name="Name"> (Optional) caller member. </param>
        ''' <returns> A Integer value. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
                Public Function AppSettingInt32(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer
            Return Convert.ToInt32(ConfigurationManager.AppSettings(name))
        End Function

        ''' <summary> Reads an application setting long Integer value. </summary>
        ''' <param name="Name"> (Optional) caller member. </param>
        ''' <returns> A long  Integer value. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
                Public Function AppSettingInt64(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Long
            Return Convert.ToInt64(ConfigurationManager.AppSettings(name))
        End Function

        ''' <summary> Application setting enum. </summary>
        ''' <param name="Name"> (Optional) caller member. </param>
        ''' <returns> An ENUM value. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
                Public Function AppSettingEnum(Of T)(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As T
            Return CType(System.Enum.Parse(GetType(T), ConfigurationManager.AppSettings(name)), T)
        End Function

#End Region

#Region " CONVERSIONS "

        ''' <summary> Converts a value to a nullable double. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a Double? </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Shared Function ToNullableDouble(ByVal value As String) As Double?
            If String.IsNullOrWhiteSpace(value) Then
                Return New Double?
            Else
                Return Convert.ToDouble(value)
            End If
        End Function

#End Region

    End Class

End Namespace

