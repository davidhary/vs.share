'---------------------------------------------------------------------------------------------------
' file:		C:\My\LIBRARIES\VS\Share\Bare\ShareMyLibrary_Dispatcher.vb
'
' summary:	Share my library dispatcher class
' Requires: C:\My\LIBRARIES\VS\Share\Extensions\DispatcherExtensions_.vb
'           C:\My\LIBRARIES\VS\Share\Extensions\TimeSpanExtensions_.vb
'---------------------------------------------------------------------------------------------------
Imports System.Windows.Threading
Namespace My

    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Lets Windows process all the messages currently in the message queue. </summary>
        Public Shared Sub DoEvents()
            isr.Core.DispatcherExtensions.DoEvents(Dispatcher.CurrentDispatcher)
        End Sub

        ''' <summary>
        ''' Delays operations by the given delay time selecting the delay clock which resolution exceeds
        ''' 0.2 times the delay time. T
        ''' </summary>
        ''' <param name="delayMilliseconds"> The delay in milliseconds. </param>
        Public Shared Sub Delay(ByVal delayMilliseconds As Double)
            Delay(TimeSpanExtensions.FromMilliseconds(delayMilliseconds))
        End Sub

        ''' <summary>
        ''' Delays operations by the given delay time selecting the delay clock which resolution exceeds
        ''' 0.2 times the delay time. sions.DoEvents(Dispatcher)"/> to release messages currently in the message queue.
        ''' </summary>
        ''' <param name="delayTime"> The delay time. </param>
        Public Shared Sub Delay(ByVal delayTime As TimeSpan)
            TimeSpanExtensions.StartDelayTask(delayTime).Wait()
        End Sub

        ''' <summary>
        ''' Delays operations by the given delay time selecting the delay clock which resolution exceeds
        ''' <paramref name="resolution"/> times the delay time. 
        ''' </summary>       
        ''' <param name="delayTime">  The delay time. </param>
        ''' <param name="resolution"> The resolution. </param>
        Public Shared Sub Delay(ByVal delayTime As TimeSpan, ByVal resolution As Double)
            TimeSpanExtensions.StartDelayTask(delayTime, resolution).Wait()
        End Sub

        ''' <summary> Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority. </summary>
        ''' <param name="act"> The act. </param>
        Public Shared Sub Render(ByVal act As Action)
            isr.Core.DispatcherExtensions.Render(Dispatcher.CurrentDispatcher, act)
        End Sub

    End Class

End Namespace

