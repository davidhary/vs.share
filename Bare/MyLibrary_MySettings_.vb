'---------------------------------------------------------------------------------------------------
' file:		C:\My\Libraries\VS\Share\Bare\MyLibrary_Settings_.vb
'
' summary:	My library settings class
'---------------------------------------------------------------------------------------------------
Namespace My

    Partial Public Class MyLibrary

        ''' <summary> Provide public access to the shared settings. </summary>
        ''' <remarks>
        ''' Although VS allows setting access to the settings object as Public, the
        ''' <see cref="MySettingsProperty"/> is often set with Friend access when editing the settings in
        ''' the IDE.  This property aims at overcoming this issue.
        ''' </remarks>
        ''' <value> The settings. </value>
        Public Shared ReadOnly Property Settings() As MySettings
            Get
                Return My.Settings
            End Get
        End Property

    End Class

End Namespace

