ISR .Shared<sub>&trade;</sub>: Shared code modules. Revision History


x.x.4820 03/13/13*<br/>	Adds My App Settings class. Adds exceptions.
Deprecates Cast Expression (use the System Invalid Cast Exception),
Feature Not Implement Exception (use Not Implemented Exception).

x.x.4802 02/22/13*<br/>	Invokes the exception message on the apartment thread.
Adds units tests for the exception message processor.

x.x.4799 02/19/13*<br/>	Defaults to using the current user application data
folder for data logs. Provides function to specify 'All Users' when
selecting the application data folders.

x.x.4795 02/16/13*<br/>	Adds significant version elements for building the
application data folders.

x.x.4591 07/27/12*<br/>	Adds LLBLGEN code analysis rule set. No change in Base
Exception.

\(c\) 2011 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

The source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).
