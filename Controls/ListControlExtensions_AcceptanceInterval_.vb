﻿'---------------------------------------------------------------------------------------------------
' file:		Core\Engineering\Forms\ListControl_AcceptanceInterval.vb
'
' summary:	List control acceptance interval extension methods
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports System.Drawing

Partial Public Module Methods

    ''' <summary>
    ''' Displays Acceptance Intervals in the specified
    ''' <see cref="system.Windows.Forms.ListControl">control</see>.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The list control to bind. </param>
    ''' <param name="values">  The values. </param>
    ''' <returns> The item count. </returns>
    <Extension>
    Public Function ListValues(ByVal control As System.Windows.Forms.ListControl, ByVal values As isr.Core.Engineering.CompoundCaptionCollection) As Integer
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Dim comboEnabled As Boolean = control.Enabled
        control.Enabled = False
        control.DataSource = Nothing
        control.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        control.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        ' converting to array ensures that values displayed in two controls are not connected.
        control.DataSource = values.ToArray
        control.SelectedIndex = -1
        control.Enabled = comboEnabled
        control.Invalidate()
        If values IsNot Nothing Then
            Return values.Count
        Else
            Return 0
        End If
    End Function

    ''' <summary> Displays British standard temperature coefficients of resistance in the specified
    '''           <see cref="System.Windows.Forms.ListControl">control</see> </summary>
    ''' <param name="control"> The list control to bind. </param>
    ''' <returns> An Integer. </returns>
    <Extension>
    Public Function ListBritishStandardTcrValues(ByVal control As System.Windows.Forms.ListControl) As Integer
        Return Methods.ListValues(control, isr.Core.Engineering.BritishStadnardTcr.CompoundCaptions)
    End Function

    ''' <summary> Displays temperature coefficients of resistances in the specified
    '''           <see cref="System.Windows.Forms.ListControl">control</see> </summary>
    ''' <param name="control"> The list control to bind. </param>
    ''' <returns> The item count. </returns>
    <Extension>
    Public Function ListTcrValues(ByVal control As System.Windows.Forms.ListControl) As Integer
        Return Methods.ListValues(control, isr.Core.Engineering.TcrAcceptanceInterval.Dictionary.CompoundCaptions)
    End Function

    ''' <summary> Displays tolerances in the specified 
    '''           <see cref="System.Windows.Forms.ListControl">control</see>. </summary>
    ''' <param name="control"> The list control to bind. </param>
    ''' <returns> The item count. </returns>
    <Extension>
    Public Function ListToleranceValues(ByVal control As System.Windows.Forms.ListControl) As Integer
        Return Methods.ListValues(control, isr.Core.Engineering.ToleranceAcceptanceInterval.Dictionary.CompoundCaptions)
    End Function

End Module
