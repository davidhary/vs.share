Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports System.Windows.Forms.DataVisualization.Charting
Namespace ChartingExtensions

    ''' <summary> Includes extensions for charts. </summary>
    ''' <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 02/22/2014, 1.0.5168.x. </para></remarks>
    Public Module Methods

#Region " CHART POINTS "

        ''' <summary> Remove all values from a collection. </summary>
        ''' <remarks> Same as clear. Is preferred in some collections such as the collection of points in a
        ''' chart where clear is slow. </remarks>
        ''' <param name="list"> The collection. </param>
        <Extension()>
        Public Sub RemoveAll(Of T)(ByVal list As IList(Of T))
            If (list?.Any).GetValueOrDefault(False) Then
                Do While list.Count > 0
                    list.RemoveAt(list.Count - 1)
                Loop
            End If
        End Sub

        ''' <summary> Fast clear. </summary>
        ''' <param name="points"> The points. </param>
        ''' <remarks> From Code Project Speedup MS Chart Clear Data Points by Code Artist, 30 Apr 2012, 
        ''' http://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points
        ''' </remarks>
        <Extension()>
        Public Sub FastClear(ByVal points As DataVisualization.Charting.DataPointCollection)
            If points IsNot Nothing AndAlso points.Count > 0 Then
                points.SuspendUpdates()
                points.RemoveAll()
                points.ResumeUpdates()
                ' This ensures the chart axis updates correctly on next plot. 
                ' Without adding this line, previous axis settings is used  
                ' when plotting new data points right after calling Fast Clear.
                points.Clear()
            End If
        End Sub

#End Region

#Region " NUMBER FOMRAT "

        ''' <summary>
        ''' Base 10 exponent returns the integer exponent (N) that would yield a number of the form A x
        ''' 10N, where Absolute value of A greater or equal to 1 and less than 10.0.
        ''' </summary>
        ''' <param name="value"> Number of. </param>
        ''' <returns> An Integer. </returns>
        <Extension>
        Public Function Base10Exponent(ByVal value As Double) As Integer
            Return If(value = 0, -Int32.MaxValue, Convert.ToInt32(Math.Floor(Math.Log10(Math.Abs(value)))))
        End Function

        ''' <summary>
        ''' Returns a consistent format string with minimum necessary precision for a range with intervals.
        ''' The format string should distinguish between two values spaced by
        ''' <paramref name="interval"/> and still can go as high as the <paramref name="maximum"/>.
        ''' </summary>
        ''' <param name="interval">    The interval. </param>
        ''' <param name="minimum">     The minimum value. </param>
        ''' <param name="maximum">     The maximum value. </param>
        ''' <param name="extraDigits"> The extra digits. </param>
        ''' <remarks> From https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart </remarks>
        ''' <returns> A String. </returns>
        <Extension>
        Public Function RangeFormatString(ByVal interval As Double, ByVal minimum As Double, ByVal maximum As Double, ByVal extraDigits As Integer) As String

            If interval = 0 Then interval = 0.1 * (maximum - minimum)

            ' get the precision to which must show decimal
            Dim minE As Integer = Base10Exponent(interval)

            ' get the maximum absolute value
            Dim maxAbsVal As Double = Math.Max(Math.Abs(minimum), Math.Abs(maximum))

            ' get the maximum value precision
            Dim maxE As Integer = Base10Exponent(maxAbsVal)

            ' (maxE - minE + 1) is the number of significant digits needed to distinguish two numbers spaced by "interval"
            If maxE < -4 OrElse 3 < maxE Then
                '"E#" format displays 1 digit to the left of the decimal place, and #
                ' digits to the right of the decimal place, so # = maxE - minE.
                Return $"E{extraDigits + maxE - minE}"
            Else
                'In fixed format, since all digits to the left of the decimal place are
                'displayed by default, for "F#" format, # = -minE or zero, whichever is greater.
                Return $"F{extraDigits + Math.Max(0, -minE)}"
            End If
        End Function

#End Region

#Region " AREA "

        ''' <summary> Sets the axis formats. </summary>
        ''' <param name="area">                 The area. </param>
        ''' <param name="axisLabelFormatStyle"> The axis label format style. </param>
        <Extension>
        Public Sub SetAxisFormats(ByVal area As ChartArea, ByVal axisLabelFormatStyle As AxisLabelFormat)
            If area Is Nothing Then Throw New ArgumentNullException(NameOf(area))
            area.AxisX.SetAxisFormat(axisLabelFormatStyle)
            area.AxisY.SetAxisFormat(axisLabelFormatStyle)
        End Sub

        ''' <summary> Sets the axis format. </summary>
        ''' <param name="axis">                 The axis. </param>
        ''' <param name="axisLabelFormatStyle"> The axis label format style. </param>
        <Extension>
        Public Sub SetAxisFormat(ByVal axis As Axis, ByVal axisLabelFormatStyle As AxisLabelFormat)
            If axis Is Nothing Then Throw New ArgumentNullException(NameOf(axis))
            If axisLabelFormatStyle = AxisLabelFormat.WholeNumber Then
                axis.LabelStyle.Format = "F0"
            ElseIf axisLabelFormatStyle = AxisLabelFormat.Smart Then
                With axis
                    .LabelStyle.Format = Methods.RangeFormatString(.Interval, .Minimum, .Maximum, 0)
                End With
            Else
                axis.LabelStyle.Format = String.Empty
            End If
        End Sub

        ''' <summary>
        ''' Draws an XOR selection rectangle. Zoom Rectangle is positioned in the client rectangle of the chart 
        ''' in which the zoom was initiated with a Left MouseDown event.
        ''' </summary>
        <Extension>
        Public Sub DrawZoomRect(ByVal chart As Chart, zoomRectangle As Rectangle, ByVal usingGdi As Boolean)
            If chart Is Nothing Then Throw New ArgumentNullException(NameOf(chart))
            If usingGdi Then
                Using pen As New Pen(Color.Black, 1.0F) With {.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot}
                    ' This is so much smoother than ControlPaint.DrawReversibleFrame
                    SafeNativeMethods.DrawXorRectangle(chart.CreateGraphics(), pen, zoomRectangle)
                End Using
            Else
                Dim screenRect As Rectangle = chart.RectangleToScreen(zoomRectangle)
                ControlPaint.DrawReversibleFrame(screenRect, chart.BackColor, FrameStyle.Dashed)
            End If
        End Sub

#End Region

    End Module

End Namespace
