﻿'---------------------------------------------------------------------------------------------------
' file:		Core\Engineering\Forms\DataGridView_SheetResistance.vb
'
' summary:	Data grid view sheet resistance extension methods
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports System.Drawing

Partial Public Module Methods

    ''' <summary> Configure display values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="grid">   The grid. </param>
    ''' <param name="values"> The values. </param>
    ''' <returns> An Integer. </returns>
    <Extension>
    Public Function ConfigureDisplay(ByVal grid As DataGridView, ByVal values As isr.Core.Engineering.SheetResistanceCollection) As Integer
        If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))

        Dim wasEnabled As Boolean = grid.Enabled
        grid.Enabled = False
        grid.Enabled = wasEnabled

        grid.Enabled = False
        grid.DataSource = Nothing
        grid.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.LightSalmon
        grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
        grid.DefaultCellStyle.WrapMode = DataGridViewTriState.True
        grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
        grid.AutoGenerateColumns = False
        grid.RowHeadersVisible = False
        grid.ReadOnly = True
        grid.DataSource = values

        grid.Columns.Clear()
        grid.Refresh()
        Dim displayIndex As Integer = 0
        Dim width As Integer = 0
        Dim column As DataGridViewTextBoxColumn = Nothing

        Try
            column = New DataGridViewTextBoxColumn()
            With column
                .DataPropertyName = NameOf(Core.Engineering.SheetResistance.Voltage)
                .Name = "Volt"
                .Visible = True
                .DisplayIndex = displayIndex
                .Width = 80
                .DefaultCellStyle.Format = "G5"
            End With
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn()
            With column
                .DataPropertyName = NameOf(Core.Engineering.SheetResistance.Current)
                .Name = "Ampere"
                .Visible = True
                .DisplayIndex = displayIndex
                .Width = 80
                .DefaultCellStyle.Format = "G5"
            End With
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn()
            With column
                .DataPropertyName = NameOf(Core.Engineering.SheetResistance.Resistance)
                .Name = "Ohm"
                .Visible = True
                .DisplayIndex = displayIndex
                .Width = 80
                .DefaultCellStyle.Format = "G5"
            End With
            grid.Columns.Add(column)
            width += column.Width
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try

        Try
            displayIndex += 1
            column = New DataGridViewTextBoxColumn()
            With column
                .DataPropertyName = NameOf(Core.Engineering.SheetResistance.SheetResistance)
                .Name = "Ohm/sq"
                .Visible = True
                .DisplayIndex = displayIndex
                .Width = grid.Width - width - grid.Columns.Count
                .DefaultCellStyle.Format = "G5"
            End With
            grid.Columns.Add(column)
        Catch
            If column IsNot Nothing Then column.Dispose()
            Throw
        End Try
        grid.Enabled = wasEnabled
        If grid.Columns IsNot Nothing AndAlso grid.Columns.Count > 0 Then
            Return grid.Columns.Count
        Else
            Return 0
        End If

    End Function

    ''' <summary> Displays the values described by grid. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="grid"> The grid. </param>
    ''' <returns> An Integer. </returns>
    <Extension>
    Public Function DisplayValues(ByVal grid As DataGridView, ByVal values As isr.Core.Engineering.SheetResistanceCollection) As Integer
        If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))

        Dim wasEnabled As Boolean = grid.Enabled
        grid.Enabled = False
        grid.Enabled = wasEnabled

        If grid.DataSource Is Nothing Then
            Methods.ConfigureDisplay(grid, values)
            Windows.Forms.Application.DoEvents()
        End If

        grid.DataSource = values.ToArray
        Windows.Forms.Application.DoEvents()
        Return grid.Columns.Count

    End Function

End Module
