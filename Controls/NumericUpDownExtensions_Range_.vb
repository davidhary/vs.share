'---------------------------------------------------------------------------------------------------
' file:		C:\My\LIBRARIES\VS\Share\Controls\NumericUpDownExtensions_.vb
'
' summary:	Numeric up down extensions class
' requires: isr.Core.Constructs.DLL
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices
Imports isr.Core.Constructs
Namespace NumericUpDownExtensions
    Partial Public Module Methods

#Region " RANGE "

        ''' <summary> Range setter. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="range">   The range. </param>
        ''' <returns> A Tuple(Of Decimal, Decimal) </returns>
        <Extension()>
        Public Function RangeSetter(ByVal control As NumericUpDown, ByVal range As RangeR) As Tuple(Of Decimal, Decimal)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            If range Is Nothing Then Throw New ArgumentNullException(NameOf(range))
            Methods.RangeSetter(control, CDec(range.Min), CDec(range.Max))
            Return New Tuple(Of Decimal, Decimal)(control.Minimum, control.Maximum)
        End Function

        ''' <summary> Range setter. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="range">   The range. </param>
        ''' <returns> A Tuple(Of Decimal, Decimal) </returns>
        <Extension()>
        Public Function RangeSetter(ByVal control As NumericUpDown, ByVal range As RangeI) As Tuple(Of Decimal, Decimal)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            If range Is Nothing Then Throw New ArgumentNullException(NameOf(range))
            Methods.RangeSetter(control, CDec(range.Min), CDec(range.Max))
            Return New Tuple(Of Decimal, Decimal)(control.Minimum, control.Maximum)
        End Function

#End Region

    End Module
End Namespace
