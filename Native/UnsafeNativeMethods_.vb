﻿''' <summary>
''' A safe native GDI32 methods. P/Invoke method declarations which are harmless for any code to
''' call.
''' </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/18/2019 </para></remarks>
Partial Friend NotInheritable Class UnsafeNativeMethods

#Region " CONSTRUCTION "
    Private Sub New()
    End Sub
#End Region

End Class
