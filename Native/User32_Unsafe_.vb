'---------------------------------------------------------------------------------------------------
' file:		PInvoke\UnsafeMethods\User32.vb
'
' summary:	Unsafe User 32 methods
'           http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition
'---------------------------------------------------------------------------------------------------
Imports System.Drawing
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions
Imports System.Windows.Forms

<SecurityPermission(SecurityAction.Assert, Flags:=SecurityPermissionFlag.UnmanagedCode)>
Partial Friend NotInheritable Class UnsafeNativeMethods

#Region " WINDOWS CONSTANTS "

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WM_GETTABRECT As Integer = &H130A

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WS_EX_TRANSPARENT As Integer = &H20

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WM_SETFONT As Integer = &H30

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WM_FONTCHANGE As Integer = &H1D

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WM_HSCROLL As Integer = &H114

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const TCM_HITTEST As Integer = &H130D

    ''' <summary> The windows message paint. Sent when the system makes a request to paint (a portion of) a window
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WM_PAINT As Integer = &HF

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WS_EX_LAYOUTRTL As Integer = &H400000

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification := "<Pending>")>
    Public Const WS_EX_NOINHERITLAYOUT As Integer = &H100000

#End Region

#Region " CONTENT ALIGNMENT "

    Public Const AnyRightAlign As ContentAlignment = ContentAlignment.BottomRight Or ContentAlignment.MiddleRight Or ContentAlignment.TopRight

    Public Const AnyLeftAlign As ContentAlignment = ContentAlignment.BottomLeft Or ContentAlignment.MiddleLeft Or ContentAlignment.TopLeft

    Public Const AnyTopAlign As ContentAlignment = ContentAlignment.TopRight Or ContentAlignment.TopCenter Or ContentAlignment.TopLeft

    Public Const AnyBottomAlign As ContentAlignment = ContentAlignment.BottomRight Or ContentAlignment.BottomCenter Or ContentAlignment.BottomLeft

    Public Const AnyMiddleAlign As ContentAlignment = ContentAlignment.MiddleRight Or ContentAlignment.MiddleCenter Or ContentAlignment.MiddleLeft

    Public Const AnyCenterAlign As ContentAlignment = ContentAlignment.BottomCenter Or ContentAlignment.MiddleCenter Or ContentAlignment.TopCenter

#End Region

#Region " User32.dll "

    ''' <summary> Sends a message. </summary>
    ''' <param name="hWnd">   The window. </param>
    ''' <param name="msg">    The message. </param>
    ''' <param name="wParam"> The parameter. </param>
    ''' <param name="lParam"> The parameter. </param>
    ''' <returns> An IntPtr. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function SendMessage(hWnd As IntPtr, msg As Integer, wParam As IntPtr, lParam As IntPtr) As IntPtr
        '	This Method replaces the User32 method SendMessage, but will only work for sending
        '	messages to Managed controls.
        Dim control__1 As Control = Control.FromHandle(hWnd)
        If control__1 Is Nothing Then
            Return IntPtr.Zero
        End If
        Dim winMessage As New Message() With {.HWnd = hWnd, .LParam = lParam, .WParam = wParam, .Msg = msg}
        Dim wproc As MethodInfo = control__1.[GetType]().GetMethod("WndProc", BindingFlags.NonPublic Or BindingFlags.InvokeMethod Or
                                                                   BindingFlags.FlattenHierarchy Or BindingFlags.IgnoreCase Or BindingFlags.Instance)
        Dim args As Object() = New Object() {winMessage}
        wproc.Invoke(control__1, args)
        Return CType(args(0), Message).Result
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll")>
    Friend Shared Function BeginPaint(hWnd As IntPtr, ByRef paintStruct As PAINTSTRUCT) As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll")>
    Friend Shared Function EndPaint(hWnd As IntPtr, ByRef paintStruct As PAINTSTRUCT) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", EntryPoint:="GetScrollInfo")>
    Friend Shared Function GetScrollInfo(ByVal hwnd As IntPtr, ByVal nBar As Integer, ByRef lpsi As ScrollInfo) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", CharSet:=CharSet.Auto, CallingConvention:=CallingConvention.Winapi)>
    Friend Shared Function GetFocus() As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", CharSet:=CharSet.Auto, CallingConvention:=CallingConvention.Winapi)>
    Friend Shared Function SetParent(hWndChild As IntPtr, hWndNewParent As IntPtr) As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", EntryPoint:="GetForegroundWindow")>
    Friend Shared Function GetForegroundWindow() As IntPtr
    End Function

#End Region

#Region " CLASS "

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function GetClassLongPtr(ByVal hWnd As HandleRef, ByVal nIndex As Integer) As IntPtr
        If IntPtr.Size > 4 Then
            Return GetClassLongPtr64(hWnd, nIndex)
        Else
            Return New IntPtr(GetClassLongPtr32(hWnd, nIndex))
        End If
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", EntryPoint:="GetClassLong")>
    Friend Shared Function GetClassLongPtr32(ByVal hWnd As HandleRef, ByVal nIndex As Integer) As UInteger
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", EntryPoint:="GetClassLongPtr")>
    Friend Shared Function GetClassLongPtr64(ByVal hWnd As HandleRef, ByVal nIndex As Integer) As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function SetClassLong(ByVal hWnd As HandleRef, ByVal nIndex As Integer, ByVal dwNewLong As IntPtr) As IntPtr
        If IntPtr.Size > 4 Then
            Return SetClassLongPtr64(hWnd, nIndex, dwNewLong)
        Else
            'INSTANT VB TODO TASK: There is no VB equivalent to 'unchecked' in this context:
            'ORIGINAL LINE: Return New IntPtr(SetClassLongPtr32(hWnd, nIndex, unchecked((uint)dwNewLong.ToInt32())));
            Return New IntPtr(SetClassLongPtr32(hWnd, nIndex, CUInt(dwNewLong.ToInt32())))
        End If
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", EntryPoint:="SetClassLong")>
    Public Shared Function SetClassLongPtr32(ByVal hWnd As HandleRef, ByVal nIndex As Integer, ByVal dwNewLong As UInteger) As UInteger
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", EntryPoint:="SetClassLongPtr")>
    Public Shared Function SetClassLongPtr64(ByVal hWnd As HandleRef, ByVal nIndex As Integer, ByVal dwNewLong As IntPtr) As IntPtr
    End Function

#End Region

#Region " CURSOR "

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", CharSet:=CharSet.Unicode)>
    Public Shared Function LoadCursorFromFile(<MarshalAs(UnmanagedType.LPWStr)> ByVal lpFileName As String) As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Public Shared Function LoadCursor(ByVal hInstance As IntPtr, <MarshalAs(UnmanagedType.LPWStr)> ByVal lpCursorName As String) As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll")>
    Public Shared Function DestroyCursor(ByVal hCursor As IntPtr) As Integer
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)>
    Public Shared Function ShowCursor(<MarshalAs(UnmanagedType.Bool)> ByVal bShow As Boolean) As Integer
    End Function

#End Region

#Region " MISC FUNCTIONS "

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function LoWord(dWord As IntPtr) As Integer
        Return dWord.ToInt32() And &HFFFF
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function HiWord(dWord As IntPtr) As Integer
        If (dWord.ToInt32() And &H80000000UI) = &H80000000UI Then
            Return (dWord.ToInt32() >> 16)
        Else
            Return (dWord.ToInt32() >> 16) And &HFFFF
        End If
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2106:SecureAsserts")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")>
    Public Shared Function ToIntPtr([structure] As Object) As IntPtr
        Dim lparam As IntPtr
        lparam = Marshal.AllocCoTaskMem(Marshal.SizeOf([structure]))
        Marshal.StructureToPtr([structure], lparam, False)
        Return lparam
    End Function


#End Region

End Class

