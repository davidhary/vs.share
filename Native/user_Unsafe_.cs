using System.Runtime.InteropServices;
using System;

namespace UpgradeSolution1Support.PInvoke.UnsafeNative
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	public static class user
	{

		[DllImport("User.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short DestroyCursor(short hCursor);
		[DllImport("User.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short GetClassWord(short hWnd, short nIndex);
		[DllImport("User.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short LoadCursor(short hInstance, System.IntPtr lpCursorName);
		[DllImport("User.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short SetClassWord(short hWnd, short nIndex, short wNewWord);
		[DllImport("User.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short ShowCursor(short bShow);
	}
}