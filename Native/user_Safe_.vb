Imports System
Imports System.Runtime.InteropServices
Namespace SafeNative
    Friend Module User
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function DestroyCursor(ByVal hCursor As Short) As Short
            Return UnsafeNative.User.DestroyCursor(hCursor)
        End Function
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function LoadCursor(ByVal hInstance As Short, ByVal lpCursorName As String) As Short
            Dim result2 As Short = 0
            Dim tmpPtr As IntPtr = Marshal.StringToHGlobalAnsi(lpCursorName)
            Try
                result2 = UnsafeNative.User.LoadCursor(hInstance, tmpPtr)
                lpCursorName = Marshal.PtrToStringAnsi(tmpPtr)
            Finally
                Marshal.FreeHGlobal(tmpPtr)
            End Try
            Return result2
        End Function
    End Module
End Namespace