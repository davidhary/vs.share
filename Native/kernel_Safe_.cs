using System.Runtime.InteropServices;
using System;

namespace UpgradeSolution1Support.PInvoke.SafeNative
{
	public static class kernel
	{

		public static void FreeLibrary(short hLibModule)
		{
			UpgradeSolution1Support.PInvoke.UnsafeNative.kernel.FreeLibrary(hLibModule);
		}
		public static short LoadLibrary(ref string lpLibFileName)
		{
			return UpgradeSolution1Support.PInvoke.UnsafeNative.kernel.LoadLibrary(ref lpLibFileName);
		}
	}
}