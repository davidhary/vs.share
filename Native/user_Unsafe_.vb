Imports System
Imports System.Runtime.InteropServices
Namespace UnsafeNative
    <CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2118:ReviewSuppressUnmanagedCodeSecurityUsage")>
    <System.Security.SuppressUnmanagedCodeSecurity>
    Friend Module User
        Declare Function DestroyCursor Lib "User" (ByVal hCursor As Short) As Short
        Declare Function GetClassWord Lib "User" (ByVal hWnd As Short, ByVal nIndex As Short) As Short
        Declare Function LoadCursor Lib "User" (ByVal hInstance As Short, ByVal lpCursorName As Integer) As Short
        Declare Function LoadCursor Lib "User" (ByVal hInstance As Short, ByVal lpCursorName As IntPtr) As Short
        Declare Function SetClassWord Lib "User" (ByVal hWnd As Short, ByVal nIndex As Short, ByVal wNewWord As Short) As Short
        Declare Function ShowCursor Lib "User" (ByVal bShow As Short) As Short
    End Module
End Namespace