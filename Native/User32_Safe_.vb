'---------------------------------------------------------------------------------------------------
' file:		PInvoke\SafeMethods\User32.vb
'
' summary:	Safe User 32 methods
'           http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition
'---------------------------------------------------------------------------------------------------
Imports System
Imports System.Runtime.InteropServices
Partial Friend NotInheritable Class SafeNativeMethods

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function DestroyCursor(ByVal cursorHandler As IntPtr) As Integer
        Return UnsafeNativeMethods.DestroyCursor(cursorHandler)
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function LoadCursor(ByVal libraryHandle As IntPtr, ByVal cursorName As String) As IntPtr
        Return UnsafeNativeMethods.LoadCursor(libraryHandle, cursorName)
    End Function
End Class
