# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

```## [Unreleased] ### Share
```
## [x.x.7612] - 2020-11-03
* Backup editor config and run settings on update

## [x.x.4820] - 2013-03-13
*Adds My App Settings class. Adds exceptions.
Deprecates Cast Expression (use the System Invalid Cast Exception),
Feature Not Implement Exception (use Not Implemented Exception).

## [x.x.4802] - 2013-02-22
* Invokes the exception message on the apartment thread.
Adds units tests for the exception message processor.

## [x.x.4799] - 2013-02-19
* Defaults to using the current user application data
folder for data logs. Provides function to specify 'All Users' when
selecting the application data folders.

## [x.x.4795] - 2013-02-13
* Adds significant version elements for building the
application data folders.

## [x.x.4591] - 2012-07-27
* Adds LLBLGEN code analysis rule set. No change in Base
Exception.

\(C\) 2010 Integrated Scientific Resources, Inc. All rights reserved.
