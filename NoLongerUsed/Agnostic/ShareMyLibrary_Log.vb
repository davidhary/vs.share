﻿'---------------------------------------------------------------------------------------------------
' file:		C:\My\Libraries\VS\Share\Agnostic\ShareMyLibrary_Log.vb
'
' summary:	Share my library log class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.AssemblyExtensions
Namespace My

    Partial Public NotInheritable Class MyLibrary

#Region " LOG "

        Private Shared _MyLog As MyLog
        ''' <summary> Gets the log. </summary>
        ''' <value> The log. </value>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="MyLog")>
        Public Shared ReadOnly Property MyLog As MyLog
            Get
                If MyLibrary._MyLog Is Nothing Then MyLibrary.CreateLog()
                Return MyLibrary._MyLog
            End Get
        End Property

        ''' <summary> Creates the log. </summary>
        Private Shared Sub CreateLog()
            MyLibrary._MyLog = Nothing
            Try
                MyLibrary._MyLog = isr.Core.Agnostic.MyLog.Create(MyProject.Application.Info.ProductName)
                Dim listener As Microsoft.VisualBasic.Logging.FileLogTraceListener
                With MyLibrary.MyLog
                    listener = .ReplaceDefaultTraceListener(isr.Core.Agnostic.UserLevel.AllUsers)
                    If Not .LogFileExists Then .TraceEventOverride(My.MyLibrary.ApplicationInfoTraceMessage)
                End With

                ' set the log for the application
                With My.Application.Log
                    If Not String.Equals(.DefaultFileLogWriter.FullLogFileName, listener.FullLogFileName, StringComparison.OrdinalIgnoreCase) Then
                        .TraceSource.Listeners.Remove(DefaultFileLogTraceListener.DefaultFileLogWriterName)
                        .TraceSource.Listeners.Add(listener)
                        .TraceSource.Switch.Level = SourceLevels.Verbose
                    End If
                End With

                ' set the trace level.
                MyLibrary.ApplyTraceLogLevel()
            Catch
                If MyLibrary._MyLog IsNot Nothing Then MyLibrary._MyLog.Dispose()
                MyLibrary._MyLog = Nothing
                Throw
            End Try
        End Sub

        ''' <summary> Gets the trace level. </summary>
        ''' <value> The trace level. </value>
        Public Shared Property TraceLevel As TraceEventType = TraceEventType.Warning

#End Region

#Region " IDENTITY "

        ''' <summary> Identifies this library. </summary>
        ''' <param name="talker"> The talker. </param>
        Public Shared Sub Identify(ByVal talker As isr.Core.Services.ITraceMessageTalker)
            talker?.IdentifyTalker(MyLibrary.IdentityTraceMessage())
        End Sub

        ''' <summary> Gets the identity. </summary>
        ''' <value> The identity. </value>
        Public Shared ReadOnly Property Identity() As String
            Get
                Return $"{MyLibrary.AssemblyProduct} ID = {MyLibrary.TraceEventId:X}"
            End Get
        End Property

        ''' <summary> Library log trace message. </summary>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function LogTraceMessage() As TraceMessage
            Return New TraceMessage(TraceEventType.Information, MyLibrary.TraceEventId, $"Log at;. {MyLog.FullLogFileName}")
        End Function

        ''' <summary> Gets a message describing the identity trace. </summary>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function IdentityTraceMessage() As TraceMessage
            Return MyLibrary.IdentityTraceMessage(TraceEventType.Information)
            Return New TraceMessage(TraceEventType.Information, MyLibrary.TraceEventId, My.MyLibrary.Identity)
        End Function

        ''' <summary> Gets a message describing the identity trace. </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function IdentityTraceMessage(ByVal eventType As TraceEventType) As TraceMessage
            Return New TraceMessage(eventType, MyLibrary.TraceEventId, My.MyLibrary.Identity)
        End Function

        ''' <summary> Application information trace message. </summary>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function ApplicationInfoTraceMessage() As TraceMessage
            Return New TraceMessage(TraceEventType.Information, MyLibrary.TraceEventId, MyProject.Application.Info.BuildProductTimeCaption())
        End Function

#End Region

#Region " UNPUBLISHED MESSAGES "

        ''' <summary> Gets the unpublished identify date. </summary>
        ''' <value> The unpublished identify date. </value>
        Public Shared Property UnpublishedIdentifyDate As DateTimeOffset

        ''' <summary> Gets or sets the unpublished trace messages. </summary>
        ''' <value> The unpublished trace messages. </value>
        Public Shared ReadOnly Property UnpublishedTraceMessages As TraceMessagesQueue = New TraceMessagesQueue

        ''' <summary> Logs unpublished message. </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="message">   The message. </param>
        ''' <returns> A String. </returns>
        Public Shared Function LogUnpublishedMessage(ByVal eventType As TraceEventType, ByVal message As String) As String
            Return MyLibrary.LogUnpublishedMessage(eventType, MyLibrary.TraceEventId, message)
        End Function

        ''' <summary> Logs unpublished message. </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="format">    Describes the format to use. </param>
        ''' <param name="args">      A variable-length parameters list containing arguments. </param>
        Public Shared Function LogUnpublishedMessage(ByVal eventType As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return MyLibrary.LogUnpublishedMessage(eventType, MyLibrary.TraceEventId, format, args)
        End Function

        ''' <summary> Logs unpublished message. </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="id">        The identifier. </param>
        ''' <param name="format">    Describes the format to use. </param>
        ''' <param name="args">      A variable-length parameters list containing arguments. </param>
        Public Shared Function LogUnpublishedMessage(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return MyLibrary.LogUnpublishedMessage(New TraceMessage(eventType, id, format, args))
        End Function

        ''' <summary> Logs unpublished message. </summary>
        ''' <param name="message"> The message. </param>
        Public Shared Function LogUnpublishedMessage(ByVal message As TraceMessage) As String
            If message Is Nothing Then
                Return String.Empty
            Else
                If DateTimeOffset.Now.Date > MyLibrary.UnpublishedIdentifyDate Then
                    MyLog.TraceEventOverride(MyLibrary.ApplicationInfoTraceMessage)
                    MyLog.TraceEventOverride(MyLibrary.LogTraceMessage)
                    MyLog.TraceEventOverride(MyLibrary.IdentityTraceMessage())
                    MyLibrary.UnpublishedIdentifyDate = DateTimeOffset.Now.Date
                End If
                MyLibrary.UnpublishedTraceMessages.Enqueue(message)
                MyLog.TraceEvent(message)
                Return message.Details
            End If
        End Function

#End Region

    End Class

End Namespace


