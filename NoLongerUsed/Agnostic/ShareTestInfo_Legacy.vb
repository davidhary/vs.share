﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
''' <summary> Information about the test. </summary>
''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/13/2017 </para></remarks>
Partial Friend NotInheritable Class TestInfo

#Region " CONSTRUCTION "

    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " CONFIGURATION INFORMATION "

    ''' <summary> Gets the Model of the resource. </summary>
    ''' <value> The Model of the resource. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property Exists As Boolean
        Get
            Return My.MyAppSettingsReader.AppSettingBoolean()
        End Get
    End Property

    ''' <summary> Gets the verbose. </summary>
    ''' <value> The verbose. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property Verbose As Boolean
        Get
            Return My.MyAppSettingsReader.AppSettingBoolean()
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating of all data are to be used for a test. </summary>
    ''' <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property All As Boolean
        Get
            Return My.MyAppSettingsReader.AppSettingBoolean()
        End Get
    End Property

    ''' <summary> Gets the time zone of the first test site. </summary>
    ''' <value> The first time zone. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property FirstTimeZone As String
        Get
            Return My.MyAppSettingsReader.AppSettingValue()
        End Get
    End Property

    ''' <summary> Gets the time zone offset of the first test site. </summary>
    ''' <value> The second time zone offset. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property FirstTimeZoneOffset As Integer
        Get
            Return My.MyAppSettingsReader.AppSettingInt32
        End Get
    End Property

    ''' <summary> Gets the time zone of the second test site. </summary>
    ''' <value> The second time zone. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property SecondTimeZone As String
        Get
            Return My.MyAppSettingsReader.AppSettingValue()
        End Get
    End Property

    ''' <summary> Gets the time zone offset of the second test site. </summary>
    ''' <value> The second time zone offset. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property SecondTimeZoneOffset As Integer
        Get
            Return My.MyAppSettingsReader.AppSettingInt32
        End Get
    End Property

#End Region

#Region " VALIDATIONS "

    ''' <summary> Validated test context. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <returns> A TestContext. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function ValidatedTestContext(ByVal testContext As TestContext) As TestContext
        If testContext Is Nothing Then Throw New ArgumentNullException(NameOf(testContext))
        Return testContext
    End Function

#End Region

#Region " TRACE "

    ''' <summary> Initializes the trace listener. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub InitializeTraceListener()
        TestInfo.ReplaceTraceListener()
        Console.Out.WriteLine(My.Application.Log.DefaultFileLogWriter.FullLogFileName)
    End Sub

    ''' <summary> Replace trace listener. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub ReplaceTraceListener()
        With My.Application.Log
            .TraceSource.Listeners.Remove(isr.Core.Agnostic.DefaultFileLogTraceListener.DefaultFileLogWriterName)
            .TraceSource.Listeners.Add(isr.Core.Agnostic.DefaultFileLogTraceListener.CreateListener(isr.Core.Agnostic.UserLevel.CurrentUser))
            .TraceSource.Switch.Level = SourceLevels.Verbose
        End With
    End Sub

    ''' <summary> Trace message. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub TraceMessage(ByVal format As String, ByVal ParamArray args() As Object)
        TestInfo.TraceMessage(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Trace message. </summary>
    ''' <param name="message"> The message. </param>
    Private Shared Sub TraceMessage(ByVal message As String)
        My.Application.Log.WriteEntry(message)
        'System.Diagnostics.Debug.WriteLine(message)
        Console.Out.WriteLine(message)
    End Sub

    ''' <summary> Verbose message. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub VerboseMessage(ByVal format As String, ByVal ParamArray args() As Object)
        If TestInfo.Verbose Then TraceMessage(format, args)
    End Sub

#End Region

#Region " TRACE MESSAGES QUEUE "

    Private Shared _TraceMessagesQueueListener As isr.Core.Services.TraceMessagesQueueListener

    ''' <summary> Gets the trace message queue listener. </summary>
    ''' <value> The trace message queue listener. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property TraceMessagesQueueListener As isr.Core.Services.TraceMessagesQueueListener
        Get
            If TestInfo._TraceMessagesQueueListener Is Nothing Then
                TestInfo._TraceMessagesQueueListener = New isr.Core.Services.TraceMessagesQueueListener
                TestInfo._TraceMessagesQueueListener.ApplyTraceLevel(TraceEventType.Warning)
            End If
            Return TestInfo._TraceMessagesQueueListener
        End Get
    End Property

    ''' <summary> Assert message. </summary>
    ''' <param name="traceMessage"> Message describing the trace. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub AssertMessage(ByVal traceMessage As isr.Core.Services.TraceMessage)
        If traceMessage Is Nothing Then
        ElseIf traceMessage.EventType = TraceEventType.Warning Then
            TestInfo.TraceMessage($"Warning published: {traceMessage.ToString}")
        ElseIf traceMessage.EventType = TraceEventType.Error Then
            Assert.Fail($"Error published: {traceMessage.ToString}")
        End If
    End Sub

    ''' <summary> Assert message queue. </summary>
    ''' <param name="queue"> The queue listener. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub AssertMessageQueue(ByVal queue As isr.Core.Services.TraceMessagesQueue)
        Do While Not queue.IsEmpty
            TestInfo.AssertMessage(queue.TryDequeue)
        Loop
    End Sub

#End Region

#Region " TRACE MESSAGES QUEUE COLLECTION "

    Private Shared _TraceMessagesQueues As TraceMessageQueueCollection

    ''' <summary> Gets the collection of trace messages queues. </summary>
    ''' <value> The trace messages queues. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared ReadOnly Property TraceMessagesQueues As TraceMessageQueueCollection
        Get
            If TestInfo._TraceMessagesQueues Is Nothing Then
                TestInfo._TraceMessagesQueues = New TraceMessageQueueCollection
            End If
            Return TestInfo._TraceMessagesQueues
        End Get
    End Property

    ''' <summary> Assert message queue. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub AssertMessageQueue()
        TestInfo.TraceMessagesQueues.AssertMessageQueue()
    End Sub

    ''' <summary> Clears the message queue. </summary>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function ClearMessageQueue() As String
        Return TestInfo.TraceMessagesQueues.ClearMessageQueue()
    End Function

    ''' <summary> Collection of trace message queues. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 4/10/2018 </para></remarks>
    Partial Public Class TraceMessageQueueCollection
        Inherits ObjectModel.Collection(Of isr.Core.Services.TraceMessagesQueue)
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub AssertMessageQueue()
            For Each traceMessageQueue As isr.Core.Services.TraceMessagesQueue In Me
                TestInfo.AssertMessageQueue(traceMessageQueue)
            Next
        End Sub
        ''' <summary> Appends a line. </summary>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Private Shared Sub AppendLine(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then builder.AppendLine(value)
        End Sub
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function ClearMessageQueue() As String
            Dim builder As New System.Text.StringBuilder
            For Each traceMessageQueue As isr.Core.Services.TraceMessagesQueue In Me
                TraceMessageQueueCollection.AppendLine(builder, traceMessageQueue.DequeueContent())
            Next
            Return builder.ToString
        End Function
    End Class

#End Region

#Region " LOCATION "

    ''' <summary> Gets the name of the host. </summary>
    ''' <value> The name of the host. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property HostName As String

    Private Shared _IPv4Address As Net.IPAddress
    ''' <summary> Gets the IP address. </summary>
    ''' <value> The IP address. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property IPv4Address As Net.IPAddress
        Get
            If TestInfo._IPv4Address Is Nothing Then
                TestInfo._HostName = System.Net.Dns.GetHostName()
                For Each value As Net.IPAddress In System.Net.Dns.GetHostEntry(TestInfo.HostName).AddressList
                    If value.AddressFamily = Net.Sockets.AddressFamily.InterNetwork Then
                        TestInfo._IPv4Address = value
                        Exit For
                    End If
                Next
            End If
            Return TestInfo._IPv4Address
        End Get
    End Property

    Private Shared _TestLocation As TestLocation
    ''' <summary> Gets the test location. </summary>
    ''' <value> The test location. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property TestLocation As TestLocation
        Get
            If TestInfo._TestLocation = TestLocation.None Then
                Dim ip As String = TestInfo.IPv4Address.ToString
                If ip.StartsWith(TestInfo.FirstIPv4, StringComparison.OrdinalIgnoreCase) Then
                    TestInfo._TestLocation = TestLocation.First
                ElseIf ip.StartsWith(TestInfo.SecondIPv4, StringComparison.OrdinalIgnoreCase) Then
                    TestInfo._TestLocation = TestLocation.Second
                Else
                    TestInfo._TestLocation = TestLocation.None
                End If
            End If
            Return TestInfo._TestLocation
        End Get
    End Property

    ''' <summary> Gets the IPv4 prefix for the second test site. </summary>
    ''' <value> The Remote IPv4 prefix. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property SecondIPv4 As String
        Get
            Return My.MyAppSettingsReader.AppSettingValue
        End Get
    End Property

    ''' <summary> Gets the IPv4 prefix for the first test site. </summary>
    ''' <value> The first IPv4 prefix. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property FirstIPv4 As String
        Get
            Return My.MyAppSettingsReader.AppSettingValue
        End Get
    End Property

#End Region

#Region " APPLICATION DOMAIN DATA DIRECTORY "

    ''' <summary> The name of the data directory application domain property. </summary>
    Public Const ApplicationDomainDataDirectoryPropertyName As String = "DataDirectory"

    ''' <summary> Modify application domain data directory path. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub ModifyApplicationDomainDataDirectoryPath()
        TestInfo.ModifyApplicationDomainDataDirectoryPath(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))
    End Sub

    ''' <summary> Modify application domain data directory path. </summary>
    ''' <param name="path"> Full pathname of the file. </param>
    ''' <remarks> https://stackoverflow.com/questions/1833640/connection-string-with-relative-path-to-the-database-file </remarks>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub ModifyApplicationDomainDataDirectoryPath(ByVal path As String)
        AppDomain.CurrentDomain.SetData(TestInfo.ApplicationDomainDataDirectoryPropertyName, path)
    End Sub

    ''' <summary> Reads application domain data directory path. </summary>
    ''' <returns> The application domain data directory path. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function ReadApplicationDomainDataDirectoryPath() As String
        Dim value As String = String.Empty
        value = TryCast(AppDomain.CurrentDomain.GetData(TestInfo.ApplicationDomainDataDirectoryPropertyName), String)
        Return If(value, "")
    End Function

#End Region

End Class

''' <summary> Values that represent test locations. </summary>
Friend Enum TestLocation
    None
    ''' <summary> An enum constant representing the first test location option. </summary>
    First
    ''' <summary> An enum constant representing the second test location option. </summary>
    Second
End Enum
