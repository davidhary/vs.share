﻿
Partial Public NotInheritable Class TestInfo

    Private Shared _TraceMessagesQueueListener As isr.Core.Services.TraceMessagesQueueListener

    ''' <summary> Gets the trace message queue listener. </summary>
    ''' <value> The trace message queue listener. </value>
    Public Shared ReadOnly Property TraceMessagesQueueListener As isr.Core.Services.TraceMessagesQueueListener
        Get
            If TestInfo._TraceMessagesQueueListener Is Nothing Then
                TestInfo._TraceMessagesQueueListener = New isr.Core.Services.TraceMessagesQueueListener
                TestInfo._TraceMessagesQueueListener.ApplyTraceLevel(TraceEventType.Warning)
            End If
            Return TestInfo._TraceMessagesQueueListener
        End Get
    End Property

    ''' <summary> Assert message. </summary>
    ''' <param name="traceMessage"> Message describing the trace. </param>
    Private Shared Sub AssertMessage(ByVal traceMessage As isr.Core.Services.TraceMessage)
        If traceMessage Is Nothing Then
        ElseIf traceMessage.EventType = TraceEventType.Warning Then
            TestInfo.TraceMessage($"Warning published: {traceMessage.ToString}")
        ElseIf traceMessage.EventType = TraceEventType.Error Then
            Assert.Fail($"Error published: {traceMessage.ToString}")
        End If
    End Sub

    ''' <summary> Assert message queue. </summary>
    ''' <param name="queue"> The queue listener. </param>
    Private Shared Sub AssertMessageQueue(ByVal queue As isr.Core.Services.TraceMessagesQueue)
        Do While Not queue.IsEmpty
            TestInfo.AssertMessage(queue.TryDequeue)
        Loop
    End Sub

    Private Shared _TraceMessagesQueues As TraceMessageQueueCollection

    ''' <summary> Gets the collection of trace messages queues. </summary>
    ''' <value> The trace messages queues. </value>
    Private Shared ReadOnly Property TraceMessagesQueues As TraceMessageQueueCollection
        Get
            If TestInfo._TraceMessagesQueues Is Nothing Then
                TestInfo._TraceMessagesQueues = New TraceMessageQueueCollection
            End If
            Return TestInfo._TraceMessagesQueues
        End Get
    End Property

    ''' <summary> Assert message queue. </summary>
    Public Shared Sub AssertMessageQueue()
        TestInfo.TraceMessagesQueues.AssertMessageQueue()
    End Sub

    ''' <summary> Clears the message queue. </summary>
    ''' <returns> A String. </returns>
    Public Shared Function ClearMessageQueue() As String
        Return TestInfo.TraceMessagesQueues.ClearMessageQueue()
    End Function

End Class
