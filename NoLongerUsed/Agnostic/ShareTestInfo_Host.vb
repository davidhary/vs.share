﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Partial Friend NotInheritable Class TestInfo

    ''' <summary> Gets the candidate time zones of the test site. </summary>
    ''' <value> The candidate time zones of the test site. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property TimeZones As String
        Get
            Return My.MyAppSettingsReader.AppSettingValue()
        End Get
    End Property

    Private Shared _TimeZone As String

    ''' <summary> Gets the time zone of the test site. </summary>
    ''' <value> The time zone of the test site. </value>
    Public Shared ReadOnly Property TimeZone As String
        Get
            If String.IsNullOrWhiteSpace(TestInfo._TimeZone) Then
                TestInfo._TimeZone = TestInfo.TimeZones.Split(","c)(TestInfo.HostInfoIndex)
            End If
            Return TestInfo._TimeZone
        End Get
    End Property

    ''' <summary> Gets the time zone offset of the first test site. </summary>
    ''' <value> The second time zone offset. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property TimeZoneOffsets As String
        Get
            Return My.MyAppSettingsReader.AppSettingValue()
        End Get
    End Property

    Private Shared _TimeZoneOffset As Integer = Integer.MinValue

    ''' <summary> Gets the time zone offset of the test site. </summary>
    ''' <value> The time zone offset of the test site. </value>
    Public Shared ReadOnly Property TimeZoneOffset As Integer
        Get
            If TestInfo._TimeZoneOffset = Integer.MinValue Then
                TestInfo._TimeZoneOffset = Integer.Parse(TestInfo.TimeZoneOffsets.Split(","c)(TestInfo.HostInfoIndex))
            End If
            Return TestInfo._TimeZoneOffset
        End Get
    End Property

    ''' <summary> Gets the host name of the test site. </summary>
    ''' <value> The host name of the test site. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property HostName As String

    Private Shared _HostAddress As Net.IPAddress
    ''' <summary> Gets the IP address of the test site. </summary>
    ''' <value> The IP address of the test site. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property HostAddress As Net.IPAddress
        Get
            If TestInfo._HostAddress Is Nothing Then
                TestInfo._HostName = System.Net.Dns.GetHostName()
                For Each value As Net.IPAddress In System.Net.Dns.GetHostEntry(TestInfo.HostName).AddressList
                    If value.AddressFamily = Net.Sockets.AddressFamily.InterNetwork Then
                        TestInfo._HostAddress = value
                        Exit For
                    End If
                Next
            End If
            Return TestInfo._HostAddress
        End Get
    End Property

    Private Shared _HostInfoIndex As Integer = -1
    ''' <summary> Gets the index into the host information strings. </summary>
    ''' <value> The index into the host information strings. </value>
    Public Shared ReadOnly Property HostInfoIndex As Integer
        Get
            If _HostInfoIndex < 0 Then
                Dim ip As String = TestInfo.HostAddress.ToString
                Dim i As Integer = -1
                For Each value As String In TestInfo.IPv4Prefixes.Split(","c)
                    i += 1
                    If ip.StartsWith(value, StringComparison.OrdinalIgnoreCase) Then
                        TestInfo._HostInfoIndex = i
                        Exit For
                    End If
                Next
            End If
            Return TestInfo._HostInfoIndex
        End Get
    End Property

    ''' <summary> Gets the IPv4 prefixes for the candidate test sites. </summary>
    ''' <value> The IPv4 prefixes fr the candidate test sites. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property IPv4Prefixes As String
        Get
            Return My.MyAppSettingsReader.AppSettingValue
        End Get
    End Property

End Class

