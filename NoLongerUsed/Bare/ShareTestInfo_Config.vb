﻿Partial Friend NotInheritable Class TestAssist

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Shared ReadOnly Property Exists As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean
        End Get
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Shared ReadOnly Property Verbose As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating of all data are to be used for a test. </summary>
    ''' <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Shared ReadOnly Property All As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean()
        End Get
    End Property

    ''' <summary> True if the test set is enabled. </summary>
    ''' <value> The enabled option. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Shared ReadOnly Property Enabled As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean
        End Get
    End Property

End Class

