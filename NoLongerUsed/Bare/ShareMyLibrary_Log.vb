Imports System.Runtime.CompilerServices
Namespace My

    Public NotInheritable Class MyLibrary

#Region " CUSTOM LOG "

        ''' <summary> Gets the log. </summary>
        ''' <value> The log. </value>
        Public Shared ReadOnly Property Log As Logging.Log
            Get
                Return My.Application.Log
            End Get
        End Property

#End Region

    End Class

    Public Module [LogExtensions]

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <param name="log">      The <see cref="Logging.Log">application
        ''' log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="details">  The message details. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal details As String) As String
            Return LogExtensions.WriteLogEntry(log, severity, My.MyLibrary.TraceEventId, details)
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="details">  The message details. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal id As Integer, ByVal details As String) As String
            If details IsNot Nothing Then
                If log IsNot Nothing Then
                    log.WriteEntry(details, severity, id)
                End If
                Return details
            End If
            Return String.Empty
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <param name="log">      The <see cref="Logging.Log">application
        ''' log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="format">   The message format. </param>
        ''' <param name="args">     Specified the message arguments. </param>
        ''' <returns> The message details or empty. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Return LogExtensions.WriteLogEntry(log, severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
            Return String.Empty
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="format">   The message format. </param>
        ''' <param name="args">     Specified the message arguments. </param>
        ''' <returns> The message details or empty. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal id As Integer,
                                      ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Return LogExtensions.WriteLogEntry(log, severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
            Return String.Empty
        End Function

    End Module

End Namespace

