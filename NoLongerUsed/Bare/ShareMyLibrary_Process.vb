Namespace My

    Public NotInheritable Class MyProcess

#Region " PROCESS "

        Private Shared _currentProcessName As String
        ''' <summary> Gets the current process name. </summary>
        Public Shared ReadOnly Property CurrentProcessName() As String
            Get
                If String.IsNullOrWhiteSpace(MyProcess._currentProcessName) Then
                    MyProcess._currentProcessName = Process.GetCurrentProcess().ProcessName
                End If
                Return MyProcess._currentProcessName
            End Get
        End Property

        ''' <summary> Gets the number of current process threads. </summary>
        ''' <value> The number of current process threads. </value>
        Public Shared ReadOnly Property CurrentProcessThreadCount As Integer
            Get
                Return Process.GetCurrentProcess.Threads.Count
            End Get
        End Property

        ''' <summary> Gets the allocated memory. </summary>
        ''' <value> The allocated memory. </value>
        Public Shared ReadOnly Property AllocatedMemory As Long
            Get
                Return Process.GetCurrentProcess.WorkingSet64
            End Get
        End Property

        ''' <summary> Build loaded module info. </summary>
        ''' <returns> A String. </returns>
        Public Shared Function BuildLoadedModulesInfo() As String
            Dim builder As New System.Text.StringBuilder
            Dim lineBuilder As New System.Text.StringBuilder
            For Each p As Process In Process.GetProcesses
                Try
                    If p.MainModule IsNot Nothing Then
                        For Each m As ProcessModule In p.Modules
                            lineBuilder.Clear()
                            lineBuilder.Append($"{NameOf(ProcessModule.ModuleName)} = {m.ModuleName}")
                            lineBuilder.Append($"; {NameOf(ProcessModule.BaseAddress)} = 0x{m.BaseAddress:X}")
                            lineBuilder.Append($"; {NameOf(ProcessModule.ModuleMemorySize)} = 0x{m.ModuleMemorySize:X}")
                            builder.AppendLine(lineBuilder.ToString)
                        Next
                    End If
                Catch
                Finally
                End Try
            Next
            Return builder.ToString
        End Function

        Public Shared Function BuildLoadedAssembliesInfo() As String
            Dim builder As New System.Text.StringBuilder
            Dim lineBuilder As New System.Text.StringBuilder
            For Each m As Reflection.Assembly In AppDomain.CurrentDomain.GetAssemblies
                lineBuilder.Clear()
                With m.GetName
                    lineBuilder.Append($"{NameOf(.Name)} = { .Name}")
                    lineBuilder.Append($"; {NameOf(.Version)} = { .Version}")
                    lineBuilder.Append($"; {NameOf(.GetPublicKeyToken)} = { .GetPublicKeyToken.Aggregate(String.Empty, Function(current As String, value As Byte) (current & value.ToString("X2")))}")
                    ' lineBuilder.Append($"; {NameOf(.CodeBase)} = { .CodeBase}")

                End With
                builder.AppendLine(lineBuilder.ToString)
                For Each pm As Reflection.Module In m.GetLoadedModules
                    lineBuilder.Clear()
                    lineBuilder.Append($"{NameOf(pm.Name)} = { pm.Name}")
                    lineBuilder.Append($"; {NameOf(pm.ScopeName)} = { pm.ScopeName}")
                    builder.AppendLine(pm.ToString)

                Next
                builder.AppendLine(lineBuilder.ToString)
            Next
            Return builder.ToString
        End Function


#End Region

    End Class

End Namespace

