﻿'---------------------------------------------------------------------------------------------------
' file:		C:\My\LIBRARIES\VS\Share\Bare\ShareMyApplication_Dispatcher.vb
'
' summary:	Share my library dispatcher class
' Requires: C:\My\LIBRARIES\VS\Share\Extensions\DispatcherExtensions_.vb
'           C:\My\LIBRARIES\VS\Share\Extensions\StopwatchExtensions_.vb
'           C:\My\LIBRARIES\VS\Share\Extensions\TimeSpanExtensions_.vb
'---------------------------------------------------------------------------------------------------
Imports System.Windows.Threading
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Lets Windows process all the messages currently in the message queue. </summary>
        Public Shared Sub DoEvents()
            DispatcherExtensions.DoEvents(Dispatcher.CurrentDispatcher)
        End Sub

        ''' <summary>
        ''' Delays operations by waiting for a threading clock task to complete. The delay is preceded
        ''' and followed with

        ''' <see cref="isr.Core.Agnostic.DispatcherExtensions.DoEvents(Dispatcher)"/>
        ''' to releases messages currently in the message queue.
        ''' </summary>
        ''' <param name="delayTime"> The delay time. </param>
        ''' <returns> A TimeSpan. </returns>
        Public Shared Function DoEventsThreadingClockDelay(ByVal delayTime As TimeSpan) As TimeSpan
            Return DispatcherExtensions.DoEventsDelay(Dispatcher.CurrentDispatcher, delayTime, DispatcherExtensions.ClockResolution.ThreadingClock)
        End Function

        ''' <summary>
        ''' Delays operations by waiting for a system clock task to complete. The delay is preceded and
        ''' followed with
        ''' <see cref="isr.Core.Agnostic.DispatcherExtensions.DoEvents(System.Windows.Threading.Dispatcher)"/>
        ''' to releases messages currently in the message queue.
        ''' </summary>
        ''' <param name="delayTime"> The delay time. </param>
        ''' <returns> A TimeSpan. </returns>
        Public Shared Function DoEventsSystemClockDelay(ByVal delayTime As TimeSpan) As TimeSpan
            Return DispatcherExtensions.DoEventsDelay(Dispatcher.CurrentDispatcher, delayTime, DispatcherExtensions.ClockResolution.SystemClock)
        End Function

        ''' <summary>
        ''' Delays operations by waiting for a high resolution task to complete. The delay is preceded
        ''' and followed with
        ''' <see cref="isr.Core.Agnostic.DispatcherExtensions.DoEvents(System.Windows.Threading.Dispatcher)"/>
        ''' to releases messages currently in the message queue.
        ''' </summary>
        ''' <param name="delayTime"> The delay time. </param>
        ''' <returns> A TimeSpan. </returns>
        Public Shared Function DoEventsHighResolutionDelay(ByVal delayTime As TimeSpan) As TimeSpan
            Return DispatcherExtensions.DoEventsDelay(Dispatcher.CurrentDispatcher, delayTime, DispatcherExtensions.ClockResolution.HighResolution)
        End Function

    End Class

End Namespace
