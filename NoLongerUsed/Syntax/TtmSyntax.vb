﻿Namespace TtmSyntax

    Namespace Support

        Public Module SupportSyntax
            ''' <summary> The version query command. </summary>
            Public Const VersionQueryCommand As String = "_G.isr.version()"
        End Module

    End Namespace

    Namespace Meters

        Public Module ThermalTransient
            ''' <summary> The version query command. </summary>
            Public Const VersionQueryCommand As String = "_G.isr.meters.thermalTransient.version()"
        End Module

    End Namespace

End Namespace

