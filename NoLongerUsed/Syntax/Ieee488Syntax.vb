Namespace Ieee488

    Namespace Syntax

        ''' <summary> Defines the standard IEEE488 command set. </summary>
        ''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
        ''' Licensed under The MIT License.</para><para>
        ''' David, 01/15/2005, 1.0.1841.x. </para></remarks>
        Public Module Ieee488Syntax

#Region " IEEE 488.2 STANDARD COMMANDS "

            ''' <summary> Gets the Clear Status (CLS) command. </summary>
            Public Const ClearExecutionStateCommand As String = "*CLS"

            ''' <summary> Gets the Identity query (*IDN?) command. </summary>
            Public Const IdentityQueryCommand As String = "*IDN?"

            ''' <summary> Gets the operation complete (*OPC) command. </summary>
            Public Const OperationCompletedCommand As String = "*OPC"

            ''' <summary> Gets the operation complete query (*OPC?) command. </summary>
            Public Const OperationCompletedQueryCommand As String = "*OPC?"

            ''' <summary> Gets the options query (*OPT?) command. </summary>
            Public Const OptionsQueryCommand As String = "*OPT?"

            ''' <summary> Gets the Wait (*WAI) command. </summary>
            Public Const WaitCommand As String = "*WAI"

            ''' <summary> Gets the Standard Event Enable (*ESE) command. </summary>
            Public Const StandardEventEnableCommandFormat As String = "*ESE {0:D}"

            ''' <summary> Gets the Standard Event Enable query (*ESE?) command. </summary>
            Public Const StandardEventEnableQueryCommand As String = "*ESE?"

            ''' <summary> Gets the Standard Event Enable (*ESR?) command. </summary>
            Public Const StandardEventQueryCommand As String = "*ESR?"

            ''' <summary> Gets the Service Request Enable (*SRE) command. </summary>
            Public Const ServiceRequestEnableCommandFormat As String = "*SRE {0:D}"

            ''' <summary> Gets the Standard Event and Service Request Enable '*CLS; *ESE {0:D}; *SRE {1:D}' command format. </summary>
            Public Const StandardServiceEnableCommandFormat As String = "*CLS; *ESE {0:D}; *SRE {1:D}"

            ''' <summary> Gets the Standard Event and Service Request Enable '*CLS; *ESE {0:D}; *SRE {1:D} *OPC' command format. </summary>
            Public Const StandardServiceEnableCompleteCommandFormat As String = "*CLS; *ESE {0:D}; *SRE {1:D}; *OPC"

            ''' <summary> Gets the Service Request Enable query (*SRE?) command. </summary>
            Public Const ServiceRequestEnableQueryCommand As String = "*SRE?"

            ''' <summary> Gets the Service Request Status query (*STB?) command. </summary>
            Public Const ServiceRequestQueryCommand As String = "*STB?"

            ''' <summary> Gets the reset to know state (*RST) command. </summary>
            Public Const ResetKnownStateCommand As String = "*RST"

#End Region

        End Module

    End Namespace

End Namespace
