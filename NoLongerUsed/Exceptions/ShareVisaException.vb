﻿Imports NationalInstruments
Imports System.ComponentModel

''' <summary> Handles General VISA exceptions. </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 11/7/2013, ">            Updated from TSP Library. 
''' David, 12/25/2010, 1.0.4011.x. </para></remarks>
<Serializable()> Public Class VisaException
    Inherits ExceptionBase

#Region " STANDARD CONSTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="VisaException"/> class. </summary>
    Public Sub New()
        Me.New("VISA Exception")
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="VisaException" /> class. </summary>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="VisaException" /> class. </summary>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:System.Exception" /> class with
    ''' serialized data. </summary>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    ''' that holds the serialized object data about the exception being thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    ''' that contains contextual information about the source or destination. </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

#Region " CUSTOM CONSTRUCTORS "

    ''' <summary> Constructor specifying the Message to be set. </summary>
    ''' <param name="visaStatus">   Specifies the VISA status code. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="format">       Specifies the exception message format. </param>
    ''' <param name="args">         Specifies the report argument. </param>
    Public Sub New(ByVal visaStatus As Integer, ByVal resourceName As String, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(resourceName, VisaException.BuildVisaStatusDetails(visaStatus), format, args)
    End Sub

    ''' <summary> Constructor specifying the Message to be set. </summary>
    ''' <param name="resourceName">          Name of the resource. </param>
    ''' <param name="lastVisaStatusDetails"> The last visa status details. </param>
    ''' <param name="format">                Specifies the exception message format. </param>
    ''' <param name="args">                  Specifies the report argument. </param>
    Public Sub New(ByVal resourceName As String, ByVal lastVisaStatusDetails As String,
                   ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture,
                                 "{0} Visa I/O Failed; {1}. Visa error: {2}",
                                 resourceName,
                                 String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                 lastVisaStatusDetails))
    End Sub

    ''' <summary> Constructor specifying the Message to be set. </summary>
    ''' <param name="visaStatus">   Specifies the VISA status code. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="nodeNumber">   The node number. </param>
    ''' <param name="format">       Specifies the exception message format. </param>
    ''' <param name="args">         Specifies the report argument. </param>
    Public Sub New(ByVal visaStatus As Integer, ByVal resourceName As String, ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(resourceName, nodeNumber, VisaException.BuildVisaStatusDetails(visaStatus), format, args)
    End Sub

    ''' <summary> Constructor specifying the Message to be set. </summary>
    ''' <param name="resourceName">          Name of the resource. </param>
    ''' <param name="nodeNumber">            The node number. </param>
    ''' <param name="lastVisaStatusDetails"> The last visa status details. </param>
    ''' <param name="format">                Specifies the exception message format. </param>
    ''' <param name="args">                  Specifies the report argument. </param>
    Public Sub New(ByVal resourceName As String, ByVal nodeNumber As Integer, ByVal lastVisaStatusDetails As String,
                   ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture,
                                 "{0} node {1} Visa I/O Failed; {2}. Visa error: {3}",
                                 resourceName, nodeNumber,
                                 String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                 lastVisaStatusDetails))
    End Sub

#End Region

#Region " VISA STATUS "

    ''' <summary> Builds a VISA warning or error message. </summary>
    ''' <param name="lastVisaAction"> The last visa action. </param>
    ''' <param name="value">          The <see cref="VisaNS.VisaStatusCode">status code</see> from the
    ''' last operation. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function BuildVisaStatusDetails(ByVal lastVisaAction As String, ByVal value As Integer) As String
        Return BuildVisaStatusDetails(lastVisaAction, CType(value, VisaNS.VisaStatusCode))
    End Function

    ''' <summary> Builds a VISA warning or error message. </summary>
    ''' <param name="lastVisaAction"> The last visa action. </param>
    ''' <param name="value">          The <see cref="VisaNS.VisaStatusCode">status code</see> from the
    ''' last operation. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function BuildVisaStatusDetails(ByVal lastVisaAction As String, ByVal value As VisaNS.VisaStatusCode) As String
        If value = 0 Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0} OK.", lastVisaAction)
        Else
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "{0} {1}.", lastVisaAction, BuildVisaStatusDetails(value))
        End If
    End Function

    ''' <summary> Builds a VISA warning or error message. </summary>
    ''' <param name="value"> The <see cref="VisaNS.VisaStatusCode">status code</see> from the last
    ''' operation. </param>
    ''' <returns> The visa status details. </returns>
    Public Shared Function BuildVisaStatusDetails(ByVal value As Integer) As String

        Return BuildVisaStatusDetails(CType(value, VisaNS.VisaStatusCode))

    End Function

    ''' <summary> Builds a VISA warning or error message. This method does not require additional
    ''' information from the device. </summary>
    ''' <param name="value"> The <see cref="VisaNS.VisaStatusCode">status code</see> from the last
    ''' operation. </param>
    ''' <returns> The visa status details. </returns>
    Public Shared Function BuildVisaStatusDetails(ByVal value As VisaNS.VisaStatusCode) As String

        If value = 0 Then
            Return "OK"
        Else
            Dim visaMessage As New System.Text.StringBuilder
            If value > 0 Then
                visaMessage.Append("VISA Warning")
            Else
                visaMessage.Append("VISA Error")
            End If
            Dim description As String = value.ToString()
            Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(description)
            Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

            If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                description = attributes(0).Description
            End If

            If String.Equals(value.ToString, description, StringComparison.CurrentCultureIgnoreCase) Then
                visaMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, ": {0}.", value)
            Else
                visaMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, " {0}: {1}.", value, description)
            End If
            Return visaMessage.ToString
        End If

    End Function

#End Region

End Class

