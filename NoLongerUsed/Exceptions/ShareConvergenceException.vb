﻿Imports System.Runtime.Serialization
''' <summary> The exception that is thrown when an algorithm fails to converge. </summary>
''' <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
<Serializable()>
Public Class ConvergenceException
    Inherits Exception

    ''' <summary> Initializes a new non-convergence exception. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new non-convergence exception with the given exception message. </summary>
    ''' <param name="message"> The exception message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Initializes a new non-convergence exception with the given exception message and
    ''' inner exception. </summary>
    ''' <param name="message">        The exception message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new non-convergence exception with the given serialization information
    ''' and streaming context. </summary>
    ''' <param name="info">    The serialization information. </param>
    ''' <param name="context"> The streaming context. </param>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class

