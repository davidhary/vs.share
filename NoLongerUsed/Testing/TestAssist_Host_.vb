﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Partial Friend NotInheritable Class TestAssist

    ''' <summary> Gets the candidate time zones of the test site. </summary>
    ''' <value> The candidate time zones of the test site. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property TimeZones As String
        Get
            Return My.AppSettingsReader.AppSettingValue()
        End Get
    End Property

    Private _TimeZone As String

    ''' <summary> Gets the time zone of the test site. </summary>
    ''' <value> The time zone of the test site. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property TimeZone As String
        Get
            If String.IsNullOrWhiteSpace(Me._TimeZone) Then
                Me._TimeZone = TestInfo.TimeZones.Split(","c)(TestInfo.HostInfoIndex)
            End If
            Return Me._TimeZone
        End Get
    End Property

    ''' <summary> Gets the time zone offset of the first test site. </summary>
    ''' <value> The second time zone offset. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property TimeZoneOffsets As String
        Get
            Return My.AppSettingsReader.AppSettingValue()
        End Get
    End Property

    Private _TimeZoneOffset As Double = Double.MinValue

    ''' <summary> Gets the time zone offset of the test site. </summary>
    ''' <value> The time zone offset of the test site. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property TimeZoneOffset As Double
        Get
            If Me._TimeZoneOffset = Double.MinValue Then
                Me._TimeZoneOffset = Double.Parse(TestInfo.TimeZoneOffsets.Split(","c)(TestInfo.HostInfoIndex))
            End If
            Return Me._TimeZoneOffset
        End Get
    End Property

    ''' <summary> Gets the host name of the test site. </summary>
    ''' <value> The host name of the test site. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property HostName As String

    Private _HostAddress As Net.IPAddress
    ''' <summary> Gets the IP address of the test site. </summary>
    ''' <value> The IP address of the test site. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property HostAddress As Net.IPAddress
        Get
            If Me._HostAddress Is Nothing Then
                Me._HostName = System.Net.Dns.GetHostName()
                For Each value As Net.IPAddress In System.Net.Dns.GetHostEntry(TestInfo.HostName).AddressList
                    If value.AddressFamily = Net.Sockets.AddressFamily.InterNetwork Then
                        Me._HostAddress = value
                        Exit For
                    End If
                Next
            End If
            Return Me._HostAddress
        End Get
    End Property

    Private _HostInfoIndex As Integer = -1
    ''' <summary> Gets the index into the host information strings. </summary>
    ''' <value> The index into the host information strings. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property HostInfoIndex As Integer
        Get
            If _HostInfoIndex < 0 Then
                Dim ip As String = TestInfo.HostAddress.ToString
                Dim i As Integer = -1
                For Each value As String In TestInfo.IPv4Prefixes.Split(","c)
                    i += 1
                    If ip.StartsWith(value, StringComparison.OrdinalIgnoreCase) Then
                        Me._HostInfoIndex = i
                        Exit For
                    End If
                Next
            End If
            Return Me._HostInfoIndex
        End Get
    End Property

    ''' <summary> Gets the IPv4 prefixes for the candidate test sites. </summary>
    ''' <value> The IPv4 prefixes fr the candidate test sites. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property IPv4Prefixes As String
        Get
            Return My.AppSettingsReader.AppSettingValue
        End Get
    End Property

End Class

