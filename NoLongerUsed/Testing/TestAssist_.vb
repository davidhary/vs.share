﻿'---------------------------------------------------------------------------------------------------
' file:		C:\My\LIBRARIES\VS\Share\Testing\TestAssist_.vb
'
' summary:	Test assist class
' Requires: isr.Core.Services.DLL
'---------------------------------------------------------------------------------------------------
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Services
''' <summary> Assists in with test tasks. </summary>
''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/13/2017 </para></remarks>
<CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")>
Partial Friend NotInheritable Class TestAssist

#Region " SINGLETON "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Gets the locking object to enforce thread safety when creating the singleton
    ''' instance. </summary>
    ''' <value> The sync locker. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Property _Instance As TestAssist

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function [Get]() As TestAssist
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = New TestAssist
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " TEST CONFIGURATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public ReadOnly Property Exists As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean
        End Get
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public ReadOnly Property Verbose As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating of all data are to be used for a test. </summary>
    ''' <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property All As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean()
        End Get
    End Property

    ''' <summary> True if the test set is enabled. </summary>
    ''' <value> The enabled option. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public ReadOnly Property Enabled As Boolean
        Get
            Return My.AppSettingsReader.AppSettingBoolean
        End Get
    End Property

#End Region

#Region " VALIDATIONS "

    ''' <summary> Validated test context. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <returns> A TestContext. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Function ValidatedTestContext(ByVal testContext As TestContext) As TestContext
        If testContext Is Nothing Then Throw New ArgumentNullException(NameOf(testContext))
        Return testContext
    End Function

#End Region

#Region " TRACE "

    ''' <summary> Initializes the trace listener. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub InitializeTraceListener()
        Me.ReplaceTraceListener()
        Console.Out.WriteLine(My.Application.Log.DefaultFileLogWriter.FullLogFileName)
    End Sub

    ''' <summary> Replace trace listener. </summary>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub ReplaceTraceListener()
        With My.Application.Log
            .TraceSource.Listeners.Remove(DefaultFileLogTraceListener.DefaultFileLogWriterName)
            .TraceSource.Listeners.Add(DefaultFileLogTraceListener.CreateListener(False))
            .TraceSource.Switch.Level = SourceLevels.Verbose
        End With
    End Sub

    ''' <summary> Trace message. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub TraceMessage(ByVal format As String, ByVal ParamArray args() As Object)
        Me.TraceMessage(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Trace message. </summary>
    ''' <param name="message"> The message. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub TraceMessage(ByVal message As String)
        My.Application.Log.WriteEntry(message)
        'System.Diagnostics.Debug.WriteLine(message)
        Console.Out.WriteLine(message)
    End Sub

    ''' <summary> Verbose message. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub VerboseMessage(ByVal format As String, ByVal ParamArray args() As Object)
        If Me.Verbose Then TraceMessage(format, args)
    End Sub

#End Region

#Region " TRACE MESSAGES QUEUE "

    Private _TraceMessagesQueueListener As TraceMessagesQueueListener

    ''' <summary> Gets the trace message queue listener. </summary>
    ''' <value> The trace message queue listener. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property TraceMessagesQueueListener As TraceMessagesQueueListener
        Get
            If Me._TraceMessagesQueueListener Is Nothing Then
                Me._TraceMessagesQueueListener = New TraceMessagesQueueListener
                Me._TraceMessagesQueueListener.ApplyTraceLevel(TraceEventType.Warning)
            End If
            Return Me._TraceMessagesQueueListener
        End Get
    End Property

    ''' <summary> Assert message. </summary>
    ''' <param name="traceMessage"> Message describing the trace. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub AssertMessage(ByVal traceMessage As TraceMessage)
        If traceMessage Is Nothing Then
        ElseIf traceMessage.EventType = TraceEventType.Warning Then
            Me.TraceMessage($"Warning published: {traceMessage.ToString}")
        ElseIf traceMessage.EventType = TraceEventType.Error Then
            Assert.Fail($"Error published: {traceMessage.ToString}")
        End If
    End Sub

    ''' <summary> Assert message queue. </summary>
    ''' <param name="queue"> The queue listener. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub AssertMessageQueue(ByVal queue As TraceMessagesQueue)
        Do While Not queue.IsEmpty
            Me.AssertMessage(queue.TryDequeue)
        Loop
    End Sub

#End Region

#Region " TRACE MESSAGES QUEUE COLLECTION "

    Private _TraceMessagesQueues As TraceMessageQueueCollection

    ''' <summary> Gets the collection of trace messages queues. </summary>
    ''' <value> The trace messages queues. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private ReadOnly Property TraceMessagesQueues As TraceMessageQueueCollection
        Get
            If Me._TraceMessagesQueues Is Nothing Then
                Me._TraceMessagesQueues = New TraceMessageQueueCollection
            End If
            Return Me._TraceMessagesQueues
        End Get
    End Property

    ''' <summary> Assert message queue. THis clears the queues. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub AssertMessageQueue()
        Me.TraceMessagesQueues.AssertMessageQueue(Me)
    End Sub

    ''' <summary> Clears the message queue. </summary>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Function ClearMessageQueue() As String
        Return Me.TraceMessagesQueues.ClearMessageQueue()
    End Function

    ''' <summary> Collection of trace message queues. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 4/10/2018 </para></remarks>
    Partial Public Class TraceMessageQueueCollection
        Inherits ObjectModel.Collection(Of TraceMessagesQueue)

        ''' <summary> Query if this object has queued messages. </summary>
        ''' <returns> <c>true</c> if queued messages; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function HasQueuedMessages() As Boolean
            Dim result As Boolean = False
            For Each traceMessageQueue As TraceMessagesQueue In Me
                result = result AndAlso traceMessageQueue.Any
            Next
            Return result
        End Function

        ''' <summary> Count queued messages. </summary>
        ''' <returns> The total number of queued messages. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function CountQueuedMessages() As Integer
            Dim result As Integer = 0
            For Each traceMessageQueue As TraceMessagesQueue In Me
                result += traceMessageQueue.Count
            Next
            Return result
        End Function

        ''' <summary> Assert message queue. </summary>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub AssertMessageQueue(ByVal testAssist As TestAssist)
            For Each traceMessageQueue As TraceMessagesQueue In Me
                testAssist.AssertMessageQueue(traceMessageQueue)
            Next
        End Sub
        ''' <summary> Appends a line. </summary>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Private Shared Sub AppendLine(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then builder.AppendLine(value)
        End Sub

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function ClearMessageQueue() As String
            Dim builder As New System.Text.StringBuilder
            For Each traceMessageQueue As TraceMessagesQueue In Me
                TraceMessageQueueCollection.AppendLine(builder, traceMessageQueue.DequeueContent())
            Next
            Return builder.ToString
        End Function
    End Class

#End Region

#Region " APPLICATION DOMAIN DATA DIRECTORY "

    ''' <summary> The name of the data directory application domain property. </summary>
    Public Const ApplicationDomainDataDirectoryPropertyName As String = "DataDirectory"

    ''' <summary> Modify application domain data directory path. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub ModifyApplicationDomainDataDirectoryPath()
        Me.ModifyApplicationDomainDataDirectoryPath(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))
    End Sub

    ''' <summary> Modify application domain data directory path. </summary>
    ''' <param name="path"> Full pathname of the file. </param>
    ''' <remarks> https://stackoverflow.com/questions/1833640/connection-string-with-relative-path-to-the-database-file </remarks>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub ModifyApplicationDomainDataDirectoryPath(ByVal path As String)
        AppDomain.CurrentDomain.SetData(TestAssist.ApplicationDomainDataDirectoryPropertyName, path)
    End Sub

    ''' <summary> Reads application domain data directory path. </summary>
    ''' <returns> The application domain data directory path. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Function ReadApplicationDomainDataDirectoryPath() As String
        Dim value As String = String.Empty
        value = TryCast(AppDomain.CurrentDomain.GetData(TestAssist.ApplicationDomainDataDirectoryPropertyName), String)
        Return If(value, "")
    End Function

#End Region

End Class

''' <summary> A test information property module. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Friend Module TestInfoProperty
    Friend ReadOnly Property TestInfo() As TestAssist
        Get
            Return TestAssist.Get
        End Get
    End Property
End Module

