Imports isr.Core.Forma
Imports System.Windows.Forms
Imports Microsoft.VisualBasic.ApplicationServices

Namespace My

    Partial Friend Class MyApplication

#Region " APPLICATION EXTENSIONS "

        Private _MyApplicationInfo As MyAssemblyInfo

        ''' <summary> Gets an object that provides information about the application's assembly. </summary>
        ''' <value> The assembly information object. </value>
        Public Overloads ReadOnly Property Info As MyAssemblyInfo
            Get
                If Me._myApplicationInfo Is Nothing Then
                    Me._myApplicationInfo = New MyAssemblyInfo(MyBase.Info)
                End If
                Return Me._myApplicationInfo
            End Get
        End Property

        Private Shared _currentProcessName As String
        ''' <summary> Gets the current process name. </summary>
        Public Shared ReadOnly Property CurrentProcessName() As String
            Get
                If String.IsNullOrWhiteSpace(MyApplication._currentProcessName) Then
                    _currentProcessName = Process.GetCurrentProcess().ProcessName
                End If
                Return _currentProcessName
            End Get
        End Property

        ''' <summary> Gets the number of current process threads. </summary>
        ''' <value> The number of current process threads. </value>
        Public Shared ReadOnly Property CurrentProcessThreadCount As Integer
            Get
                Return Process.GetCurrentProcess.Threads.Count
            End Get
        End Property

        ''' <summary> Gets a value indicating whether the application is running under the IDE in design
        ''' mode. </summary>
        ''' <value> <c>True</c> if the application is running under the IDE in design mode; otherwise,
        ''' <c>False</c>. </value>
        Public Shared ReadOnly Property InDesignMode() As Boolean
            Get
                Return Debugger.IsAttached
            End Get
        End Property

#End Region

#Region " SPLASH TRACE "

        ''' <summary> Traces the event and displays on the splash screen if exists. </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="format">    The details. </param>
        ''' <param name="args">      A variable-length parameters list containing arguments. </param>
        Private Sub SplashTraceEvent(ByVal eventType As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
            Me.SplashTraceEvent(eventType, MyApplication.TraceEventId,
                                String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Sub

        ''' <summary> Traces the event and displays on the splash screen if exists. </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="details">   The details. </param>
        Private Sub SplashTraceEvent(ByVal eventType As TraceEventType, ByVal details As String)
            Me.SplashTraceEvent(eventType, MyApplication.TraceEventId, details)
        End Sub

        ''' <summary> Traces the event and displays on the splash screen if exists. </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="id">        The identifier. </param>
        ''' <param name="details">   The details. </param>
        Private Sub SplashTraceEvent(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String)
            MySplashScreen.SplashMessage(details)
            Me.MyLog.TraceSource.TraceEvent(eventType, id, details)
        End Sub

#End Region

#Region " APPLICATION EVENTS "

        ''' <summary> Occurs when the network connection is connected or disconnected. </summary>
        ''' <param name="sender"> The source of the event. </param>
        ''' <param name="e">      Network available event information. </param>
        Private Sub HandlesNetworkAvailabilityChanged(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs) Handles Me.NetworkAvailabilityChanged
        End Sub

#End Region

#Region " APPLICATION OVERRIDE EVENTS "

        ''' <summary> Gets the log. </summary>
        ''' <value> The log. </value>
        Public ReadOnly Property MyLog As MyLog = New MyLog(My.MyApplication.AssemblyProduct)

        ''' <summary> Applies the trace level described by value. </summary>
        ''' <param name="value"> The value. </param>
        Public Sub ApplyTraceLevel(ByVal value As TraceEventType)
            Me.MyLog.ApplyTraceLevel(value)
        End Sub

        ''' <summary> Sets the visual styles, text display styles, and current principal for the main
        ''' application thread (if the application uses Windows authentication), and initializes the
        ''' splash screen, if defined. Replaces the default trace listener with the modified listener.
        ''' Updates the minimum splash screen display time. </summary>
        ''' <param name="commandLineArgs"> A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection" /> of String, 
        '''                                containing the command-line arguments as strings for the current application. </param>
        ''' <returns> A <see cref="T:System.Boolean" /> indicating if application startup should continue. </returns>
        Protected Overrides Function OnInitialize(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Me.Apply(New MyLog(My.MyApplication.AssemblyProduct))
            Dim listener As Microsoft.VisualBasic.Logging.FileLogTraceListener
            With Me.MyLog
                listener = .ReplaceDefaultTraceListener(UserLevel.AllUsers)
                If Not .LogFileExists Then
                    .TraceSource.TraceEventOverride(TraceEventType.Information, My.MyApplication.TraceEventId, My.Application.Info.IsoShortCaption)
                End If
                .TraceSource.TraceEventOverride(TraceEventType.Information, My.MyApplication.TraceEventId, "Process {0} initialized", MyApplication.CurrentProcessName)
            End With

            ' set the log for the application
            With My.Application.Log
                .TraceSource.Listeners.Remove(DefaultFileLogTraceListener.DefaultFileLogWriterName)
                .TraceSource.Listeners.Add(listener)
                .TraceSource.Switch.Level = SourceLevels.Verbose
            End With

            ' applies the default trace level.
            Me.ApplyTraceLevel()

            Return MyBase.OnInitialize(commandLineArgs)

        End Function

        ''' <summary>
        ''' When overridden in a derived class, allows a designer to emit code that initializes the
        ''' splash screen.
        ''' </summary>
        Protected Overrides Sub OnCreateSplashScreen()
            Me.MinimumSplashScreenDisplayTime = My.Settings.MinimumSplashScreenDisplayMilliseconds
            Me.SplashScreen = MySplashScreen
            MySplashScreen.CreateInstance(My.Application.SplashScreen)
            Me.SplashTraceEvent(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Allowing library use of splash screen")
        End Sub

        ''' <summary> Handles the Shutdown event of the MyApplication control. Saves user settings for all
        ''' related libraries. </summary>
        ''' <remarks> This event is not raised if the application terminates abnormally. Application log is
        ''' set at verbose level to log shut down operations. </remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Sub OnShutdown()
            My.Application.SaveMySettingsOnExit = True
            ' Save library settings here
            Me.processShutDown()
            Try
                If My.Application.SaveMySettingsOnExit Then
                    Me.MyLog.TraceSource.TraceEventOverride(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Saving assembly settings")
                    My.Settings.Save()
                End If
                Me.MyLog.TraceSource.Flush()
            Catch
            Finally
            End Try
            Try
                MySplashScreen.Close()
                MySplashScreen.Dispose()
                Me.SplashScreen = Nothing
            Catch
            Finally
                MyBase.OnShutdown()
            End Try
        End Sub

        ''' <summary> Occurs when the application starts, before the startup form is created. </summary>
        ''' <param name="eventArgs">      Startup event information. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Function OnStartup(eventArgs As StartupEventArgs) As Boolean

            If eventArgs Is Nothing Then eventArgs = New StartupEventArgs(New ObjectModel.ReadOnlyCollection(Of String)(New String() {}))

            ' Turn on the screen hourglass
            Cursor.Current = Cursors.AppStarting
            Windows.Forms.Application.DoEvents()

            Try

                Cursor.Current = Cursors.AppStarting

                Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

                Me.ProcessStartup(eventArgs)

                If eventArgs.Cancel Then

                    ' Show the exception message box with three custom buttons.
                    Cursor.Current = Cursors.Default
                    If isr.Core.MyMessageBox.ShowDialogIgnoreExit(Nothing, "Failed parsing command line.",
                                                         "Failed Starting Program", MessageBoxIcon.Stop) = DialogResult.OK Then
                        Me.SplashTraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId,
                                            "Application aborted by the user because of failure to parse the command line.")
                        eventArgs.Cancel = True
                    Else
                        eventArgs.Cancel = False
                    End If
                    Cursor.Current = Cursors.AppStarting

                End If

                If Not eventArgs.Cancel Then
                    eventArgs.Cancel = Not Me.TryinitializeKnownState()
                    If eventArgs.Cancel Then
                        isr.Core.MyMessageBox.ShowDialogExit(Nothing, String.Format(
                                                    "Failed initializing application state. Check the program log at '{0}' for additional information.",
                                                     Me.MyLog?.FullLogFileName), "Failed Starting Program", MessageBoxIcon.Stop)
                    End If
                End If

                If eventArgs.Cancel Then
                    Me.SplashTraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, "Application failed to start up.")
                    Me.MyLog.TraceSource.Flush()

                    ' exit with an error code
                    Environment.Exit(-1)
                    Windows.Forms.Application.Exit()

                ElseIf MySplashScreen.IsCloseRequested Then
                    Me.SplashTraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, "User close requested.")
                    Me.MyLog.TraceSource.Flush()

                    ' exit with an error code
                    Environment.Exit(-1)
                    Windows.Forms.Application.Exit()

                Else
                    Me.SplashTraceEvent(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Loading application window...")
                End If

            Catch ex As Exception

                Me.SplashTraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, "Exception occurred starting application.")
                Cursor.Current = Cursors.Default
                Dim owner As IWin32Window = Nothing
                Me.MyLog.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
                ex.Data.Add("@isr", "Exception occurred starting this application")
                If DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(owner, ex, MessageBoxIcon.Error) Then
                    ' exit with an error code
                    Environment.Exit(-1)
                    Windows.Forms.Application.Exit()
                End If

            Finally

                Cursor.Current = Cursors.Default
                Trace.CorrelationManager.StopLogicalOperation()

            End Try
            Return MyBase.OnStartup(eventArgs)

        End Function

        ''' <summary> Occurs when launching a single-instance application and the application is already
        ''' active. </summary>
        ''' <param name="eventArgs">      Startup next instance event information. </param>
        Protected Overrides Sub OnStartupNextInstance(eventArgs As StartupNextInstanceEventArgs)
            Me.SplashTraceEvent(TraceEventType.Information, My.MyApplication.TraceEventId, "Application next instant starting.")
            MyBase.OnStartupNextInstance(eventArgs)
        End Sub

        ''' <summary> When overridden in a derived class, allows for code to run when an unhandled
        ''' exception occurs in the application. </summary>
        ''' <param name="e"> <see cref="T:Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs" />. </param>
        ''' <returns> A <see cref="T:System.Boolean" /> that indicates whether the
        ''' <see cref="E:Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase.UnhandledException" />
        ''' event was raised. </returns>
        Protected Overrides Function OnUnhandledException(e As Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs) As Boolean

            Dim returnedValue As Boolean = True
            If e Is Nothing Then
                Debug.Assert(Not Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing.")
                Return MyBase.OnUnhandledException(e)
            End If

            Try
                Me.MyLog.DefaultFileLogWriter.Flush()
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception occurred flushing the log", "Exception occurred flushing the log: {0}", ex)
            End Try

            Try
                Dim owner As IWin32Window = Nothing
                e.Exception.Data.Add("@isr", "Unhandled Exception Occurred.")
                Me.MyLog.TraceSource.TraceEvent(e.Exception, My.MyApplication.TraceEventId)
                If DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(owner, e.Exception, MessageBoxIcon.Error) Then
                    ' exit with an error code
                    Environment.Exit(-1)
                    Windows.Forms.Application.Exit()
                End If
            Catch
                If MessageBox.Show(e.Exception.ToString, "Unhandled Exception occurred.",
                                   MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error,
                                   MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly) = Windows.Forms.DialogResult.Abort Then
                End If
            Finally
            End Try
            Return returnedValue

        End Function

#End Region

    End Class

End Namespace
