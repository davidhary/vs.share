Imports System.ComponentModel
Namespace Global.isr

    ''' <summary> A trace event constants. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Friend Module TraceEventConstants

        ''' <summary> The application type scale factor. </summary>
        Public Const BaseScaleFactor As Integer = &H10
        Public Const LibraryDigitMask As Integer = &H1 * BaseScaleFactor
        Public Const FormApplicationDigitMask As Integer = &H2 * BaseScaleFactor
        Public Const UnitTestDigitMask As Integer = &H3 * BaseScaleFactor
        Public Const NamespaceScaleFactor As Integer = &H10 * BaseScaleFactor
        Public Const NamespaceMask As Integer = &HFF * NamespaceScaleFactor

    End Module

End Namespace
