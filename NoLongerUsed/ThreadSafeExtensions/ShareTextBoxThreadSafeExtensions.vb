﻿Imports System.Runtime.CompilerServices
Namespace TextBoxExtensions
    ''' <summary> Includes extensions for <see cref="TextBox">Text Box</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Sets the <see cref="TextBoxBase">control</see>
        ''' <see cref="TextBoxBase.MaxLength">maximum length</see> value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The <see cref="TextBoxBase">control</see>. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeMaxLengthSetter(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBoxBase, Integer)(AddressOf TextBoxExtensions.SafeMaxLengthSetter), New Object() {control, value})
                Else
                    control.MaxLength = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">text box</see> read-only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCausesValidationSetter(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBoxBase, Boolean)(AddressOf TextBoxExtensions.SafeCausesValidationSetter), New Object() {control, value})
                Else
                    control.CausesValidation = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">text box</see> read-only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeReadOnlySetter(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBoxBase, Boolean)(AddressOf TextBoxExtensions.SafeReadOnlySetter), New Object() {control, value})
                Else
                    control.ReadOnly = value
                End If
            End If
            Return value
        End Function

    End Module
End Namespace
