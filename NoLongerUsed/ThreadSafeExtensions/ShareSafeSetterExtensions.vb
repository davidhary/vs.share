﻿Imports System.Runtime.CompilerServices
Namespace SafeSetterExtensions
    ''' <summary> Includes extensions for controls. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " DRAW TEXT "

        ''' <summary> Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>
        ''' center middle using the control font. </summary>
        ''' <remarks> Call this method after changing the progress bar's value. If for some reason, the
        ''' changing of the progress bar's value doesn't refresh it and clear the previously drawn text,
        ''' call the Refresh method of the progress bar before calling this method. </remarks>
        ''' <param name="control"> The target progress bar to add text into. </param>
        ''' <param name="value">   The text to add into the progress bar. Leave null or empty to
        ''' automatically add the percent. </param>
        <Extension()>
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.Control, ByVal value As String)
            If control IsNot Nothing Then
                SafelyDrawText(control, value, Drawing.ContentAlignment.MiddleCenter, control.Font)
            End If
        End Sub

        ''' <summary> Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>. </summary>
        ''' <remarks> Call this method after changing the progress bar's value. If for some reason, the
        ''' changing of the progress bar's value doesn't refresh it and clear the previously drawn text,
        ''' call the Refresh method of the progress bar before calling this method. </remarks>
        ''' <param name="control">   The target progress bar to add text into. </param>
        ''' <param name="value">     The text to add into the progress bar. Leave null or empty to
        ''' automatically add the percent. </param>
        ''' <param name="textAlign"> Where the text is to be placed. </param>
        <Extension()>
        Public Sub SafelyDrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment)
            If control IsNot Nothing Then
                SafelyDrawText(control, value, textAlign, control.Font)
            End If
        End Sub

        ''' <summary> Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>. </summary>
        ''' <remarks> Call this method after changing the progress bar's value. If for some reason, the
        ''' changing of the progress bar's value doesn't refresh it and clear the previously drawn text,
        ''' call the Refresh method of the progress bar before calling this method. </remarks>
        ''' <param name="control">   The target progress bar to add text into. </param>
        ''' <param name="value">     The text to add into the progress bar. Leave null or empty to
        ''' automatically add the percent. </param>
        ''' <param name="textAlign"> Where the text is to be placed. </param>
        ''' <param name="textFont">  The font the text should be drawn in. </param>
        <Extension()>
        Public Sub SafelyDrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment,
                                  ByVal textFont As System.Drawing.Font)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, String, Drawing.ContentAlignment, Drawing.Font)(AddressOf Methods.SafelyDrawText), New Object() {control, value, textAlign, textFont})
                Else
                    Using gr As Drawing.Graphics = control.CreateGraphics()
                        Const margin As Single = 0
                        Dim x As Single = margin
                        Dim y As Single = (control.Height - gr.MeasureString(value, textFont).Height) / 2.0F
                        If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.MiddleCenter OrElse textAlign = Drawing.ContentAlignment.TopCenter Then
                            x = (control.Width - gr.MeasureString(value, textFont).Width) / 2.0F
                        ElseIf textAlign = Drawing.ContentAlignment.BottomRight OrElse textAlign = Drawing.ContentAlignment.MiddleRight OrElse textAlign = Drawing.ContentAlignment.TopRight Then
                            x = control.Width - gr.MeasureString(value, textFont).Width - margin
                        End If
                        If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.BottomLeft OrElse textAlign = Drawing.ContentAlignment.BottomRight Then
                            y = control.Height - gr.MeasureString(value, textFont).Height - margin
                        ElseIf textAlign = Drawing.ContentAlignment.TopCenter OrElse textAlign = Drawing.ContentAlignment.TopLeft OrElse textAlign = Drawing.ContentAlignment.TopRight Then
                            y = margin
                        End If
                        y = Math.Max(y, margin)
                        x = Math.Max(x, margin)
                        Using sb As New Drawing.SolidBrush(control.ForeColor)
                            gr.DrawString(value, textFont, sb, New Drawing.PointF(x, y))
                        End Using
                    End Using
                End If
            End If
        End Sub

#End Region

#Region " ENABLED "

        ''' <summary> Sets the <see cref="Control">control</see> enabled value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeEnabledSetter(ByVal control As Control, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, Boolean)(AddressOf Methods.SafeEnabledSetter), New Object() {control, value})
                Else
                    control.Enabled = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " VISIBLE "

        ''' <summary> Sets the <see cref="Control">control</see> enabled value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeVisibleSetter(ByVal control As Control, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, Boolean)(AddressOf Methods.SafeVisibleSetter), New Object() {control, value})
                Else
                    control.Visible = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " BACK COLOR "

        ''' <summary> Safe back color setter. This setter is thread safe. </summary>
        ''' <param name="control"> The target progress bar to add text into. </param>
        ''' <param name="value">   Specifies a value to set. </param>
        <Extension()>
        Public Sub SafeBackColorSetter(ByVal control As Control, ByVal value As Drawing.Color)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, Drawing.Color)(AddressOf Methods.SafeBackColorSetter), New Object() {control, value})
                Else
                    control.BackColor = value
                End If
            End If
        End Sub

#End Region

#Region " TEXT "

        ''' <summary>
        ''' Sets the <see cref="Control">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control">       The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <param name="displayFormat"> The display format. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal value As DateTime?, ByVal displayFormat As String) As String
            Dim displayValue As String = String.Empty
            If value.HasValue AndAlso value.Value > DateTime.MinValue Then
                displayValue = String.Format(displayFormat, value.Value)
            End If
            If control IsNot Nothing AndAlso Not String.Equals(control.Text, displayValue) Then
                control.SafeTextSetter(displayValue)
            End If
            Return displayValue
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control">       The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <param name="displayFormat"> The display format. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal value As DateTimeOffset?, ByVal displayFormat As String) As String
            Dim displayValue As String = String.Empty
            If value.HasValue AndAlso value.Value > DateTime.MinValue Then
                displayValue = String.Format(displayFormat, value.Value)
            End If
            If control IsNot Nothing AndAlso Not String.Equals(control.Text, displayValue) Then
                control.SafeTextSetter(displayValue)
            End If
            Return displayValue
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal value As Integer?) As String
            Dim displayValue As String = String.Empty
            If value.HasValue Then
                displayValue = value.Value.ToString
            End If
            If control IsNot Nothing AndAlso Not String.Equals(control.Text, displayValue) Then
                control.SafeTextSetter(displayValue)
            End If
            Return displayValue
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">         The value. </param>
        ''' <param name="displayFormat"> The display format. </param>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal value As Double?, ByVal displayFormat As String) As String
            Dim displayValue As String = String.Empty
            If value.HasValue Then
                displayValue = String.Format(displayFormat, value.Value)
            End If
            If control IsNot Nothing AndAlso Not String.Equals(control.Text, displayValue) Then
                control.SafeTextSetter(displayValue)
            End If
            Return displayValue
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">         The value. </param>
        ''' <param name="provider">      The provider. </param>
        ''' <param name="displayFormat"> The display format. </param>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal value As Double?, ByVal provider As System.IFormatProvider, ByVal displayFormat As String) As String
            Dim displayValue As String = String.Empty
            If value.HasValue Then
                displayValue = String.Format(provider, displayFormat, value.Value)
            End If
            If control IsNot Nothing AndAlso Not String.Equals(control.Text, displayValue) Then
                control.SafeTextSetter(displayValue)
            End If
            Return displayValue
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal value As String) As String
            If control IsNot Nothing AndAlso Not String.Equals(control.Text, value) Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                End If
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, String)(AddressOf Methods.SafeTextSetter), New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the. </summary>
        ''' <param name="control">    The control. </param>
        ''' <param name="state">      true to state. </param>
        ''' <param name="trueValue">  The true value. </param>
        ''' <param name="falseValue"> The false value. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal state As Boolean, ByVal trueValue As String, ByVal falseValue As String) As String
            If control Is Nothing Then
                Return Nothing
            Else
                Return Methods.SafeTextSetter(control, If(state, trueValue, falseValue))
            End If
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the. </summary>
        ''' <param name="control">    The control. </param>
        ''' <param name="state">      true to state. </param>
        ''' <param name="format">     The text format. </param>
        ''' <param name="trueValue">  The true value. </param>
        ''' <param name="falseValue"> The false value. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal state As Boolean,
                                       ByVal format As String, ByVal trueValue As String, ByVal falseValue As String) As String
            If control Is Nothing Then
                Return Nothing
            Else
                Return Methods.SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                        format, IIf(state, trueValue, falseValue)))
            End If
        End Function

        ''' <summary> Sets the <see cref="TextBox">control</see> text to formated text.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

        ''' <summary> Sets the <see cref="TextBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The control is disabled when set so that the handling of the
        ''' changed event can be skipped. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentTextSetter(ByVal control As Control, ByVal value As String) As String
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, String)(AddressOf Methods.SafeSilentTextSetter), New Object() {control, value})
                Else
                    SilentTextSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="TextBox">control</see> text to the formatted text.
        ''' This setter is thread safe. The control is disabled when set so that the handling of the
        ''' changed event can be skipped. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SafeSilentTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

#End Region

#Region " TOOL TIP "

        ''' <summary> Sets the <see cref="Control">control</see> tool tip text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="toolTip"> The tool tip control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeToolTipSetter(ByVal control As Control, ByVal toolTip As ToolTip, ByVal value As String) As String
            If control IsNot Nothing AndAlso toolTip IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                End If
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, ToolTip, String)(AddressOf Methods.SafeToolTipSetter), New Object() {control, toolTip, value})
                Else
                    toolTip.SetToolTip(control, value)
                End If
            End If
            Return value
        End Function

#End Region

    End Module
End Namespace

