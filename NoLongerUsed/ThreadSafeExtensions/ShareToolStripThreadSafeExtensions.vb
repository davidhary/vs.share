﻿Imports System.Runtime.CompilerServices
Namespace ToolStripExtensions
    ''' <summary> Includes extensions for <see cref="ToolStrip">Tool Strip</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " TOOL STRIP BUTTON "

        ''' <summary> Sets the <see cref="Windows.Forms.ToolStripButton">Tool Strip Button</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The setter disables the control before altering the checked state
        ''' allowing the control code to use the enabled state for preventing the execution of the
        ''' control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentCheckedSetter(ByVal control As Windows.Forms.ToolStripButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of Windows.Forms.ToolStripButton, Boolean)(AddressOf ToolStripExtensions.SafeSilentCheckedSetter), (New Object() {control, value}))
                Else
                    Dim wasEnabled As Boolean = control.Enabled
                    control.Enabled = False
                    control.Checked = value
                    control.Enabled = wasEnabled
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Windows.Forms.ToolStripButton">Tool Strip Button</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCheckedSetter(ByVal control As Windows.Forms.ToolStripButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of Windows.Forms.ToolStripButton, Boolean)(AddressOf ToolStripExtensions.SafeCheckedSetter), (New Object() {control, value}))
                Else
                    control.Checked = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Windows.Forms.ToolStripButton">Tool Strip Button</see> Enabled value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeEnabledSetter(ByVal control As Windows.Forms.ToolStripButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of Windows.Forms.ToolStripButton, Boolean)(AddressOf ToolStripExtensions.SafeEnabledSetter), (New Object() {control, value}))
                Else
                    control.Enabled = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " TOOL STRIP ITEM "

        ''' <summary> Sets the <see cref="Windows.Forms.ToolStripMenuItem">control</see> Checked to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeCheckedSetter(ByVal control As Windows.Forms.ToolStripMenuItem, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of Windows.Forms.ToolStripMenuItem, Boolean)(AddressOf ToolStripExtensions.SafeCheckedSetter), (New Object() {control, value}))
                Else
                    control.Checked = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Windows.Forms.ToolStripMenuItem">control</see> Checked to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeSilentCheckedSetter(ByVal control As Windows.Forms.ToolStripMenuItem, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of Windows.Forms.ToolStripMenuItem, Boolean)(AddressOf ToolStripExtensions.SafeCheckedSetter), (New Object() {control, value}))
                Else
                    Dim wasEnabled As Boolean = control.Enabled
                    control.Enabled = False
                    control.Checked = value
                    control.Enabled = wasEnabled
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> check state value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The setter disables the control before altering the checked state
        ''' allowing the control code to use the enabled state for preventing the execution of the
        ''' control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentCheckStateSetter(ByVal control As Windows.Forms.ToolStripMenuItem, ByVal value As CheckState) As CheckState
            If control IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of Windows.Forms.ToolStripMenuItem, CheckState)(AddressOf Methods.SafeSilentCheckStateSetter), (New Object() {control, value}))
                Else
                    Methods.SilentCheckStateSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolStripItem">control</see> Enabled to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip item control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeEnabledSetter(ByVal control As ToolStripItem, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of ToolStripItem, Boolean)(AddressOf ToolStripExtensions.SafeEnabledSetter), New Object() {control, value})
                Else
                    control.Enabled = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolStripItem">control</see> Text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip item control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As ToolStripItem, ByVal value As String) As String
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of ToolStripItem, String)(AddressOf ToolStripExtensions.SafeTextSetter), New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolStripItem">control</see> Text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip item control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <param name="format">  Describes the format to use. </param>
        ''' <returns> The formatted value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As ToolStripItem, ByVal value As Integer?, ByVal format As String) As String
            Dim v As String = String.Empty
            If value.HasValue Then
                If String.IsNullOrWhiteSpace(format) Then
                    v = CStr(value.Value)
                Else
                    v = String.Format(Globalization.CultureInfo.CurrentCulture, format, CByte(value.Value And &HFF))
                End If
            End If
            Return ToolStripExtensions.SafeTextSetter(control, v)
        End Function

        ''' <summary> Sets the <see cref="ToolStripItem">control</see> ToolTipText to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip item control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeToolTipTextSetter(ByVal control As ToolStripItem, ByVal value As String) As String
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of ToolStripItem, String)(AddressOf ToolStripExtensions.SafeToolTipTextSetter), New Object() {control, value})
                Else
                    control.ToolTipText = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolStripItem">control</see> visible to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip item control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeVisibleSetter(ByVal control As ToolStripItem, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing AndAlso control.Visible <> value Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of ToolStripItem, Boolean)(AddressOf ToolStripExtensions.SafeVisibleSetter), New Object() {control, value})
                Else
                    control.Visible = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " TOOL STRIP PROGRESS BAR "

        ''' <summary> Sets the <see cref="ToolStripProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function SafeValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of ToolStripProgressBar, Integer)(AddressOf ToolStripExtensions.SafeValueSetter), New Object() {control, value})
                Else
                    value = control.ValueSetter(value)
                End If
            End If
            Return value
        End Function

        <Extension()>
        Public Function SafeValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Double) As Integer
            Return SafeValueSetter(control, CInt(value))
        End Function


        ''' <summary> Updates the <see cref="ToolStripProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function SafeValueUpdater(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.Value <> value Then
                    Return SafeValueSetter(control, value)
                End If
            End If
            Return value
        End Function

#End Region

#Region " STATUS STRIP CUSTOM PROGRESS BAR "

        ''' <summary> Sets the <see cref="StatusStripCustomProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function SafeValueSetter(ByVal control As StatusStripCustomProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of StatusStripCustomProgressBar, Integer)(AddressOf ToolStripExtensions.SafeValueSetter), New Object() {control, value})
                Else
                    value = control.ValueSetter(value)
                End If
            End If
            Return value
        End Function

        <Extension()>
        Public Function SafeValueSetter(ByVal control As StatusStripCustomProgressBar, ByVal value As Double) As Integer
            Return SafeValueSetter(control, CInt(value))
        End Function

        ''' <summary> Updates the <see cref="StatusStripCustomProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function SafeValueUpdater(ByVal control As StatusStripCustomProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.Value <> value Then
                    Return SafeValueSetter(control, value)
                End If
            End If
            Return value
        End Function

#End Region

#Region " IMAGE "

        ''' <summary> Sets the <see cref="Control">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeImageSetter(ByVal control As ToolStripItem, ByVal value As Image) As Image
            If control Is Nothing Then
                Return Nothing
            Else
                If control IsNot Nothing AndAlso Not Image.Equals(control.Image, value) Then
                    If control.Owner.InvokeRequired Then
                        control.Owner.Invoke(New Action(Of ToolStripItem, Image)(AddressOf ToolStripExtensions.SafeImageSetter), New Object() {control, value})
                    Else
                        control.Image = value
                    End If
                End If
                Return value
            End If
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the. </summary>
        ''' <param name="control">    The control. </param>
        ''' <param name="state">      true to state. </param>
        ''' <param name="trueImage">  The true image. </param>
        ''' <param name="falseImage"> The false image. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeImageSetter(ByVal control As ToolStripItem, ByVal state As Boolean, ByVal trueImage As Image, ByVal falseImage As Image) As Image
            If control Is Nothing Then
                Return Nothing
            Else
                Return ToolStripExtensions.SafeImageSetter(control, If(state, trueImage, falseImage))
            End If
        End Function

#End Region

    End Module
End Namespace
