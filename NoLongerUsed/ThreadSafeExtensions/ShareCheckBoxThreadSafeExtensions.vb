﻿Imports System.Runtime.CompilerServices
Namespace CheckBoxExtensions
    ''' <summary> Includes extensions for <see cref="CheckBox">check box</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentCheckStateSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As CheckState) As CheckState
            If control IsNot Nothing Then
                If control.ThreeState Then
                    Dim wasEnabled As Boolean = control.Enabled
                    control.Enabled = False
                    control.CheckState = value
                    control.Enabled = wasEnabled
                Else
                    Throw New InvalidOperationException("Attempted to set a two-state control with a three-state value.")
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> check state value to the
        ''' <paramref name="value">value</paramref>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentCheckedSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.Checked = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> check state value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCheckStateSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As CheckState) As CheckState
            If control IsNot Nothing Then
                If Not control.ThreeState Then
                    Throw New InvalidOperationException("Attempted to set a two-state control with a three-state value.")
                End If
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of CheckBox, CheckState)(AddressOf Methods.SafeCheckStateSetter), New Object() {control, value})
                Else
                    control.CheckState = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCheckedSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As Boolean?) As Boolean?
            Return Methods.FromCheckState(Methods.SafeCheckStateSetter(control, Methods.ToCheckState(value)))
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCheckedSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of CheckBox, Boolean)(AddressOf Methods.SafeCheckedSetter), New Object() {control, value})
                Else
                    control.Checked = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="CheckBox">Check Box Control</see> Read-Only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check Box Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeReadOnlySetter(ByVal control As Core.Controls.CheckBox, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of CheckBox, Boolean)(AddressOf Methods.SafeReadOnlySetter), New Object() {control, value})
                Else
                    control.ReadOnly = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The setter disables the control before altering the checked state
        ''' allowing the control code to use the enabled state for preventing the execution of the
        ''' control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentCheckedSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of CheckBox, Boolean)(AddressOf Methods.SafeSilentCheckedSetter), New Object() {control, value})
                Else
                    Methods.SilentCheckedSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> check state value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The setter disables the control before altering the checked state
        ''' allowing the control code to use the enabled state for preventing the execution of the
        ''' control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentCheckStateSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As CheckState) As CheckState
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of CheckBox, CheckState)(AddressOf Methods.SafeSilentCheckStateSetter), New Object() {control, value})
                Else
                    Methods.SilentCheckStateSetter(control, value)
                End If
            End If
            Return value
        End Function

    End Module
End Namespace
