﻿Imports System.Runtime.CompilerServices
Namespace LabelExtensions
    ''' <summary> Includes extensions for <see cref="Label"/>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Sets the <see cref="Label">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The Label control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Label, ByVal value As String) As String
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Label, String)(AddressOf LabelExtensions.SafeTextSetter), New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Label">control</see> text to the
        ''' formated value. This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="Label">label </see> control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As Label, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

    End Module
End Namespace
