﻿Imports System.Runtime.CompilerServices
Namespace ButtonExtensions
    ''' <summary> Includes extensions for <see cref="ButtonBase">button base</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " CAUSES VALIDATION "

        ''' <summary> Sets the <see cref="Control">button base</see> read-only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCausesValidationSetter(ByVal control As ButtonBase, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ButtonBase, Boolean)(AddressOf ButtonExtensions.SafeCausesValidationSetter), New Object() {control, value})
                Else
                    control.CausesValidation = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " IMAGE "

        ''' <summary> Sets the <see cref="Control">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeImageSetter(ByVal control As ButtonBase, ByVal value As Image) As Image
            If control Is Nothing Then
                Return Nothing
            Else
                If control IsNot Nothing AndAlso Not Image.Equals(control.Image, value) Then
                    If control.InvokeRequired Then
                        control.Invoke(New Action(Of ButtonBase, Image)(AddressOf ButtonExtensions.SafeImageSetter), New Object() {control, value})
                    Else
                        control.Image = value
                    End If
                End If
                Return value
            End If
        End Function

        ''' <summary> Sets the <see cref="Control">control</see> text to the. </summary>
        ''' <param name="control">    The control. </param>
        ''' <param name="state">      true to state. </param>
        ''' <param name="trueImage">  The true image. </param>
        ''' <param name="falseImage"> The false image. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeImageSetter(ByVal control As ButtonBase, ByVal state As Boolean, ByVal trueImage As Image, ByVal falseImage As Image) As Image
            If control Is Nothing Then
                Return Nothing
            Else
                Return ButtonExtensions.SafeImageSetter(control, If(state, trueImage, falseImage))
            End If
        End Function

#End Region

    End Module

End Namespace
