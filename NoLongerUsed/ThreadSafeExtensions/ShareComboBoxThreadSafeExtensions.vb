﻿Imports System.Runtime.CompilerServices
Namespace ComboBoxExtensions
    ''' <summary> Includes extensions for <see cref="ComboBox">Combo Box</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " READ ONLY SETTER "

        ''' <summary> Sets the <see cref="ComboBox">Combo Box Control</see> Read-Only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo Box Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeReadOnlySetter(ByVal control As Core.Controls.ComboBox, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Boolean)(AddressOf ComboBoxExtensions.SafeReadOnlySetter), New Object() {control, value})
                Else
                    control.ReadOnly = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " TEXT SETTER "

        ''' <summary> Sets the <see cref="ComboBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The combo box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As System.Windows.Forms.ComboBox, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                End If
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeTextSetter), New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ComboBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The control is disabled when set so that the handling of the
        ''' changed event can be skipped. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The combo box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentTextSetter(ByVal control As System.Windows.Forms.ComboBox, ByVal value As String) As String
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeSilentTextSetter), New Object() {control, value})
                Else
                    SilentTextSetter(control, value)
                End If
            End If
            Return value
        End Function

#End Region

#Region " SELECT ITEM "

        ''' <summary> Selects an item in a thread safe way. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="displayValue"> The display value. </param>
        <Extension()>
        Public Sub SafeSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal displayValue As String)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeSelectItem), New Object() {control, displayValue})
                Else
                    control.SelectItem(displayValue)
                End If
            End If
        End Sub

        ''' <summary> Selects an item in a 'Silent' (control is disabled) and thread safe way. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="displayValue"> The display value. </param>
        <Extension()>
        Public Sub SafeSilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal displayValue As String)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeSelectItem), New Object() {control, displayValue})
                Else
                    control.SilentSelectItem(displayValue)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Selects the <see cref="Control">combo box</see> item by setting the selected item to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ComboBoxExtensions.SafeSilentSelectItem), New Object() {control, value})
                Else
                    ComboBoxExtensions.SilentSelectItem(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> item by setting the selected item to
        ''' the <see cref="System.Collections.Generic.KeyValuePair">key value pair</see>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="key">     The selected item key. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
            Return ComboBoxExtensions.SafeSilentSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
        End Function

#End Region

#Region " SELECT Value "

        ''' <summary> Selects the <see cref="Control">combo box</see> Value by setting the selected Value to
        ''' the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected Value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSelectValue(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ComboBoxExtensions.SafeSelectValue), New Object() {control, value})
                Else
                    control.SelectedValue = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> Value by setting the selected Value to
        ''' the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected Value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentSelectValue(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ComboBoxExtensions.SafeSilentSelectValue), New Object() {control, value})
                Else
                    ComboBoxExtensions.SilentSelectValue(control, value)
                End If
            End If
            Return value
        End Function

#End Region

    End Module
End Namespace
