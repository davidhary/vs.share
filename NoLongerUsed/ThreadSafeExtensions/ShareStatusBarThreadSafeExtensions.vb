﻿Imports System.Runtime.CompilerServices
Namespace StatusBarExtensions
    ''' <summary> Includes extensions for <see cref="StatusBar">Status Bar</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Sets the <see cref="TextBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="StatusBarPanel"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As StatusBarPanel, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                End If
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of StatusBarPanel, String)(AddressOf StatusBarExtensions.SafeTextSetter), New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="TextBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="StatusBarPanel"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeToolTipTextSetter(ByVal control As StatusBarPanel, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                End If
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of StatusBarPanel, String)(AddressOf StatusBarExtensions.SafeToolTipTextSetter), New Object() {control, value})
                Else
                    control.ToolTipText = value
                End If
            End If
            Return value
        End Function

    End Module
End Namespace
