﻿Imports System.Runtime.CompilerServices
Namespace RadioButtonExtensions
    ''' <summary> Includes extensions for <see cref="RadioButton">Radio Button</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The setter disables the control before altering the checked state
        ''' allowing the control code to use the enabled state for preventing the execution of the
        ''' control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentCheckedSetter(ByVal control As System.Windows.Forms.RadioButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of RadioButton, Boolean)(AddressOf RadioButtonExtensions.SafeSilentCheckedSetter), New Object() {control, value})
                Else
                    SilentCheckedSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCheckedSetter(ByVal control As System.Windows.Forms.RadioButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of RadioButton, Boolean)(AddressOf RadioButtonExtensions.SafeCheckedSetter), New Object() {control, value})
                Else
                    control.Checked = value
                End If
            End If
            Return value
        End Function

    End Module
End Namespace
