﻿Imports System.Threading

Partial Public Class EventHandlerContext(Of TEventArgs As EventArgs)

#Region " POST DYNAMIC INVOKE "

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub PostDynamicInvoke(ByVal sender As Object, ByVal e As TEventArgs)
        Dim context As SynchronizationContext = If(Me.Context, EventHandlerContext(Of TEventArgs).CurrentSyncContext)
        Me.PostDynamicInvoke(context, sender, e)
    End Sub

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <param name="context"> The context. </param>
    ''' <param name="sender">  Source of the event. </param>
    ''' <param name="e">       Property changed event information. </param>
    Private Sub PostDynamicInvoke(ByVal context As SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
        Dim evt As EventHandler(Of TEventArgs) = Me.Handler
        If evt IsNot Nothing Then
            For Each d As [Delegate] In evt.GetInvocationList
                ' the event handler context is expected to have only a single target delegate.
                context.Post(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e)
            Next
        End If
    End Sub

#End Region

#Region " SEND/POST OR TARGET INVOKE "

    ''' <summary> Invokes the event on a delegate target. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      T event information. </param>
    Public Sub PostTargetInvoke(ByVal sender As Object, ByVal e As TEventArgs)
        Me.PostTargetInvoke(Me.Context, sender, e)
    End Sub

    ''' <summary> Invokes the event on a delegate target. </summary>
    ''' <param name="context"> The context. </param>
    ''' <param name="sender">  Source of the event. </param>
    ''' <param name="e">       The event information. </param>
    Public Sub PostTargetInvoke(ByVal context As SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
        Dim evt As EventHandler(Of TEventArgs) = Me.Handler
        If evt IsNot Nothing Then EventHandlerContext(Of TEventArgs)._PostTargetInvoke(evt, context, sender, e)
    End Sub

    ''' <summary> Invokes the event on a delegate target. </summary>
    ''' <param name="evt">     The event handler. </param>
    ''' <param name="context"> The context. </param>
    ''' <param name="sender">  Source of the event. </param>
    ''' <param name="e">       The event information. </param>
    Private Shared Sub PostTargetInvokeThis(evt As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
        If context Is Nothing Then
            EventHandlerExtensions.TargetInvoke(evt, sender, e)
        Else
            context.Send(Sub() evt(sender, e), Nothing)
        End If
    End Sub

    ''' <summary> Invokes the event on a delegate target. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      T event information. </param>

    Public Sub SendTargetInvoke(ByVal sender As Object, ByVal e As TEventArgs)
        Me.SendTargetInvoke(Me.Context, sender, e)
    End Sub

    ''' <summary> Invokes the event on a delegate target. </summary>
    ''' <param name="context"> The context. </param>
    ''' <param name="sender">  Source of the event. </param>
    ''' <param name="e">       The event information. </param>
    Public Sub SendTargetInvoke(ByVal context As SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
        Dim evt As EventHandler(Of TEventArgs) = Me.Handler
        If evt IsNot Nothing Then EventHandlerContext(Of TEventArgs)._SendTargetInvoke(evt, context, sender, e)
    End Sub

    ''' <summary> Invokes the event on a delegate target. </summary>
    ''' <param name="evt">     The event handler. </param>
    ''' <param name="context"> The context. </param>
    ''' <param name="sender">  Source of the event. </param>
    ''' <param name="e">       The event information. </param>
    Private Shared Sub SendTargetInvokeThis(evt As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
        If context Is Nothing Then
            EventHandlerExtensions.TargetInvoke(evt, sender, e)
        Else
            context.Send(Sub() evt(sender, e), Nothing)
        End If
    End Sub

#End Region

End Class



Partial Public Class EventHandlerContextCollection(Of TEventArgs As EventArgs)

    ''' <summary> Send this message. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    Public Sub PostDynamicInvoke(sender As Object, ByVal e As TEventArgs)
        For Each evt As EventHandlerContext(Of TEventArgs) In Me : evt?.PostDynamicInvoke(sender, e) : Next
    End Sub

    ''' <summary> Posts or target invokes. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      T event information. </param>
    Public Sub PostTargetInvoke(sender As Object, ByVal e As TEventArgs)
        For Each evt As EventHandlerContext(Of TEventArgs) In Me : evt?.PostTargetInvoke(sender, e) : Next
    End Sub

    ''' <summary> Sends or target invokes. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      T event information. </param>
    Public Sub SendTargetInvoke(sender As Object, ByVal e As TEventArgs)
        For Each evt As EventHandlerContext(Of TEventArgs) In Me : evt?.SendTargetInvoke(sender, e) : Next
    End Sub

End Class