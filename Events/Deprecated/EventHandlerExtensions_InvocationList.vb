﻿Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Collections.Specialized

Namespace EventHandlerExtensions
    Public Module Methods

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <remarks>
        ''' David, 1/1/2016. This workaround is required because the null conditional does not seem to
        ''' work correctly causing an null reference exception.
        ''' </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(Of TEventArgs)(ByVal handler As EventHandler(Of TEventArgs)) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As EventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As AddingNewEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As AssemblyLoadEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As AsyncCompletedEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As CancelEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As CollectionChangeEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As ConsoleCancelEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As CurrentChangingEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As DataReceivedEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As ProgressChangedEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As PropertyChangedEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As PropertyChangingEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function InvocationList(ByVal handler As NotifyCollectionChangedEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

    End Module
End Namespace
