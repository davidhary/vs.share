Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Namespace EventHandlerExtensions

    Partial Public Module Methods

#Region " UNSAFE INVOKES "

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            evt?.Invoke(sender, e)
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(ByVal handler As EventHandler(Of EventArgs), ByVal sender As Object, ByVal e As EventArgs)
            Dim evt As EventHandler(Of EventArgs) = handler
            evt?.Invoke(sender, e)
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            evt?.Invoke(sender, e)
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            evt?.Invoke(sender, e)
        End Sub

#End Region

#Region " TARGET INVOKE "

        ''' <summary> Invokes the event on a delegate target. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  Source of the event. </param>
        <Extension>
        Public Sub TargetInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
            Methods.TargetInvoke(handler, sender, System.EventArgs.Empty)
        End Sub

        ''' <summary> Invokes the event on a delegate target. </summary>
        ''' <param name="Handler"> The event handler. </param>
        ''' <param name="sender">  Source of the event. </param>
        ''' <param name="e">       The event information. </param>
        <Extension>
        Public Sub TargetInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then Methods._TargetInvoke(evt, sender, e)
        End Sub

        ''' <summary> Invokes the event on a delegate target. </summary>
        ''' <param name="evt">    The event handler. </param>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      The event information. </param>
        Private Sub TargetInvokeThis(Of TEventArgs As EventArgs)(evt As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            For Each d As [Delegate] In evt.GetInvocationList
                If d.Target IsNot Nothing Then
                    ' asynchronously executes the delegate on the target thread.
                    ' https://blogs.msdn.microsoft.com/jaredpar/2008/01/07/isynchronizeinvoke-now/
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    Methods.TargetInvoke(syncInvokeTarget, d, sender, e)
                End If
            Next
        End Sub

        ''' <summary> Invokes the event on a delegate target. </summary>
        ''' <param name="synchronizeTarget"> The synchronization invoke target. </param>
        ''' <param name="delegate">          A [Delegate] to process. </param>
        ''' <param name="sender">            Source of the event. </param>
        ''' <param name="e">                 The event information. </param>
        Private Sub TargetInvoke(Of TEventArgs As EventArgs)(ByVal synchronizeTarget As ISynchronizeInvoke, [delegate] As [Delegate], ByVal sender As Object, ByVal e As TEventArgs)
            If synchronizeTarget Is Nothing OrElse Not synchronizeTarget.InvokeRequired Then
                ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                [delegate].DynamicInvoke(New Object() {sender, e})
            Else
                ' execute the delegate on the target thread.
                synchronizeTarget.Invoke([delegate], New Object() {sender, e})
            End If
        End Sub

        ''' <summary> Invokes the event on a delegate target. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub TargetInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods._TargetInvoke(evt, sender, e)
        End Sub

        ''' <summary> Invokes the event on that delegate target. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        Private Sub TargetInvokeThis(evt As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            For Each d As [Delegate] In evt.GetInvocationList
                If d.Target IsNot Nothing Then
                    ' asynchronously executes the delegate on the target thread.
                    ' https://blogs.msdn.microsoft.com/jaredpar/2008/01/07/isynchronizeinvoke-now/
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    Methods.TargetInvoke(syncInvokeTarget, d, sender, e)
                End If
            Next
        End Sub

        ''' <summary> Invokes the event on a delegate target. </summary>
        ''' <param name="synchronizeTarget"> The synchronization invoke target. </param>
        ''' <param name="delegate">          A [Delegate] to process. </param>
        ''' <param name="sender">            Source of the event. </param>
        ''' <param name="e">                 Property Changed event information. </param>
        Private Sub TargetInvoke(ByVal synchronizeTarget As ISynchronizeInvoke, [delegate] As [Delegate], ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            If synchronizeTarget Is Nothing OrElse Not synchronizeTarget.InvokeRequired Then
                ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                [delegate].DynamicInvoke(New Object() {sender, e})
            Else
                ' execute the delegate on the target thread.
                synchronizeTarget.Invoke([delegate], New Object() {sender, e})
            End If
        End Sub

#End Region

    End Module

End Namespace

