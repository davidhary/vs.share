﻿Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Collections.Specialized

Namespace EventHandlerExtensions

    Partial Public Module Methods


        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an
        ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        Private Sub AsyncDynamicInvokeThis(Of TEventArgs As EventArgs)(ByVal evt As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
            For Each d As [Delegate] In evt.GetInvocationList
                context.Post(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e)
            Next
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>

        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property changed event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(ByVal handler As PropertyChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property changed event information. </param>
        Private Sub AsyncDynamicInvokeThis(ByVal evt As PropertyChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            For Each d As [Delegate] In evt.GetInvocationList
                context.Post(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e)
            Next
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>

        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(ByVal handler As PropertyChangingEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an
        ''' <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        Private Sub AsyncDynamicInvokeThis(ByVal evt As PropertyChangingEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            For Each d As [Delegate] In evt.GetInvocationList
                context.Post(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e)
            Next
            ' much slower than the loop through the invocation list.
            ' If e v t IsNot Nothing Then Methods.CurrentSyncContext.Post(Sub() e v t(sender, e), Nothing)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an
        ''' <see cref="NotifyCollectionChangedEventHandler">Event</see>.

        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(ByVal handler As NotifyCollectionChangedEventHandler, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub AsyncDynamicInvoke(ByVal handler As NotifyCollectionChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then Methods._AsyncDynamicInvoke(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously and dynamically invokes an
        ''' <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        Private Sub AsyncDynamicInvokeThis(ByVal evt As NotifyCollectionChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            For Each d As [Delegate] In evt.GetInvocationList
                context.Post(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e)
            Next
            ' much slower than the loop throw the invocation list.
            ' If e v t IsNot Nothing Then Methods.CurrentSyncContext.Post(Sub() e v t(sender, e), Nothing)
        End Sub


    End Module


End Namespace

