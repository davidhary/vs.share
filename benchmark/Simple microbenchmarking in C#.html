<?xml version="1.0" encoding="ISO-8859-1"?>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Simple microbenchmarking in C#</title>
    <link rel="stylesheet" href="styles.css"/>
  </head>
  <body>
    <h1>Simple microbenchmarking in C#</h1>
    <h2>Introduction and scope</h2>
    <p>
    	Often in the newsgroups, people ask the fastest way of doing
    	something. The first thing I believe it's important to realise
    	is that unless you know that that task is causing a bottleneck
    	in your real application, it's probably not worth working out
    	what the truly fastest way of doing it is. Instead, write the
    	clearest code which does the job - it's much better to take a
    	while to get the right answer than to get the wrong answer
    	very quickly.
    </p>
    <p>
    	Having said that, I've always enjoyed a bit of microbenchmarking.
    	For proper application-wide benchmarking, you should be using a 
    	decent profiler etc, worrying about all kinds of different aspects
    	of performance. A server-side app, for instance, should worry about
    	latency as well as total throughput, and of course scalability and
    	reliability. This page doesn't attempt to cover any of that.
    </p>
    <p>
    	This page provides a very simple microbenchmarking framework which
    	I now use when testing different ways of attacking a problem. It's
    	rough and ready, but it does well enough for newsgroup posts and
    	testing for fun.    	
    </p>
    <h2>Obtaining the code and compiling benchmarks</h2>
    <p>
    	The code has grown slightly since its first incarnation, and it
    	no longer makes sense to have it inline in this page. However,
    	it's still just a single file to download: 
    	<a href="Benchmark.cs">Benchmark.cs</a>. Once you've downloaded it, 
    	using it is simple: just compile it into the same assembly as the
    	code you wish to benchmark, and run it. It will find all the types
    	with benchmarking code in, and run that code, reporting the timings.
    	It's probably best to demonstrate this with a small sample benchmark:
    </p>
<table class="code"><tr><td><pre>
<span class="Namespace">using</span> System;
<span class="Namespace">using</span> System.Collections;

<span class="Modifier">public</span> <span class="ReferenceType">class</span> FillingArrayList
{
    [Benchmark]
    <span class="Modifier">public</span> <span class="Modifier">static</span> <span class="ValueType">void</span> Test()
    {
        ArrayList al = <span class="Keyword">new</span> ArrayList();
        <span class="Statement">for</span> (<span class="ValueType">int</span> i=0; i &lt; 10000000; i++)
            al.Add (<span class="String">"hello"</span>);
    }
}
</pre></td></tr></table>
    <p>
    	The only "special" thing about this class is that the <code>Test</code>
    	method is marked with the <code>Benchmark</code> attribute (which is
    	a simple attribute which can only be applied to methods - it's declared
    	in <code>Benchmark.cs</code>). The method <i>must</i> be public, static,
    	and not take any parameters, otherwise the framework ignores it. All
    	the methods which obey these rules are executed and timed (one at a time).
    </p>
    <p>
    	I almost always compile simple test programs from the command line -
    	if you wish to use Visual Studio, it's just a case of having 
    	<code>Benchmark.cs</code> in the project. If you have other classes
    	which have a <code>Main</code> method, set <code>Benchmark</code> as
    	the startup class. The project should be set to be a console application
    	project. When compiling from the command line I simply use
    	something like: <code>cs Benchmark.cs FillingArrayList.cs</code> (using
    	the above code as an example). This produces <code>Benchmark.exe</code>
    	which I can then run directly. 
    </p>
    <p>
    	Running the code produces results like this:
    </p>
<table class="results"><tr><td><pre>
Benchmarking type FillingArrayList
  Test                 00:00:02.1044640
</pre></td></tr></table>
		<p>
			Hopefully the meaning is self-explanatory!
		</p>
		<h2>
		  Going beyond the very basics
		</h2>
		<p>
			Some of the time, a test like the above is all that's needed. Often you'll
			want a bit more though. The framework allows you to use command-line parameters
			to control the tests, along with ways of resetting the class between tests
			and checking the results after each test. Here's an example showing all of these
			features, as well as demonstrating what happens when you have more than one
			benchmark method in a class. Note that you can use any of these features
			individually, or indeed none of them at all.
		</p>
<table class="code"><tr><td><pre>
<span class="InlineComment">// See http://www.pobox.com/~skeet/csharp/benchmark.html for how to run this code</span>
<span class="Namespace">using</span> System;
<span class="Namespace">using</span> System.Collections;

<span class="Modifier">public</span> <span class="ReferenceType">class</span> FillingArrayList
{
    <span class="Modifier">static</span> <span class="ValueType">int</span> iterations=10000000; <span class="InlineComment">// Give it a reasonable default</span>
    
    <span class="Modifier">static</span> ArrayList list; <span class="InlineComment">// This is what we'll check</span>
    
    <span class="Modifier">public</span> <span class="Modifier">static</span> <span class="ValueType">void</span> Init(<span class="ReferenceType">string</span>[] args)
    {
        <span class="Statement">if</span> (args.Length&gt;0)
            iterations = Int32.Parse(args[0]);
    }
    
    <span class="Modifier">public</span> <span class="Modifier">static</span> <span class="ValueType">void</span> Reset()
    {
        <span class="InlineComment">// Make sure that tests which don't produce</span>
        <span class="InlineComment">// anything don't end up using the previous result</span>
        list = <span class="Keyword">null</span>;
    }
   
    <span class="Modifier">public</span> <span class="Modifier">static</span> <span class="ValueType">void</span> Check()
    {
        <span class="Statement">if</span> (list.Count != iterations)
            <span class="Statement">throw</span> <span class="Keyword">new</span> Exception (<span class="String">"List doesn't have the right size."</span>);
    }
    
    [Benchmark]
    <span class="Modifier">public</span> <span class="Modifier">static</span> <span class="ValueType">void</span> Simple()
    {
        <span class="ValueType">int</span> count=iterations;
        
        ArrayList al = <span class="Keyword">new</span> ArrayList();
        <span class="Statement">for</span> (<span class="ValueType">int</span> i=0; i &lt; count; i++)
            al.Add (<span class="String">"hello"</span>);
        <span class="InlineComment">// Set the result</span>
        list=al;
    }

    [Benchmark]
    <span class="Modifier">public</span> <span class="Modifier">static</span> <span class="ValueType">void</span> RightSizing()
    {
        <span class="ValueType">int</span> count=iterations;
        
        ArrayList al = <span class="Keyword">new</span> ArrayList(count);
        <span class="Statement">for</span> (<span class="ValueType">int</span> i=0; i &lt; count; i++)
            al.Add (<span class="String">"hello"</span>);
        <span class="InlineComment">// Set the result</span>
        list=al;
    }
}
</pre></td></tr></table>    
		<p>
			The first thing to notice is that the number of iterations is
			no longer hardcoded. There's a static variable, <code>iterations</code>,
			which the tests use to determine how many iterations to run for. This
			has a default value (which is the same as the hardcoded value we used before),
			but it can also be set in the new <code>Init</code> method. This method
			must be public and static, and take an array of strings as its only parameter.
			The value passed in will never be null. 
		</p>
		<p>
			Before each test is run, the <code>Reset</code> method is called (if it is
			present). Like benchmark methods, it must be public, static and parameterless 
			to be noticed by the framework. This can be used to clear any information
			which might be left from the previous test, for instance. In this example,
			we make sure that the <code>list</code> variable is null before the start of
			the test.
		</p>
		<p>
			After each test is run, the <code>Check</code> method is called (if it 
			is present). Again, it must be public, static and parameterless. In practice,
			you can often get away with resetting state at the end of this method rather
			than having a separate <code>Reset</code> method, but you may prefer to use
			<code>Reset</code> for clarity. The aim of the <code>Check</code> method is
			to verify that the results of the test are correct, and throw an exception
			if they're not. If the check fails, the message of the exception is printed
			out instead of the time taken. In this example, we check that the size of
			the resultant list is correct. Note that this check is carried out <i>after</i>
			the time has been calculated, so you can afford to put time-consuming checks
			in here without affecting the results.
		</p>
		<p>
			Here are the results of two runs of the new class - once with a parameter
			on the command line, and once without.
		</p>
<table class="results"><tr><td><pre>
c:\test>benchmark 1000000
Benchmarking type FillingArrayList
  Simple               00:00:00.1001440
  RightSizing          00:00:00.0801152

c:\test>benchmark
Benchmarking type FillingArrayList
  Simple               00:00:02.1731248
  RightSizing          00:00:00.7610944
</pre></td></tr></table>
    <h2>
    	Command-line options
    </h2>
    <p>
    	There are now two command-line options available, which must come before any 
			parameters you wish to be passed to the benchmarked types themselves. The types
			will only receive the options from the point of the first option which isn't 
			understood, or after the pseudo-option <code>-endoptions</code>. (This last allows
			you to write tasks which use the same option names as the benchmark framework
			understands, if you really want to.) The options are:
			<dl>
	  		<dt><code>-version</code></dt>
	  		<dd>
					This just displays the operating system and CLR versions.
				</dd>
	  		<dt><code>-runtwice</code></dt>
	  		<dd>
					This changes the behaviour so that each method in each benchmarked type is run 
					twice. This allows you to see roughly how long was spent JITting the method being 
					tested, and to see the performance after JITting (ie the second result).
				</dd>
			</dl>
    </p>
	  <h2>Any other features?</h2>
	  <p>
	  	That's pretty much all there is to it. If you feel that there's a feature missing,
	  	please mail me at <a href="mailto:skeet@pobox.com">skeet@pobox.com</a> to let
	  	me know. I suppose the most obvious "feature" to add would be a GUI interface
	  	instead of the current command-line one, but that will have to come at a later
	  	date, if at all.
	  </p>
	  <h2>Microbenchmark best practices</h2>
	  <p>
	  	A few guidelines when it comes to writing benchmarks which will give some
	  	useful information:
	  	<dl>
	  		<dt><i>Run tests on an idle machine.</i></dt>
				<dd>
					The framework makes no attempt to only measure the time spent by
					the processor executing the benchmark code - it just compares the
					time at the start of the test with the time at the end of the test.
					If your machine is heavily loaded, that will affect things 
					significantly.
				</dd>
	  		<dt><i>Make your tests take a long time, where possible.</i></dt>
	  		<dd>
	  			Even when you try to make your machine as idle as possible,
	  			things can crop up to make benchmarks less dependable. Running
	  			benchmarks for a long time (minutes rather than seconds) can
	  			help to average these "blips" out, so you can get more repeatable
	  			results. It also makes the "one-time" effects of things like
	  			the inaccuracy of the system timer, and the time taken to call
	  			the benchmarking methods by reflection less significant as a 
	  			proportion of the final result. Sometime it's very difficult
	  			to get benchmarks to run for a long time without incurring
	  			other penalties such as garbage collection and swapping, unfortunately. 
	  			The	example code used here is a case in point - if you increase the
	  			size of the ArrayList too far, you'll end up eating up a lot
	  			of memory and the system will start swapping, which will dwarf
	  			any other considerations.
	  		</dd>
	  		<dt><i>Use local variables where possible.</i></dt>
	  		<dd>
	  			The CLR can do a more optimisations on code which doesn't (for
	  			the most part) "escape" from just local variables. For instance,
	  			it doesn't need to worry about other threads tampering with the
	  			variables. That's the reason the second example copies the number
	  			of iterations into a local variable before running the loop, and 
	  			copies the result out of a local variable into a class variable right
	  			at the very end. This may or may not make a significant difference
	  			to your test (on the current CLR), but I believe it's good practice
	  			anyway - although you need to bear this in mind when considering
	  			using the results in a real application!
	  		</dd>
	  		<dt><i>Avoid the compiler optimising your code away entirely.</i></dt>
	  		<dd>
	  			Unless you use the results of an operation which the JIT compiler 
	  			knows	can't affect anything else, it may well decide not to bother
	  			performing the operation at all. It's very important that you
	  			check the results in some form or other, making sure that the
	  			storage of results doesn't overwhelm the rest of the test, of course!
	  			For instance, I often keep a running total of results when testing
	  			something that produces a numeric result. I then test the total in
	  			the <code>Check</code> method. This will only work while adding
	  			to the total doesn't take long compared with the time taken to
	  			calculate what I need to add though.
	  		</dd>
	  		<dt><i>When posting code, say how to run it.</i></dt>
	  		<dd>
	  			At the time of writing, I'm the only one who's used this benchmarking
	  			framework. Of course, I hope it will be used more widely in the long
	  			run - but unless you know someone else already has the framework,
	  			they won't have a clue what to do with the benchmark code you give
	  			them unless you tell them how to run it. I suggest using a comment
	  			at the top of the source code, such as the one used in the second piece
	  			of sample code above.
	  		</dd>
	  		<dt><i>Avoid garbage collection if possible, unless that's the aim of the test.</i></dt>
	  		<dd>
	  			Garbage collection can slow things down in unpredictable ways. If you can
	  			avoid creating too much garbage (particularly long-lived garbage) in your tests,
	  			so much the better. The framework calls the garbage collector before
	  			each test is run in order to give it the best possible chance, but there's
	  			not a lot it can do to help if you eat up vast amounts of memory in your tests
	  			themselves.
	  		</dd>
	  	</dl>
	  </p>
	  <h2>The framework code</h2>
	  <p>
	  	Just in case you want to see the code but don't actually want to download it separately,
	  	here's the full code for the framework.
	  </p>
<table class="code"><tr><td><pre>
<span class="Namespace">using</span> System;
<span class="Namespace">using</span> System.Reflection;
<span class="Namespace">using</span> System.Collections;

<span class="XmlComment">/// &lt;summary&gt;</span>
<span class="XmlComment">/// The attribute to use to mark methods as being</span>
<span class="XmlComment">/// the targets of benchmarking.</span>
<span class="XmlComment">/// &lt;/summary&gt;</span>
[AttributeUsage(AttributeTargets.Method)]
<span class="Modifier">public</span> <span class="ReferenceType">class</span> BenchmarkAttribute : Attribute
{
}

<span class="XmlComment">/// &lt;summary&gt;</span>
<span class="XmlComment">/// Very simple benchmarking framework. Looks for all types</span>
<span class="XmlComment">/// in the current assembly which have static parameterless</span>
<span class="XmlComment">/// methods </span>
<span class="Modifier">public</span> <span class="ReferenceType">class</span> Benchmark
{
    <span class="Modifier">public</span> <span class="Modifier">static</span> <span class="ValueType">void</span> Main(<span class="ReferenceType">string</span>[] args)
    {
        <span class="InlineComment">// Save all the benchmark classes from doing a nullity test</span>
        <span class="Statement">if</span> (args==<span class="Keyword">null</span>)
            args = <span class="Keyword">new</span> <span class="ReferenceType">string</span>[0];
        
        <span class="InlineComment">// We're only ever interested in public static methods. This variable</span>
        <span class="InlineComment">// just makes it easier to read the code...</span>
        BindingFlags publicStatic = BindingFlags.Public | BindingFlags.Static;

        <span class="Statement">foreach</span> (Type type <span class="Statement">in</span> Assembly.GetCallingAssembly().GetTypes())
        {
            <span class="InlineComment">// Find an Init method taking string[], if any</span>
            MethodInfo initMethod=type.GetMethod (<span class="String">"Init"</span>, publicStatic, <span class="Keyword">null</span>, 
                                                  <span class="Keyword">new</span> Type[]{<span class="Keyword">typeof</span>(<span class="ReferenceType">string</span>[])},
                                                  <span class="Keyword">null</span>);
            
            <span class="InlineComment">// Find a parameterless Reset method, if any</span>
            MethodInfo resetMethod=type.GetMethod (<span class="String">"Reset"</span>, publicStatic, 
                                                   <span class="Keyword">null</span>, <span class="Keyword">new</span> Type[0],
                                                   <span class="Keyword">null</span>);
            
            <span class="InlineComment">// Find a parameterless Check method, if any</span>
            MethodInfo checkMethod=type.GetMethod(<span class="String">"Check"</span>, publicStatic, 
                                                  <span class="Keyword">null</span>, <span class="Keyword">new</span> Type[0],
                                                  <span class="Keyword">null</span>);

            <span class="InlineComment">// Find all parameterless methods with the [Benchmark] attribute</span>
            ArrayList benchmarkMethods=<span class="Keyword">new</span> ArrayList();
            <span class="Statement">foreach</span> (MethodInfo method <span class="Statement">in</span> type.GetMethods(publicStatic))
            {
                ParameterInfo[] parameters = method.GetParameters();
                <span class="Statement">if</span> (parameters!=<span class="Keyword">null</span> &amp;&amp; parameters.Length != 0)
                    <span class="Statement">continue</span>;
                
                <span class="Statement">if</span> (method.GetCustomAttributes
                    (<span class="Keyword">typeof</span>(BenchmarkAttribute), <span class="Keyword">false</span>).Length != 0)
                {
                    benchmarkMethods.Add (method);
                }
            }

            <span class="InlineComment">// Ignore types with no appropriate methods to benchmark</span>
            <span class="Statement">if</span> (benchmarkMethods.Count==0)
                <span class="Statement">continue</span>;
            
            Console.WriteLine (<span class="String">"Benchmarking type {0}"</span>, type.Name);

            <span class="InlineComment">// If we've got an Init method, call it once</span>
            <span class="Statement">try</span>
            {
                <span class="Statement">if</span> (initMethod!=<span class="Keyword">null</span>)
                    initMethod.Invoke (<span class="Keyword">null</span>, <span class="Keyword">new</span> <span class="ReferenceType">object</span>[]{args});
            }
            <span class="Statement">catch</span> (TargetInvocationException e)
            {
                Exception inner = e.InnerException;
                <span class="ReferenceType">string</span> message = (inner==<span class="Keyword">null</span> ? <span class="Keyword">null</span> : inner.Message);
                <span class="Statement">if</span> (message==<span class="Keyword">null</span>)
                    message = <span class="String">"(No message)"</span>;
                Console.WriteLine (<span class="String">"Init failed ({0})"</span>, message);
                <span class="Statement">continue</span>; <span class="InlineComment">// Next type</span>
            }
                
            <span class="Statement">foreach</span> (MethodInfo method <span class="Statement">in</span> benchmarkMethods)
            {
                <span class="Statement">try</span>
                {
                    <span class="InlineComment">// Reset (if appropriate)</span>
                    <span class="Statement">if</span> (resetMethod!=<span class="Keyword">null</span>)
                        resetMethod.Invoke(<span class="Keyword">null</span>, <span class="Keyword">null</span>);

                    <span class="InlineComment">// Give the test as good a chance as possible</span>
                    <span class="InlineComment">// of avoiding garbage collection</span>
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    
                    <span class="InlineComment">// Now run the test itself</span>
                    DateTime start = DateTime.Now;
                    method.Invoke (<span class="Keyword">null</span>, <span class="Keyword">null</span>);
                    DateTime end = DateTime.Now;

                    <span class="InlineComment">// Check the results (if appropriate)</span>
                    <span class="InlineComment">// Note that this doesn't affect the timing</span>
                    <span class="Statement">if</span> (checkMethod!=<span class="Keyword">null</span>)
                        checkMethod.Invoke(<span class="Keyword">null</span>, <span class="Keyword">null</span>);
                    
                    <span class="InlineComment">// If everything's worked, report the time taken, </span>
                    <span class="InlineComment">// nicely lined up (assuming no very long method names!)</span>
                    Console.WriteLine (<span class="String">"  {0,-20} {1}"</span>, method.Name, end-start);
                }
                <span class="Statement">catch</span> (TargetInvocationException e)
                {
                    Exception inner = e.InnerException;
                    <span class="ReferenceType">string</span> message = (inner==<span class="Keyword">null</span> ? <span class="Keyword">null</span> : inner.Message);
                    <span class="Statement">if</span> (message==<span class="Keyword">null</span>)
                        message = <span class="String">"(No message)"</span>;
                    Console.WriteLine (<span class="String">"  {0}: Failed ({1})"</span>, method.Name, message);
                }
            }
        }
    }
}
</pre></td></tr></table>	  
    <hr/>
    <script type="text/javascript"><!--
google_ad_client = "pub-8888427971238572";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_type = "text";
google_ad_channel ="3758180436";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br />
<script type="text/javascript"><!--
google_ad_client = "pub-8888427971238572";
google_ad_width = 728;
google_ad_height = 15;
google_ad_format = "728x15_0ads_al_s";
google_ad_channel ="3758180436";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

    <p>
      <a href="index.html">Back</a> to the main page.
    </p>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2298478-9', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>

