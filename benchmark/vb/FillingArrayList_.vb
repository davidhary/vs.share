''' <summary> List of filling arrays. </summary>
''' <remarks>
''' See http://www.pobox.com/~skeet/csharp/benchmark.html for how to run this code.
''' </remarks>
''' <remarks>
''' Copyright (c) 2008 Jon Skeet. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/4/2018 </para></remarks>
Public NotInheritable Class FillingArrayList

#Region " CONSTRUCTION and CLEAUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " CLASS AND TEST CONSTRUCTION AND CLEAUP "

    ''' <summary> The iterations. </summary>
    Private Shared _Iterations As Integer = 10000000 ' Give it a reasonable default

    ''' <summary> The list. </summary>
    Private Shared _List As ArrayList ' This is what we'll check

    ''' <summary> Initializes this object. </summary>
    ''' <remarks>
    ''' The init method is run before each test class. This method reads the number iterations as the
    ''' first command line argument is exists. Otherwise, the default <seealso cref="_Iterations"/> is
    ''' used. This method must be Public And Static, and take an array Of strings As its only
    ''' parameter. The value passed In will never be null.
    ''' </remarks>
    ''' <param name="args"> The command line arguments. </param>
    Public Shared Sub Init(ByVal args() As String)
        If args?.Length > 0 Then
            _Iterations = Int32.Parse(args(0))
        End If
    End Sub

    ''' <summary> Resets this object. </summary>
    ''' <remarks>
    ''' If present, the <code>Reset</code> method is called before each test. Like benchmark
    ''' methods, it must be public, static And parameterless to be noticed by the framework. This can
    ''' be used to clear any information which might be left from the previous test, For instance. In
    ''' this example, we make sure that the <code>list</code> variable Is null before the start Of
    ''' the test.
    ''' </remarks>
    Public Shared Sub Reset()
        ' Make sure that tests which don't produce
        ' anything don't end up using the previous result
        _List = Nothing
    End Sub

    ''' <summary> Checks this object. </summary>
    ''' <remarks>
    ''' If present, the <code>Check</code> method is called after each test is run. Again, it must be
    ''' public, static And parameterless. In practice, you can often Get away With resetting state at
    ''' the end Of this method rather than having a separate <code>Reset</code> method, but you may
    ''' prefer To use <code> Reset</code> for clarity. The aim Of the <code>Check</code> method Is
    ''' to verify that the results of the test are correct, and throw an exception
    ''' if they're not. If the check fails, the message of the exception is printed
    ''' out instead Of the time taken. In this example, we check that the size Of
    ''' the resultant list Is correct. Note that this check Is carried out <i>after</i>
    ''' the time has been calculated, so you can afford To put time-consuming checks
    ''' in here without affecting the results.
    ''' </remarks>
    ''' <exception cref="InvalidProgramException"> Thrown when an Invalid Program error condition
    '''                                            occurs. </exception>
    Public Shared Sub Check()
        If _List.Count <> _Iterations Then
            Throw New InvalidProgramException("List doesn't have the right size.")
        End If
    End Sub

#End Region

#Region " TEST METHODS "

    ''' <summary> Simple array list add benchmark test. </summary>
    ''' <remarks> Benchmark: 00:00:00.0203209</remarks>
    <Benchmark>
    Public Shared Sub Simple()
        Dim count As Integer = _Iterations

        Dim al As New ArrayList()
        For i As Integer = 0 To count - 1
            al.Add("hello")
        Next i
        ' Set the result
        _List = al
    End Sub

    ''' <summary> Right sizing benchmark. The array list is initialized to its expected size before items are added. </summary>
    ''' <remarks> Benchmark: 0000:00.0011369 </remarks>
    <Benchmark>
    Public Shared Sub RightSizing()
        Dim count As Integer = _Iterations
        Dim al As New ArrayList(count)
        For i As Integer = 0 To count - 1
            al.Add("hello")
        Next i
        ' Set the result
        _List = al
    End Sub

#End Region

End Class
