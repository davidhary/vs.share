// See http://www.pobox.com/~skeet/csharp/benchmark.html for how to run this code
using System;
using System.Collections;

public class FillingArrayList
{
    static int iterations=10000000; // Give it a reasonable default
    
    static ArrayList list; // This is what we'll check
    
    public static void Init(string[] args)
    {
        if (args.Length>0)
            iterations = Int32.Parse(args[0]);
    }
    
    public static void Reset()
    {
        // Make sure that tests which don't produce
        // anything don't end up using the previous result
        list = null;
    }
   
    public static void Check()
    {
        if (list.Count != iterations)
            throw new Exception ("List doesn't have the right size.");
    }
    
    [Benchmark]
    public static void Simple()
    {
        int count=iterations;
        
        ArrayList al = new ArrayList();
        for (int i=0; i < count; i++)
            al.Add ("hello");
        // Set the result
        list=al;
    }

    [Benchmark]
    public static void RightSizing()
    {
        int count=iterations;
        
        ArrayList al = new ArrayList(count);
        for (int i=0; i < count; i++)
            al.Add ("hello");
        // Set the result
        list=al;
    }
}
