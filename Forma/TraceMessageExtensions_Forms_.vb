Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Partial Public Module Methods

#Region " DISPLAY MESSAGE "

    ''' <summary>
    ''' Displays a trace message
    ''' </summary>
    <Extension>
    Public Sub DisplayMessage(ByVal traceMessage As isr.Core.Services.TraceMessage)
        If traceMessage Is Nothing Then Throw New ArgumentNullException(NameOf(traceMessage))
        Dim icon As MessageBoxIcon = MessageBoxIcon.Information
        Select Case traceMessage.EventType
            Case TraceEventType.Critical, TraceEventType.Error
                icon = MessageBoxIcon.Error
            Case TraceEventType.Information
                icon = MessageBoxIcon.Information
            Case TraceEventType.Verbose
                icon = MessageBoxIcon.Information
            Case TraceEventType.Warning
                icon = MessageBoxIcon.Exclamation
            Case Else
                icon = MessageBoxIcon.Information
        End Select
        MessageBox.Show(traceMessage.Details, "Trace Message", MessageBoxButtons.OK, icon,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    End Sub

#End Region

End Module

