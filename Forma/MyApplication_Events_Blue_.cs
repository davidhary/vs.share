using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.Services;

using Microsoft.VisualBasic.ApplicationServices;

namespace isr.Strip.Charts.My
{
    internal partial class MyApplication
    {

        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        private MyAssemblyInfo _MyApplicationInfo;

        /// <summary> Gets an object that provides information about the application's assembly. </summary>
        /// <value> The assembly information object. </value>
        public new MyAssemblyInfo Info
        {
            get {
                if ( this._MyApplicationInfo is null )
                {
                    this._MyApplicationInfo = new MyAssemblyInfo( base.Info );
                }

                return this._MyApplicationInfo;
            }
        }

        private static string _CurrentProcessName;
        /// <summary> Gets the current process name. </summary>
        public static string CurrentProcessName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _CurrentProcessName ) )
                {
                    _CurrentProcessName = Process.GetCurrentProcess().ProcessName;
                }

                return _CurrentProcessName;
            }
        }

        /// <summary> Gets the number of current process threads. </summary>
        /// <value> The number of current process threads. </value>
        public static int CurrentProcessThreadCount => Process.GetCurrentProcess().Threads.Count;

        /// <summary> Gets a value indicating whether the application is running under the IDE in design
        /// mode. </summary>
        /// <value> <c>True</c> if the application is running under the IDE in design mode; otherwise,
        /// <c>False</c>. </value>
        public static bool InDesignMode => Debugger.IsAttached;

        private static readonly string[] DesignerProcessNames = new string[] { "xdesproc", "devenv" };
        private static bool? _RunningFromVisualStudioDesigner = default;

        /// <summary> <see langword="True"/> if running from visual studio designer. </summary>
        /// <value> The running from visual studio designer. </value>
        public static bool RunningFromVisualStudioDesigner
        {
            get {
                if ( !_RunningFromVisualStudioDesigner.HasValue )
                {
                    using var currentProcess = Process.GetCurrentProcess();
                    _RunningFromVisualStudioDesigner = DesignerProcessNames.Contains( currentProcess.ProcessName.ToLower().Trim() );
                }

                return _RunningFromVisualStudioDesigner.Value;
            }
        }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        /// <summary> Traces the event and displays on the splash screen if exists. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="format">    The details. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        private void SplashTraceEvent( TraceEventType eventType, string format, params object[] args )
        {
            this.SplashTraceEvent( eventType, TraceEventId, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary> Traces the event and displays on the splash screen if exists. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="details">   The details. </param>
        private void SplashTraceEvent( TraceEventType eventType, string details )
        {
            this.SplashTraceEvent( eventType, TraceEventId, details );
        }

        /// <summary> Traces the event and displays on the splash screen if exists. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="details">   The details. </param>
        private void SplashTraceEvent( TraceEventType eventType, int id, string details )
        {
            MySplashScreen.SplashMessage( details );
            _ = this.MyLog.TraceSource.TraceEvent( eventType, id, details );
        }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        /// <summary> Occurs when the network connection is connected or disconnected. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Network available event information. </param>
        private void HandleNetworkAvailabilityChanged( object sender, Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs e )
        {
        }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        /// <summary> Sets the visual styles, text display styles, and current principal for the main
        /// application thread (if the application uses Windows authentication), and initializes the
        /// splash screen, if defined. Replaces the default trace listener with the modified listener.
        /// Updates the minimum splash screen display time. </summary>
        /// <param name="commandLineArgs"> A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection" /> of String,
        /// containing the command-line arguments as strings for the current application. </param>
        /// <returns> A <see cref="T:System.Boolean" /> indicating if application startup should continue. </returns>
        protected override bool OnInitialize( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            this.CreateLog();
            return base.OnInitialize( commandLineArgs );
        }

        /// <summary>
        /// When overridden in a derived class, allows a designer to emit code that initializes the
        /// splash screen.
        /// </summary>
        [DebuggerStepThrough()]
        protected override void OnCreateSplashScreen()
        {
            this.MinimumSplashScreenDisplayTime = MySettingsProperty.Settings.MinimumSplashScreenDisplayMilliseconds;
            this.SplashScreen = MyProject.Forms.MySplashScreen;
            MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );
            this.SplashTraceEvent( TraceEventType.Verbose, TraceEventId, "Allowing library use of splash screen" );
        }

        /// <summary> Handles the Shutdown event of the MyApplication control. Saves user settings for all
        /// related libraries. </summary>
        /// <remarks> This event is not raised if the application terminates abnormally. Application log is
        /// set at verbose level to log shut down operations. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShutdown()
        {
            MyProject.Application.SaveMySettingsOnExit = true;
            // Save library settings here
            this.ProcessShutDown();
            try
            {
                if ( MyProject.Application.SaveMySettingsOnExit )
                {
                    _ = this.MyLog.TraceSource.TraceEventOverride( TraceEventType.Verbose, TraceEventId, "Saving assembly settings" );
                    MySettingsProperty.Settings.Save();
                }

                this.MyLog.TraceSource.Flush();
            }
            catch
            {
            }
            finally
            {
            }

            try
            {
                MyProject.Forms.MySplashScreen.Close();
                MyProject.Forms.MySplashScreen.Dispose();
                this.SplashScreen = null;
            }
            catch
            {
            }
            finally
            {
                base.OnShutdown();
            }
        }

        /// <summary> Occurs when the application starts, before the startup form is created. </summary>
        /// <param name="eventArgs">      Startup event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override bool OnStartup( StartupEventArgs eventArgs )
        {

            /* TODO ERROR: Skipped WarningDirectiveTrivia */
            if ( eventArgs is null )
            {
                eventArgs = new StartupEventArgs( new System.Collections.ObjectModel.ReadOnlyCollection<string>( Array.Empty<string>() ) );
            }
            /* TODO ERROR: Skipped WarningDirectiveTrivia */
            // Turn on the screen hourglass
            Cursor.Current = Cursors.AppStarting;
            Application.DoEvents();
            try
            {
                Cursor.Current = Cursors.AppStarting;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
                this.ProcessStartup( eventArgs );
                if ( eventArgs.Cancel )
                {

                    // Show the exception message box with three custom buttons.
                    Cursor.Current = Cursors.Default;
                    if ( MyDialogResult.Ok == MyMessageBox.ShowDialogIgnoreExit( "Failed parsing command line.", "Failed Starting Program", MyMessageBoxIcon.Stop ) )
                    {
                        this.SplashTraceEvent( TraceEventType.Error, TraceEventId, "Application aborted by the user because of failure to parse the command line." );
                        eventArgs.Cancel = true;
                    }
                    else
                    {
                        eventArgs.Cancel = false;
                    }

                    Cursor.Current = Cursors.AppStarting;
                }

                if ( !eventArgs.Cancel )
                {
                    eventArgs.Cancel = !this.TryinitializeKnownState();
                    if ( eventArgs.Cancel )
                    {
                        _ = MyMessageBox.ShowDialogExit( $"Failed initializing application state. Check the program log at '{this.MyLog?.FullLogFileName}' for additional information.", "Failed Starting Program", MyMessageBoxIcon.Stop );
                    }
                }

                if ( eventArgs.Cancel )
                {
                    this.SplashTraceEvent( TraceEventType.Error, TraceEventId, "Application failed to start up." );
                    this.MyLog.TraceSource.Flush();

                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
                else if ( MyProject.Forms.MySplashScreen.IsCloseRequested )
                {
                    this.SplashTraceEvent( TraceEventType.Error, TraceEventId, "User close requested." );
                    this.MyLog.TraceSource.Flush();

                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
                else
                {
                    this.SplashTraceEvent( TraceEventType.Verbose, TraceEventId, "Loading application window..." );
                }
            }
            catch ( Exception ex )
            {
                this.SplashTraceEvent( TraceEventType.Error, TraceEventId, "Exception occurred starting application." );
                Cursor.Current = Cursors.Default;
                this.MyLog.TraceSource.TraceEvent( ex, TraceEventId );
                ex.Data.Add( "@isr", "Exception occurred starting this application" );
                if ( MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore( ex ) )
                {
                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
            }

            return base.OnStartup( eventArgs );
        }

        /// <summary> Occurs when launching a single-instance application and the application is already
        /// active. </summary>
        /// <param name="eventArgs">      Startup next instance event information. </param>
        protected override void OnStartupNextInstance( StartupNextInstanceEventArgs eventArgs )
        {
            this.SplashTraceEvent( TraceEventType.Information, TraceEventId, "Application next instant starting." );
            base.OnStartupNextInstance( eventArgs );
        }

        /// <summary> When overridden in a derived class, allows for code to run when an unhandled
        /// exception occurs in the application. </summary>
        /// <param name="e"> <see cref="T:Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs" />. </param>
        /// <returns> A <see cref="T:System.Boolean" /> that indicates whether the
        /// <see cref="E:Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase.UnhandledException" />
        /// event was raised. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override bool OnUnhandledException( Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs e )
        {
            bool returnedValue = true;
            if ( e is null )
            {
                Debug.Assert( !Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing." );
                return base.OnUnhandledException( e );
            }

            try
            {
                this.MyLog.DefaultFileLogWriter.Flush();
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception occurred flushing the log", "Exception occurred flushing the log: {0}", ex );
            }

            try
            {
                e.Exception.Data.Add( "@isr", "Unhandled Exception Occurred." );
                this.MyLog.TraceSource.TraceEvent( e.Exception, TraceEventId );
                if ( MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore( e.Exception ) )
                {
                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
            }
            catch
            {
                if ( MessageBox.Show( e.Exception.ToString(), "Unhandled Exception occurred.", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error, MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Abort )
                {
                }
            }
            finally
            {
            }

            return returnedValue;
        }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
    }
}
