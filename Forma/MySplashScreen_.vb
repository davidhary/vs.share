﻿''' <summary> Inherits from the <see cref="isr.Core.WindowsForms.SplashScreen"></see> to provide a
''' splash screen for the assembly. </summary>
''' <remarks> (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 02/02/2011, x.x.4050.x. </para></remarks>
Public Class MySplashScreen
    Inherits isr.Core.WindowsForms.SplashScreen

#Region " SPLASH "

    ''' <summary> Gets the is created. </summary>
    ''' <value> <c>True</c> if not nothing or disposed. </value>
    Public Shared ReadOnly Property IsCreated As Boolean
        Get
            Return MySplashScreen.instance IsNot Nothing AndAlso Not MySplashScreen.IsDisposed
        End Get
    End Property

    ''' <summary> The shared instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property Instance() As MySplashScreen

    ''' <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary> Creates the instance based on the assembly splash form. </summary>
    ''' <param name="value"> The value. </param>
    Public Shared Sub CreateInstance(ByVal value As Windows.Forms.Form)
        If value IsNot Nothing AndAlso Not value.IsDisposed Then
            SyncLock MySplashScreen.syncLocker
                MySplashScreen.instance = CType(value, MySplashScreen)
                MySplashScreen.instance.TopmostSetter(Not My.MyApplication.InDesignMode)
                MySplashScreen.instance.LicenseeName = "Integrated Scientific Resources, Inc."
            End SyncLock
        End If
    End Sub

    ''' <summary> Displays a message on the splash screen. </summary>
    ''' <param name="value"> The message. </param>
    Public Shared Sub DisplaySplashMessage(ByVal value As String)
        If MySplashScreen.instance IsNot Nothing AndAlso MySplashScreen.instance.Visible Then
            MySplashScreen.instance.DisplayMessage(value)
        End If
    End Sub

    ''' <summary> Displays a message on the splash screen. </summary>
    ''' <param name="value"> The message. </param>
    Public Shared Sub SplashMessage(ByVal value As String)
        If MySplashScreen.instance IsNot Nothing AndAlso MySplashScreen.instance.Visible Then
            MySplashScreen.instance.DisplayMessage(value)
        End If
    End Sub

    ''' <summary> Displays a splash message and logs it at the specified trace level. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="details">   The event message details. </param>
    Public Shared Sub LogSplashMessage(ByVal eventType As TraceEventType, ByVal details As String)
        MySplashScreen.LogSplashMessage(eventType, 0, details)
    End Sub

    ''' <summary> Displays a splash message and logs it at the specified trace level. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="details">   The event message details. </param>
    Public Shared Sub LogSplashMessage(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String)
        MySplashScreen.DisplaySplashMessage(details)
        My.Application.Log.TraceSource.TraceEvent(eventType, id, details)
    End Sub

#End Region

End Class
