
Namespace My

    Partial Public Class MySettings

        ''' <summary> Saves all settings to allow user editing. </summary>
        Public Sub SaveAll()
            For Each pv As System.Configuration.SettingsPropertyValue In Me.PropertyValues
                pv.PropertyValue = pv.PropertyValue
            Next
            Me.Save()
        End Sub

        ''' <summary> All user configuration locations. </summary>
        ''' <returns> A String() </returns>
        <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
        Public Function AllUserConfigurationLocations() As String()
            Dim configuration As System.Configuration.Configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None)
            Dim l As New List(Of String)
            If configuration.Locations.Count > 0 Then
                For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                    l.Add(s.Path)
                Next
            End If
            Return l.ToArray
        End Function

        ''' <summary> User roaming configuration locations. </summary>
        ''' <returns> A String() </returns>
        <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
        Public Function UserRoamingConfigurationLocations() As String()
            Dim l As New List(Of String)
            Dim configuration As System.Configuration.Configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoaming)
            If configuration.Locations.Count > 0 Then
                For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                    l.Add(s.Path)
                Next
            End If
            Return l.ToArray
        End Function

        ''' <summary> User local configuration locations. </summary>
        ''' <returns> A String() </returns>
        <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
        Public Function UserLocalConfigurationLocations() As String()
            Dim l As New List(Of String)
            Dim configuration As System.Configuration.Configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal)
            If configuration.Locations.Count > 0 Then
                For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                    l.Add(s.Path)
                Next
            End If
            Return l.ToArray
        End Function

    End Class

End Namespace
