﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Namespace ControlCollectionExtensions
    ''' <summary> Includes extensions for <see cref="Control.ControlCollection">Control Collection</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see> </summary>
        ''' <remarks> This is required because setting a tool tip from the parent form does not show the
        ''' tool tip if hovering above children controls hosted by the user control. </remarks>
        ''' <param name="parent">  Reference to the parent form or control. </param>
        ''' <param name="toolTip"> The parent form or control tool tip. </param>
        <Extension()>
        Public Sub ToolTipSetter(ByVal parent As System.Windows.Forms.Control, ByVal toolTip As ToolTip)
            If parent Is Nothing Then Return
            If toolTip Is Nothing Then Return
            toolTip.SetToolTip(parent, toolTip.GetToolTip(parent))
            If parent.HasChildren Then
                Methods.ToolTipSetter(parent.Controls, toolTip)
            End If
        End Sub

        ''' <summary>
        ''' Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
        ''' </summary>
        ''' <remarks> This is required because setting a tool tip from the parent form does not show the
        ''' tool tip if hovering above children controls hosted by the user control. </remarks>
        ''' <param name="controls"> The collection of controls. </param>
        ''' <param name="toolTip">  The parent form or control tool tip. </param>
        <Extension()>
        Public Sub ToolTipSetter(ByVal controls As System.Windows.Forms.Control.ControlCollection, ByVal toolTip As ToolTip)
            If controls Is Nothing Then Return
            If toolTip Is Nothing Then Return
            For Each control As System.Windows.Forms.Control In controls
                Methods.ToolTipSetter(control, toolTip)
            Next
        End Sub

    End Module
End Namespace
