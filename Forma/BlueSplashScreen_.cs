using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Strip.Charts
{
    /// <summary> Inherits from the <see cref="isr.Core.Forma.BlueSplash"></see> to provide a
    /// splash screen for the assembly. </summary>
    /// <license> (c) 2015 Integrated Scientific Resources, Inc.<para>
    /// Licensed under The MIT License. </para><para>
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </para> </license>
    /// <history date="05/19/2015" by="David" revision="x.x.5617.x"> Created. </history>
    public class MySplashScreen : Core.Forma.BlueSplash
    {

        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        /// <summary> Gets the sentinel indicating if the splash is created. </summary>
        /// <value> <c>True</c> if not nothing or disposed. </value>
        public static bool IsCreated => Instance is object && !Instance.IsDisposed;

        /// <summary> Gets the sentinel indicating if the splash is created and visible. </summary>
        /// <value> <c>True</c> if not nothing or disposed and visible. </value>
        public static bool IsVisible => IsCreated && Instance.Visible;

        /// <summary> The shared instance. </summary>
        /// <value> The instance. </value>
        private static MySplashScreen Instance { get; set; }

        /// <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
        private static readonly object syncLocker = new object();

        /// <summary> Creates the instance based on the assembly splash form. </summary>
        /// <param name="value"> The value. </param>
        public static void CreateInstance( Form value )
        {
            if ( value is object && !value.IsDisposed )
            {
                lock ( syncLocker )
                {
                    Instance = ( MySplashScreen ) value;
                    Instance.TopmostSetter( !Debugger.IsAttached );
                    // MySplashScreen.instance.LicenseeName = "Integrated Scientific Resources, Inc."
                }
            }
        }

        /// <summary> Displays a message on the splash screen. </summary>
        /// <param name="value"> The message. </param>
        public static void SplashMessage( string value )
        {
            if ( IsVisible )
            {
                Instance.DisplayMessage( value );
            }
        }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
    }
}
