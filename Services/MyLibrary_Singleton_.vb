Namespace My

    Public Module MyLibraryProperty
        ''' <summary>
        ''' Returns a singleton instance of the <see cref="Library">library manager</see>.
        ''' </summary>
        Public ReadOnly Property [Library]() As My.MyLibrary
            Get
                Return My.MyLibrary.[Get]
            End Get
        End Property
    End Module

    ''' <summary> Defines a singleton class to provide project management for this project.
    ''' This class has the Public Shared Not Creatable instancing property. </summary>
    ''' <remarks> (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 06/13/2006, 1.0.2355.x. </para></remarks>
    Public NotInheritable Class MyLibrary

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
            Me.Construct()
        End Sub

#End Region

#Region " SINGLETON "

        ''' <summary> Creates a new instance of this class.</summary>
        Public Overloads Shared Sub NewInstance()
            SyncLock MyLibrary.syncLocker
                MyLibrary.instance = New MyLibrary
            End SyncLock
        End Sub

        ''' <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
        Private Shared ReadOnly syncLocker As Object = New Object

        ''' <summary> The singleton instance </summary>
        Private Shared instance As MyLibrary

        ''' <summary> Instantiates the class. </summary>
        ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
        ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        ''' <returns> A new or existing instance of the class. </returns>
        Public Shared Function [Get]() As MyLibrary
            If Not MyLibrary.Instantiated Then MyLibrary.NewInstance()
            Return MyLibrary.instance
        End Function

        ''' <summary> Gets or sets True if the singleton instance was instantiated. </summary>
        ''' <value> The instantiated. </value>
        Public Shared ReadOnly Property Instantiated() As Boolean
            Get
                SyncLock MyLibrary.syncLocker
                    Return MyLibrary.instance IsNot Nothing
                End SyncLock
            End Get
        End Property

#End Region

#Region " SETTINGS "

        ''' <summary> Gets my settings. </summary>
        ''' <value> my settings. </value>
        Public Shared ReadOnly Property MySettings As MySettings
            Get
                Return My.MySettings.Default
            End Get
        End Property

#End Region

    End Class

End Namespace

