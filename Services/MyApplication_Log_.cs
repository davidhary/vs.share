﻿using System;
using System.Diagnostics;

using isr.Core.Services;

namespace isr.Strip.Charts.My
{
    internal partial class MyApplication
    {

        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        private MyLog _MyLog;
        /// <summary> Gets or sets the log. </summary>
        /// <value> The log. </value>
        public MyLog MyLog
        {
            get {
                if ( this._MyLog is null )
                {
                    this.CreateLog();
                }

                return this._MyLog;
            }
        }

        /// <summary> Creates the log. </summary>
        private void CreateLog()
        {
            this._MyLog = null;
            try
            {
                this._MyLog = MyLog.get_NewInstance( MyProject.Application.Info.ProductName );
                Microsoft.VisualBasic.Logging.FileLogTraceListener listener;
                {
                    var withBlock = this.MyLog;
                    listener = withBlock.ReplaceDefaultTraceListener( true );
                    if ( !withBlock.LogFileExists )
                    {
                        _ = withBlock.TraceEventOverride( ProductTimeTraceMessage() );
                    }
                }

                // set the log for the application
                {
                    var withBlock1 = MyProject.Application.Log;
                    if ( !string.Equals( withBlock1.DefaultFileLogWriter.FullLogFileName, listener.FullLogFileName, StringComparison.OrdinalIgnoreCase ) )
                    {
                        withBlock1.TraceSource.Listeners.Remove( DefaultFileLogTraceListener.DefaultFileLogWriterName );
                        _ = withBlock1.TraceSource.Listeners.Add( listener );
                        withBlock1.TraceSource.Switch.Level = SourceLevels.Verbose;
                    }
                }

                // set the trace level.
                this.ApplyTraceLogLevel();
            }
            catch
            {
                if ( this._MyLog is object )
                {
                    this._MyLog.Dispose();
                }

                this._MyLog = null;
                throw;
            }
        }

        /// <summary> Gets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLevel { get; set; }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        /// <summary> Identifies this application. </summary>
        /// <param name="talker"> The talker. </param>
        public void Identify( ITraceMessageTalker talker )
        {
            _ = talker.PublishDateMessage( ProductTimeTraceMessage() );
            _ = talker.PublishDateMessage( this.LogTraceMessage() );
            talker.IdentifyTalker( this.IdentityTraceMessage() );
        }

        /* TODO ERROR: Skipped WarningDirectiveTrivia */        /// <summary> Gets the identity. </summary>
        /// <value> The identity. </value>
        public static string Identity => $"{AssemblyProduct} ID = {TraceEventId:X}";

        /// <summary> Gets a message describing the identity trace. </summary>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage IdentityTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, TraceEventId, Identity );
        }

        /// <summary> Application log trace message. </summary>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage LogTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, TraceEventId, $"Log at;. {this.MyLog.FullLogFileName}" );
        }

        /// <summary> Product time trace message. </summary>
        /// <returns> A TraceMessage. </returns>
        public static TraceMessage ProductTimeTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, TraceEventId, Core.Services.AssemblyExtensions.Methods.BuildProductTimeCaption( MyProject.Application.Info ) );
        }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
        /* TODO ERROR: Skipped RegionDirectiveTrivia */
        /// <summary> Gets the unpublished identify date. </summary>
        /// <value> The unpublished identify date. </value>
        public DateTimeOffset UnpublishedIdentifyDate { get; set; }

        /// <summary> Gets or sets the unpublished trace messages. </summary>
        /// <value> The unpublished trace messages. </value>
        public TraceMessagesQueue UnpublishedTraceMessages { get; private set; } = new TraceMessagesQueue();

        /// <summary> Logs unpublished exception. </summary>
        /// <param name="message"> The message. </param>
        public string LogUnpublishedMessage( TraceMessage message )
        {
            if ( message is null )
            {
                return string.Empty;
            }
            else
            {
                if ( DateTimeOffset.Now.Date > this.UnpublishedIdentifyDate )
                {
                    _ = this.MyLog.TraceEventOverride( ProductTimeTraceMessage() );
                    _ = this.MyLog.TraceEventOverride( this.LogTraceMessage() );
                    _ = this.MyLog.TraceEventOverride( this.IdentityTraceMessage() );
                    this.UnpublishedIdentifyDate = DateTimeOffset.Now.Date;
                }

                this.UnpublishedTraceMessages.Enqueue( message );
                _ = this.MyLog.TraceEvent( message );
                return message.Details;
            }
        }

        /* TODO ERROR: Skipped EndRegionDirectiveTrivia */
    }
}