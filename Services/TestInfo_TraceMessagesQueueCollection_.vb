﻿
Partial Public NotInheritable Class TestInfo

    Partial Private Class TraceMessageQueueCollection
        Inherits ObjectModel.Collection(Of isr.Core.Services.TraceMessagesQueue)
        Public Sub New()
            MyBase.New
            Me.Add(TestInfo.TraceMessagesQueueListener)
            Me.Add(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            Me.Add(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            Me.Add(isr.VI.Tsp.My.MyLibrary.UnpublishedTraceMessages)
            Me.Add(isr.VI.Tsp.K3700.My.MyLibrary.UnpublishedTraceMessages)
        End Sub
    End Class


End Class
