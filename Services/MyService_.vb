Imports System.Security.Principal
Imports isr.Core.Services
Partial Public Class ImportService

#Region " PROCESS "

    ''' <summary> Query if the current process is already running. </summary>
    ''' <returns> true if already running, false if not. </returns>
    Public Shared Function IsAlreadyRunning() As Boolean
        Dim processName As String = Process.GetCurrentProcess().ProcessName
        Return Process.GetProcesses().Where(Function(p) p.ProcessName.Contains(processName)).Count() > 1
    End Function

    ''' <summary> Query if this object has Administrator privileges. </summary>
    ''' <returns> true if Administrator privileges, false if not. </returns>
    Public Shared Function HasAdministratorPrivileges() As Boolean
        Dim identity As WindowsIdentity = WindowsIdentity.GetCurrent
        Dim principal As WindowsPrincipal = New WindowsPrincipal(identity)
        Return principal.IsInRole(WindowsBuiltInRole.Administrator)
    End Function

    ''' <summary> Gets the number of current process threads. </summary>
    ''' <value> The number of current process threads. </value>
    Public Shared ReadOnly Property CurrentProcessThreadCount As Integer
        Get
            Return Process.GetCurrentProcess.Threads.Count
        End Get
    End Property

#End Region

#Region " SHARED TALKER "

    ''' <summary> Gets the trace message talker. </summary>
    ''' <value> The trace message talker. </value>
    Public Shared ReadOnly Property Talker As ITraceMessageTalker = New TraceMessageTalker

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Public Shared Sub AddListeners(ByVal listeners As IList(Of ITraceMessageListener))
        Talker.Listeners.AddRange(listeners)
    End Sub

    ''' <summary> Gets the log. </summary>
    ''' <value> The log. </value>
    Public Shared ReadOnly Property MyLog As MyLog = MyLog.NewInstance(ServiceInfo.ServiceProductName)

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <param name="log"> The log. </param>
    Public Shared Sub AddListeners(ByVal log As MyLog)
        Talker.Listeners.Add(log)
    End Sub

#End Region

End Class
