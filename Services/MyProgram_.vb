Imports System.Linq
Imports System.Windows.Forms
Imports isr.Core.Services
Partial Friend Class Program

#Region " APPLICATION EXTENSIONS "

    ''' <summary> Gets the number of running processes with the current process name. </summary>
    ''' <returns> Number of process with the current process name. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function CurrentProcessCount() As Integer
        Dim processName As String = Process.GetCurrentProcess().ProcessName
        Return Process.GetProcesses().Where(Function(p) p.ProcessName.Contains(processName)).Count()
    End Function

    ''' <summary> Gets the number of current process threads. </summary>
    ''' <value> The number of current process threads. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property CurrentProcessThreadCount As Integer
        Get
            Return Process.GetCurrentProcess.Threads.Count
        End Get
    End Property

    ''' <summary> Gets an object that provides information about the application's assembly. </summary>
    ''' <value> The assembly information object. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property Info As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)

    ''' <summary> Gets the log. </summary>
    ''' <value> The log. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property MyLog As MyLog = MyLog.NewInstance(Program.ServiceName)

    Private Shared _CurrentProcessName As String
    ''' <summary> Gets the current process name. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property CurrentProcessName() As String
        Get
            If String.IsNullOrWhiteSpace(_CurrentProcessName) Then
                _CurrentProcessName = Process.GetCurrentProcess().ProcessName.ToUpperInvariant
            End If
            Return _CurrentProcessName
        End Get
    End Property

#End Region

#Region " TRACE "

    ''' <summary> Gets the trace source. </summary>
    ''' <value> The trace source. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property TraceSource As MyTraceSource
        Get
            Return MyLog.TraceSource
        End Get
    End Property

    ''' <summary> Traces the event. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="format">    The details. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub TraceEvent(ByVal eventType As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        TraceEvent(eventType, TraceEventId, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Traces the event. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="details">   The details. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub TraceEvent(ByVal eventType As TraceEventType, ByVal details As String)
        TraceEvent(eventType, TraceEventId, details)
    End Sub

    ''' <summary> Traces the event. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="details">   The details. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub TraceEvent(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String)
        TraceSource.TraceEvent(eventType, id, details)
    End Sub

#End Region

#Region " EXCEPTION HANDLER "

    ''' <summary> When overridden in a derived class, allows for code to run when an unhandled
    ''' exception occurs in the application. </summary>
    ''' <param name="e"> <see cref="T:Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs" />. </param>
    ''' <returns> A <see cref="T:System.Boolean" /> that indicates whether the
    ''' <see cref="E:Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase.UnhandledException" />
    ''' event was raised. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function HandleException(ByVal e As System.Threading.ThreadExceptionEventArgs) As Boolean

        Dim returnedValue As Boolean = True
        If e Is Nothing Then
            Debug.Assert(Not Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing.")
        End If

        Try
            MyLog.DefaultFileLogWriter.Flush()
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception occurred flushing the log", "Exception occurred flushing the log: {0}", ex)
        End Try

        Try
            e.Exception.Data.Add("@isr", "Unhandled Exception Occurred.")
            MyLog.TraceSource.TraceEvent(e.Exception, Program.TraceEventId)
            If isr.Core.Services.MyDialogResult.Abort = isr.Core.Services.MyMessageBox.ShowDialogAbortIgnore(e.Exception) Then
                ' exit with an error code
                Environment.Exit(-1)
                Application.Exit()
            End If
        Catch
            If MessageBox.Show(e.Exception.ToString, "Unhandled Exception occurred.",
                               MessageBoxButtons.AbortRetryIgnore, Windows.Forms.MessageBoxIcon.Error,
                               MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly) = Windows.Forms.DialogResult.Abort Then
                ' exit with an error code
                Environment.Exit(-1)
                Application.Exit()
            End If
        Finally
        End Try
        Return returnedValue

    End Function

#End Region

End Class
