﻿Namespace My

    Partial Public Class MyLibrary

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Shared Property TraceEventId As Integer = isr.Core.Services.My.ProjectTraceEventId.CoreLibrary

        Public Const AssemblyTitle As String = String.Empty
        Public Const AssemblyDescription As String = String.Empty
        Public Const AssemblyProduct As String = String.Empty

    End Class

End Namespace
