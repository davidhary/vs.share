**Fair Site Use License Agreement**

1\. We grant you license to install and use this software on any number
of computers for your internal use. If you do not agree to the following
terms of this license, please uninstall and remove all copies and return
the product to the place that you purchased it from within 30 days of
your purchase for a full refund.

2\. You may not transfer your license to use the software to another
party.

3\. The software is protected by the copyright laws of the U.S. and
other countries, and we retain all intellectual property rights in the
software. You may not separately publish, sell, market, distribute,
lend, lease, rent, or sublicense the software code. However, this
license is not to be construed as prohibiting or limiting any fair use
sanctioned by copyright law, such as permitted library and classroom
usage or reverse engineering.

LIMITED WARRANTY

4\. We warrant that the software will provide the features and functions
generally described in the product specification on our website when you
purchased it and in the product documentation. Media on which the
Software is furnished, if any, will be free from defects in materials
and workmanship.

5\. We have taken all reasonable steps to keep the software free of
viruses, spyware, \"back door\" entrances, or any other harmful code. We
will not track or collect any information about you, your data, or your
use of the software except as you specifically authorize. The software
will not download or install patches, upgrades, or any third party
software without getting your permission. We will not intentionally
deprive you of your ability to use any features of the software or
access to your data.

6\. We do not warrant that the software or your ability to use it will
be uninterrupted or error-free. To the extent permitted by applicable
law, we disclaim any implied warranty of merchantability or fitness for
a particular purpose.

LIMITATIONS ON LIABILITY

7\. Your exclusive remedy under the above limited warranty shall be, at
our option, either a full refund of the purchase price or correction of
the defective software or media. To the fullest extent permitted by
applicable law, we disclaim all liability for indirect or consequential
damages that arise under this license agreement. Nothing in this
agreement limits our liability to you in the event of death or personal
injury resulting from gross negligence, fraud, or knowing
misrepresentation on our part.

SOURCE CODE ACCESS

8\. We grant you a license to use the source code to support your use of
the Instrument.

9\. The source code shall be used solely to maintain the Instrument and
shall be subject to every restriction on use set forth in this
Agreement. You agree not to disclose the source code to third parties
except on a need-to-know basis under an appropriate duty of
confidentiality.

GENERAL PROVISIONS

10\. If any part of this agreement is found to be invalid or
unenforceable, the remaining terms will stay in effect. This agreement
does not prejudice the statutory rights of any party dealing as a
consumer.

11\. If we have our corporate headquarters in the USA, this agreement
will be governed by the laws, including Article 2 of the Uniform
Commercial Code, of the state in which we are headquartered. If that
state has enacted the Uniform Computer Information Transactions Act
(UCITA) or substantially similar law, said statute shall not govern any
aspect of this agreement and instead the law as it existed prior to such
enactment shall govern. If we do not have our corporate headquarters in
the USA, this agreement will be governed by the laws of England.

12\. This agreement does not supersede any express warranties we made to
you. Any modification to this agreement must be agreed to in writing by
both parties.
