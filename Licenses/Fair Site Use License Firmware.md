**Fair Site Use License Agreement - Instrument Firmware**

1\. We grant you license to install and use the firmware on any number
of instruments for your internal use. If you do not agree to the
following terms of this license, please return the Instrument to us
within 30 days of your purchase for a full refund.

2\. You may not transfer your license to use the Instrument to another
party.

3\. The firmware is protected by the copyright laws of the U.S. and
other countries, and we retain all intellectual property rights in the
firmware. You may not separately publish, sell, market, distribute,
lend, lease, rent, or sublicense the firmware code. However, this
license is not to be construed as prohibiting or limiting any fair use
sanctioned by copyright law.

LIMITED WARRANTY

4\. WE WARRANT THAT THE FIRMWARE WILL PROVIDE THE FEATURES AND FUNCTIONS
GENERALLY DESCRIBED IN THE PRODUCT SPECIFICATION WHEN YOU PURCHASED IT.

5\. WE HAVE TAKEN ALL REASONABLE STEPS TO KEEP THE FIRMWARE FREE OF
VIRUSES, SPYWARE, \"BACK DOOR\" ENTRANCES, OR ANY OTHER HARMFUL CODE. WE
WILL NOT TRACK OR COLLECT ANY INFORMATION ABOUT YOU, YOUR DATA, OR YOUR
USE OF THE FIRMWARE EXCEPT AS YOU SPECIFICALLY AUTHORIZE. THE FIRMWARE
WILL NOT DOWNLOAD OR INSTALL PATCHES, UPGRADES, OR ANY THIRD PARTY
FIRMWARE WITHOUT GETTING YOUR PERMISSION. WE WILL NOT INTENTIONALLY
DEPRIVE YOU OF YOUR ABILITY TO USE ANY FEATURES OF THE FIRMWARE.

6\. WE DO NOT WARRANT THAT THE FIRMWARE OR YOUR ABILITY TO USE IT WILL
BE UNINTERRUPTED OR ERROR-FREE. TO THE EXTENT PERMITTED BY APPLICABLE
LAW, WE DISCLAIM ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR
A PARTICULAR PURPOSE.

LIMITATIONS ON LIABILITY

7\. YOUR EXCLUSIVE REMEDY UNDER THE ABOVE LIMITED WARRANTY SHALL BE, AT
OUR OPTION, EITHER A FULL REFUND OF THE PURCHASE PRICE OR CORRECTION OF
THE DEFECTIVE INSTRUMENT. TO THE FULLEST EXTENT PERMITTED BY APPLICABLE
LAW, WE DISCLAIM ALL LIABILITY FOR INDIRECT OR CONSEQUENTIAL DAMAGES
THAT ARISE UNDER THIS LICENSE AGREEMENT. NOTHING IN THIS AGREEMENT
LIMITS OUR LIABILITY TO YOU IN THE EVENT OF DEATH OR PERSONAL INJURY
RESULTING FROM GROSS NEGLIGENCE, FRAUD, OR KNOWING MISREPRESENTATION ON
OUR PART.

SOURCE CODE ACCESS

8\. We grant you a license to use the source code to support your use of
the Instrument.

9\. The source code shall be used solely to maintain the Instrument and
shall be subject to every restriction on use set forth in this
Agreement. You agree not to disclose the source code to third parties
except on a need-to-know basis under an appropriate duty of
confidentiality.

GENERAL PROVISIONS

10\. If any part of this agreement is found to be invalid or
unenforceable, the remaining terms will stay in effect. This agreement
does not prejudice the statutory rights of any party dealing as a
consumer.

11\. This agreement is governed by the laws, including Article 2 of the
Uniform Commercial Code, of the state of California.

12\. This agreement does not supersede any express warranties we made to
you. Any modification to this agreement must be agreed to in writing by
both parties.
